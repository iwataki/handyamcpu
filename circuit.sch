<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.1.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Parts" color="7" fill="1" visible="yes" active="yes"/>
<layer number="101" name="Hidden" color="7" fill="1" visible="yes" active="yes"/>
<layer number="102" name="Changes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="fp3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="Beschreib" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="BGA-Top" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="BD-Top" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="KASTMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="KASTMAAT2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="FRNTTEKEN" color="7" fill="1" visible="yes" active="yes"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="7" fill="1" visible="yes" active="yes"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="BACKMAAT2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="BIFRNTTEK" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="BIFRNTMAT" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="BottomExtra" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="yes" active="yes"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="yes" active="yes"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="Accent" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="MicroControler_Robotech">
<packages>
<package name="MLF32">
<description>&lt;b&gt;32M1-A&lt;/b&gt; Micro Lead Frame package (MLF)</description>
<rectangle x1="-2.5" y1="1.6" x2="-2.05" y2="1.9" layer="51"/>
<smd name="1" x="-2.35" y="1.75" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<rectangle x1="-2.5" y1="1.1" x2="-2.05" y2="1.4" layer="51"/>
<smd name="2" x="-2.35" y="1.25" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<rectangle x1="-2.5" y1="0.6" x2="-2.05" y2="0.9" layer="51"/>
<smd name="3" x="-2.35" y="0.75" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<rectangle x1="-2.5" y1="0.1" x2="-2.05" y2="0.4" layer="51"/>
<smd name="4" x="-2.35" y="0.25" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<rectangle x1="-2.5" y1="-0.4" x2="-2.05" y2="-0.1" layer="51"/>
<smd name="5" x="-2.35" y="-0.25" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<rectangle x1="-2.5" y1="-0.9" x2="-2.05" y2="-0.6" layer="51"/>
<smd name="6" x="-2.35" y="-0.75" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<rectangle x1="-2.5" y1="-1.4" x2="-2.05" y2="-1.1" layer="51"/>
<smd name="7" x="-2.35" y="-1.25" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<rectangle x1="-2.5" y1="-1.9" x2="-2.05" y2="-1.6" layer="51"/>
<smd name="8" x="-2.35" y="-1.75" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<rectangle x1="-1.9" y1="-2.5" x2="-1.6" y2="-2.05" layer="51"/>
<smd name="9" x="-1.75" y="-2.35" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<rectangle x1="-1.4" y1="-2.5" x2="-1.1" y2="-2.05" layer="51"/>
<smd name="10" x="-1.25" y="-2.35" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<rectangle x1="-0.9" y1="-2.5" x2="-0.6" y2="-2.05" layer="51"/>
<smd name="11" x="-0.75" y="-2.35" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<rectangle x1="-0.4" y1="-2.5" x2="-0.1" y2="-2.05" layer="51"/>
<smd name="12" x="-0.25" y="-2.35" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<rectangle x1="0.1" y1="-2.5" x2="0.4" y2="-2.05" layer="51"/>
<smd name="13" x="0.25" y="-2.35" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<rectangle x1="0.6" y1="-2.5" x2="0.9" y2="-2.05" layer="51"/>
<smd name="14" x="0.75" y="-2.35" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<rectangle x1="1.1" y1="-2.5" x2="1.4" y2="-2.05" layer="51"/>
<smd name="15" x="1.25" y="-2.35" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<rectangle x1="1.6" y1="-2.5" x2="1.9" y2="-2.05" layer="51"/>
<smd name="16" x="1.75" y="-2.35" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<rectangle x1="2.05" y1="-1.9" x2="2.5" y2="-1.6" layer="51"/>
<smd name="17" x="2.35" y="-1.75" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<rectangle x1="2.05" y1="-1.4" x2="2.5" y2="-1.1" layer="51"/>
<smd name="18" x="2.35" y="-1.25" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<rectangle x1="2.05" y1="-0.9" x2="2.5" y2="-0.6" layer="51"/>
<smd name="19" x="2.35" y="-0.75" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<rectangle x1="2.05" y1="-0.4" x2="2.5" y2="-0.1" layer="51"/>
<smd name="20" x="2.35" y="-0.25" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<rectangle x1="2.05" y1="0.1" x2="2.5" y2="0.4" layer="51"/>
<smd name="21" x="2.35" y="0.25" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<rectangle x1="2.05" y1="0.6" x2="2.5" y2="0.9" layer="51"/>
<smd name="22" x="2.35" y="0.75" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<rectangle x1="2.05" y1="1.1" x2="2.5" y2="1.4" layer="51"/>
<smd name="23" x="2.35" y="1.25" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<rectangle x1="2.05" y1="1.6" x2="2.5" y2="1.9" layer="51"/>
<smd name="24" x="2.35" y="1.75" dx="0.7" dy="0.3" layer="1" roundness="50"/>
<rectangle x1="1.6" y1="2.05" x2="1.9" y2="2.5" layer="51"/>
<smd name="25" x="1.75" y="2.35" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<rectangle x1="1.1" y1="2.05" x2="1.4" y2="2.5" layer="51"/>
<smd name="26" x="1.25" y="2.35" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<rectangle x1="0.6" y1="2.05" x2="0.9" y2="2.5" layer="51"/>
<smd name="27" x="0.75" y="2.35" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<rectangle x1="0.1" y1="2.05" x2="0.4" y2="2.5" layer="51"/>
<smd name="28" x="0.25" y="2.35" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<rectangle x1="-0.4" y1="2.05" x2="-0.1" y2="2.5" layer="51"/>
<smd name="29" x="-0.25" y="2.35" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<rectangle x1="-0.9" y1="2.05" x2="-0.6" y2="2.5" layer="51"/>
<smd name="30" x="-0.75" y="2.35" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<rectangle x1="-1.4" y1="2.05" x2="-1.1" y2="2.5" layer="51"/>
<smd name="31" x="-1.25" y="2.35" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<rectangle x1="-1.9" y1="2.05" x2="-1.6" y2="2.5" layer="51"/>
<smd name="32" x="-1.75" y="2.35" dx="0.3" dy="0.7" layer="1" roundness="50"/>
<wire x1="-2.35" y1="2.05" x2="-2.05" y2="2.35" width="0.254" layer="21"/>
<wire x1="-2.05" y1="2.35" x2="2.05" y2="2.35" width="0.254" layer="51"/>
<wire x1="2.05" y1="2.35" x2="2.35" y2="2.05" width="0.254" layer="21"/>
<wire x1="2.35" y1="2.05" x2="2.35" y2="-2.05" width="0.254" layer="51"/>
<wire x1="2.35" y1="-2.05" x2="2.05" y2="-2.35" width="0.254" layer="21"/>
<wire x1="2.05" y1="-2.35" x2="-2.05" y2="-2.35" width="0.254" layer="51"/>
<wire x1="-2.05" y1="-2.35" x2="-2.35" y2="-2.05" width="0.254" layer="21"/>
<wire x1="-2.35" y1="-2.05" x2="-2.35" y2="2.05" width="0.254" layer="51"/>
<circle x="-1.55" y="1.55" radius="0.15" width="0.254" layer="21"/>
<text x="-2.54" y="3.175" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.55" y1="-1.55" x2="1.55" y2="1.55" layer="1"/>
</package>
<package name="TQFP32-08">
<description>&lt;B&gt;Thin Plasic Quad Flat Package&lt;/B&gt; Grid 0.8 mm</description>
<wire x1="3.505" y1="3.505" x2="3.505" y2="-3.505" width="0.1524" layer="21"/>
<wire x1="3.505" y1="-3.505" x2="-3.505" y2="-3.505" width="0.1524" layer="21"/>
<wire x1="-3.505" y1="-3.505" x2="-3.505" y2="3.15" width="0.1524" layer="21"/>
<wire x1="-3.15" y1="3.505" x2="3.505" y2="3.505" width="0.1524" layer="21"/>
<wire x1="-3.15" y1="3.505" x2="-3.505" y2="3.15" width="0.1524" layer="21"/>
<rectangle x1="-4.5466" y1="2.5714" x2="-3.556" y2="3.0286" layer="51"/>
<circle x="-2.7432" y="2.7432" radius="0.3592" width="0.1524" layer="21"/>
<smd name="1" x="-4.2926" y="2.8" dx="1.27" dy="0.5588" layer="1"/>
<text x="-2.7686" y="5.08" size="0.8128" layer="25">&gt;NAME</text>
<text x="-3.0226" y="-1.27" size="0.8128" layer="27">&gt;VALUE</text>
<rectangle x1="-4.5466" y1="1.7714" x2="-3.556" y2="2.2286" layer="51"/>
<smd name="2" x="-4.2926" y="2" dx="1.27" dy="0.5588" layer="1"/>
<rectangle x1="-4.5466" y1="0.9714" x2="-3.556" y2="1.4286" layer="51"/>
<smd name="3" x="-4.2926" y="1.2" dx="1.27" dy="0.5588" layer="1"/>
<rectangle x1="-4.5466" y1="0.1714" x2="-3.556" y2="0.6286" layer="51"/>
<smd name="4" x="-4.2926" y="0.4" dx="1.27" dy="0.5588" layer="1"/>
<rectangle x1="-4.5466" y1="-0.6286" x2="-3.556" y2="-0.1714" layer="51"/>
<smd name="5" x="-4.2926" y="-0.4" dx="1.27" dy="0.5588" layer="1"/>
<rectangle x1="-4.5466" y1="-1.4286" x2="-3.556" y2="-0.9714" layer="51"/>
<smd name="6" x="-4.2926" y="-1.2" dx="1.27" dy="0.5588" layer="1"/>
<rectangle x1="-4.5466" y1="-2.2286" x2="-3.556" y2="-1.7714" layer="51"/>
<smd name="7" x="-4.2926" y="-2" dx="1.27" dy="0.5588" layer="1"/>
<rectangle x1="-4.5466" y1="-3.0286" x2="-3.556" y2="-2.5714" layer="51"/>
<smd name="8" x="-4.2926" y="-2.8" dx="1.27" dy="0.5588" layer="1"/>
<rectangle x1="-3.0286" y1="-4.5466" x2="-2.5714" y2="-3.556" layer="51"/>
<smd name="9" x="-2.8" y="-4.2926" dx="0.5588" dy="1.27" layer="1"/>
<rectangle x1="-2.2286" y1="-4.5466" x2="-1.7714" y2="-3.556" layer="51"/>
<smd name="10" x="-2" y="-4.2926" dx="0.5588" dy="1.27" layer="1"/>
<rectangle x1="-1.4286" y1="-4.5466" x2="-0.9714" y2="-3.556" layer="51"/>
<smd name="11" x="-1.2" y="-4.2926" dx="0.5588" dy="1.27" layer="1"/>
<rectangle x1="-0.6286" y1="-4.5466" x2="-0.1714" y2="-3.556" layer="51"/>
<smd name="12" x="-0.4" y="-4.2926" dx="0.5588" dy="1.27" layer="1"/>
<rectangle x1="0.1714" y1="-4.5466" x2="0.6286" y2="-3.556" layer="51"/>
<smd name="13" x="0.4" y="-4.2926" dx="0.5588" dy="1.27" layer="1"/>
<rectangle x1="0.9714" y1="-4.5466" x2="1.4286" y2="-3.556" layer="51"/>
<smd name="14" x="1.2" y="-4.2926" dx="0.5588" dy="1.27" layer="1"/>
<rectangle x1="1.7714" y1="-4.5466" x2="2.2286" y2="-3.556" layer="51"/>
<smd name="15" x="2" y="-4.2926" dx="0.5588" dy="1.27" layer="1"/>
<rectangle x1="2.5714" y1="-4.5466" x2="3.0286" y2="-3.556" layer="51"/>
<smd name="16" x="2.8" y="-4.2926" dx="0.5588" dy="1.27" layer="1"/>
<rectangle x1="3.556" y1="-3.0286" x2="4.5466" y2="-2.5714" layer="51"/>
<smd name="17" x="4.2926" y="-2.8" dx="1.27" dy="0.5588" layer="1"/>
<rectangle x1="3.556" y1="-2.2286" x2="4.5466" y2="-1.7714" layer="51"/>
<smd name="18" x="4.2926" y="-2" dx="1.27" dy="0.5588" layer="1"/>
<rectangle x1="3.556" y1="-1.4286" x2="4.5466" y2="-0.9714" layer="51"/>
<smd name="19" x="4.2926" y="-1.2" dx="1.27" dy="0.5588" layer="1"/>
<rectangle x1="3.556" y1="-0.6286" x2="4.5466" y2="-0.1714" layer="51"/>
<smd name="20" x="4.2926" y="-0.4" dx="1.27" dy="0.5588" layer="1"/>
<rectangle x1="3.556" y1="0.1714" x2="4.5466" y2="0.6286" layer="51"/>
<smd name="21" x="4.2926" y="0.4" dx="1.27" dy="0.5588" layer="1"/>
<rectangle x1="3.556" y1="0.9714" x2="4.5466" y2="1.4286" layer="51"/>
<smd name="22" x="4.2926" y="1.2" dx="1.27" dy="0.5588" layer="1"/>
<rectangle x1="3.556" y1="1.7714" x2="4.5466" y2="2.2286" layer="51"/>
<smd name="23" x="4.2926" y="2" dx="1.27" dy="0.5588" layer="1"/>
<rectangle x1="3.556" y1="2.5714" x2="4.5466" y2="3.0286" layer="51"/>
<smd name="24" x="4.2926" y="2.8" dx="1.27" dy="0.5588" layer="1"/>
<rectangle x1="2.5714" y1="3.556" x2="3.0286" y2="4.5466" layer="51"/>
<smd name="25" x="2.8" y="4.2926" dx="0.5588" dy="1.27" layer="1"/>
<rectangle x1="1.7714" y1="3.556" x2="2.2286" y2="4.5466" layer="51"/>
<smd name="26" x="2" y="4.2926" dx="0.5588" dy="1.27" layer="1"/>
<rectangle x1="0.9714" y1="3.556" x2="1.4286" y2="4.5466" layer="51"/>
<smd name="27" x="1.2" y="4.2926" dx="0.5588" dy="1.27" layer="1"/>
<rectangle x1="0.1714" y1="3.556" x2="0.6286" y2="4.5466" layer="51"/>
<smd name="28" x="0.4" y="4.2926" dx="0.5588" dy="1.27" layer="1"/>
<rectangle x1="-0.6286" y1="3.556" x2="-0.1714" y2="4.5466" layer="51"/>
<smd name="29" x="-0.4" y="4.2926" dx="0.5588" dy="1.27" layer="1"/>
<rectangle x1="-1.4286" y1="3.556" x2="-0.9714" y2="4.5466" layer="51"/>
<smd name="30" x="-1.2" y="4.2926" dx="0.5588" dy="1.27" layer="1"/>
<rectangle x1="-2.2286" y1="3.556" x2="-1.7714" y2="4.5466" layer="51"/>
<smd name="31" x="-2" y="4.2926" dx="0.5588" dy="1.27" layer="1"/>
<rectangle x1="-3.0286" y1="3.556" x2="-2.5714" y2="4.5466" layer="51"/>
<smd name="32" x="-2.8" y="4.2926" dx="0.5588" dy="1.27" layer="1"/>
</package>
</packages>
<symbols>
<symbol name="ATMEGA48/88/168-TQFP/QFN32">
<wire x1="-25.4" y1="30.48" x2="25.4" y2="30.48" width="0.254" layer="94"/>
<wire x1="25.4" y1="30.48" x2="25.4" y2="-33.02" width="0.254" layer="94"/>
<wire x1="25.4" y1="-33.02" x2="-25.4" y2="-33.02" width="0.254" layer="94"/>
<wire x1="-25.4" y1="-33.02" x2="-25.4" y2="30.48" width="0.254" layer="94"/>
<pin name="PB5(SCK/PCINT5)" x="30.48" y="-30.48" length="middle" rot="R180"/>
<pin name="PB7(XTAL2/TOSC2/PCINT7)" x="-30.48" y="7.62" length="middle"/>
<pin name="PB6(XTAL1/TOSC1/PCINT6)" x="-30.48" y="12.7" length="middle"/>
<pin name="GND@1" x="-30.48" y="-27.94" length="middle" direction="pwr"/>
<pin name="VCC@1" x="-30.48" y="0" length="middle" direction="pwr"/>
<pin name="AGND" x="-30.48" y="-22.86" length="middle" direction="pwr"/>
<pin name="AREF" x="-30.48" y="-10.16" length="middle" direction="pas"/>
<pin name="AVCC" x="-30.48" y="-7.62" length="middle" direction="pas"/>
<pin name="PB4(MISO/PCINT4)" x="30.48" y="-27.94" length="middle" rot="R180"/>
<pin name="PB3(MOSI/OC2A/PCINT3)" x="30.48" y="-25.4" length="middle" rot="R180"/>
<pin name="PB2(SS/OC1B/PCINT2)" x="30.48" y="-22.86" length="middle" rot="R180"/>
<pin name="PB1(OC1A/PCINT1)" x="30.48" y="-20.32" length="middle" rot="R180"/>
<pin name="PB0(ICP1/CLKO/PCINT0)" x="30.48" y="-17.78" length="middle" rot="R180"/>
<pin name="PD7(AIN1/PCINT23)" x="30.48" y="-12.7" length="middle" rot="R180"/>
<pin name="PD6(AIN0/OC0A/PCINT22)" x="30.48" y="-10.16" length="middle" rot="R180"/>
<pin name="PD5(T1/OC0B/PCINT21)" x="30.48" y="-7.62" length="middle" rot="R180"/>
<pin name="PD4(T0/XCK/PCINT20)" x="30.48" y="-5.08" length="middle" rot="R180"/>
<pin name="PD3(INT1/OC2B/PCINT19)" x="30.48" y="-2.54" length="middle" rot="R180"/>
<pin name="PD2(INT0/PCINT18)" x="30.48" y="0" length="middle" rot="R180"/>
<pin name="PD1(TXD/PCINT17)" x="30.48" y="2.54" length="middle" rot="R180"/>
<pin name="PD0(RXD/PCINT16)" x="30.48" y="5.08" length="middle" rot="R180"/>
<pin name="PC5(ADC5/SCL/PCINT13)" x="30.48" y="15.24" length="middle" rot="R180"/>
<pin name="PC4(ADC4/SDA/PCINT12)" x="30.48" y="17.78" length="middle" rot="R180"/>
<pin name="PC3(ADC3/PCINT11)" x="30.48" y="20.32" length="middle" rot="R180"/>
<pin name="PC2(ADC2/PCINT10)" x="30.48" y="22.86" length="middle" rot="R180"/>
<pin name="PC1(ADC1/PCINT9)" x="30.48" y="25.4" length="middle" rot="R180"/>
<pin name="PC0(ADC0/PCINT8)" x="30.48" y="27.94" length="middle" rot="R180"/>
<pin name="PC6(/RESET/PCINT14)" x="-30.48" y="27.94" length="middle"/>
<text x="-25.4" y="33.02" size="1.778" layer="95" rot="MR180">&gt;NAME</text>
<text x="-25.4" y="-35.56" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND@2" x="-30.48" y="-30.48" length="middle" direction="pwr"/>
<pin name="VCC@2" x="-30.48" y="-2.54" length="middle" direction="pwr"/>
<pin name="ADC7" x="30.48" y="10.16" length="middle" direction="pas" rot="R180"/>
<pin name="ADC6" x="30.48" y="12.7" length="middle" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ATMEGA*8" prefix="IC">
<gates>
<gate name="1" symbol="ATMEGA48/88/168-TQFP/QFN32" x="0" y="0"/>
</gates>
<devices>
<device name="PA-MU" package="MLF32">
<connects>
<connect gate="1" pin="ADC6" pad="19"/>
<connect gate="1" pin="ADC7" pad="22"/>
<connect gate="1" pin="AGND" pad="21"/>
<connect gate="1" pin="AREF" pad="20"/>
<connect gate="1" pin="AVCC" pad="18"/>
<connect gate="1" pin="GND@1" pad="3"/>
<connect gate="1" pin="GND@2" pad="5"/>
<connect gate="1" pin="PB0(ICP1/CLKO/PCINT0)" pad="12"/>
<connect gate="1" pin="PB1(OC1A/PCINT1)" pad="13"/>
<connect gate="1" pin="PB2(SS/OC1B/PCINT2)" pad="14"/>
<connect gate="1" pin="PB3(MOSI/OC2A/PCINT3)" pad="15"/>
<connect gate="1" pin="PB4(MISO/PCINT4)" pad="16"/>
<connect gate="1" pin="PB5(SCK/PCINT5)" pad="17"/>
<connect gate="1" pin="PB6(XTAL1/TOSC1/PCINT6)" pad="7"/>
<connect gate="1" pin="PB7(XTAL2/TOSC2/PCINT7)" pad="8"/>
<connect gate="1" pin="PC0(ADC0/PCINT8)" pad="23"/>
<connect gate="1" pin="PC1(ADC1/PCINT9)" pad="24"/>
<connect gate="1" pin="PC2(ADC2/PCINT10)" pad="25"/>
<connect gate="1" pin="PC3(ADC3/PCINT11)" pad="26"/>
<connect gate="1" pin="PC4(ADC4/SDA/PCINT12)" pad="27"/>
<connect gate="1" pin="PC5(ADC5/SCL/PCINT13)" pad="28"/>
<connect gate="1" pin="PC6(/RESET/PCINT14)" pad="29"/>
<connect gate="1" pin="PD0(RXD/PCINT16)" pad="30"/>
<connect gate="1" pin="PD1(TXD/PCINT17)" pad="31"/>
<connect gate="1" pin="PD2(INT0/PCINT18)" pad="32"/>
<connect gate="1" pin="PD3(INT1/OC2B/PCINT19)" pad="1"/>
<connect gate="1" pin="PD4(T0/XCK/PCINT20)" pad="2"/>
<connect gate="1" pin="PD5(T1/OC0B/PCINT21)" pad="9"/>
<connect gate="1" pin="PD6(AIN0/OC0A/PCINT22)" pad="10"/>
<connect gate="1" pin="PD7(AIN1/PCINT23)" pad="11"/>
<connect gate="1" pin="VCC@1" pad="4"/>
<connect gate="1" pin="VCC@2" pad="6"/>
</connects>
<technologies>
<technology name="16"/>
<technology name="4"/>
<technology name="8"/>
</technologies>
</device>
<device name="PA-AU" package="TQFP32-08">
<connects>
<connect gate="1" pin="ADC6" pad="19"/>
<connect gate="1" pin="ADC7" pad="22"/>
<connect gate="1" pin="AGND" pad="21"/>
<connect gate="1" pin="AREF" pad="20"/>
<connect gate="1" pin="AVCC" pad="18"/>
<connect gate="1" pin="GND@1" pad="3"/>
<connect gate="1" pin="GND@2" pad="5"/>
<connect gate="1" pin="PB0(ICP1/CLKO/PCINT0)" pad="12"/>
<connect gate="1" pin="PB1(OC1A/PCINT1)" pad="13"/>
<connect gate="1" pin="PB2(SS/OC1B/PCINT2)" pad="14"/>
<connect gate="1" pin="PB3(MOSI/OC2A/PCINT3)" pad="15"/>
<connect gate="1" pin="PB4(MISO/PCINT4)" pad="16"/>
<connect gate="1" pin="PB5(SCK/PCINT5)" pad="17"/>
<connect gate="1" pin="PB6(XTAL1/TOSC1/PCINT6)" pad="7"/>
<connect gate="1" pin="PB7(XTAL2/TOSC2/PCINT7)" pad="8"/>
<connect gate="1" pin="PC0(ADC0/PCINT8)" pad="23"/>
<connect gate="1" pin="PC1(ADC1/PCINT9)" pad="24"/>
<connect gate="1" pin="PC2(ADC2/PCINT10)" pad="25"/>
<connect gate="1" pin="PC3(ADC3/PCINT11)" pad="26"/>
<connect gate="1" pin="PC4(ADC4/SDA/PCINT12)" pad="27"/>
<connect gate="1" pin="PC5(ADC5/SCL/PCINT13)" pad="28"/>
<connect gate="1" pin="PC6(/RESET/PCINT14)" pad="29"/>
<connect gate="1" pin="PD0(RXD/PCINT16)" pad="30"/>
<connect gate="1" pin="PD1(TXD/PCINT17)" pad="31"/>
<connect gate="1" pin="PD2(INT0/PCINT18)" pad="32"/>
<connect gate="1" pin="PD3(INT1/OC2B/PCINT19)" pad="1"/>
<connect gate="1" pin="PD4(T0/XCK/PCINT20)" pad="2"/>
<connect gate="1" pin="PD5(T1/OC0B/PCINT21)" pad="9"/>
<connect gate="1" pin="PD6(AIN0/OC0A/PCINT22)" pad="10"/>
<connect gate="1" pin="PD7(AIN1/PCINT23)" pad="11"/>
<connect gate="1" pin="VCC@1" pad="4"/>
<connect gate="1" pin="VCC@2" pad="6"/>
</connects>
<technologies>
<technology name="16"/>
<technology name="4"/>
<technology name="8"/>
</technologies>
</device>
<device name="P-AU" package="TQFP32-08">
<connects>
<connect gate="1" pin="ADC6" pad="19"/>
<connect gate="1" pin="ADC7" pad="22"/>
<connect gate="1" pin="AGND" pad="21"/>
<connect gate="1" pin="AREF" pad="20"/>
<connect gate="1" pin="AVCC" pad="18"/>
<connect gate="1" pin="GND@1" pad="3"/>
<connect gate="1" pin="GND@2" pad="5"/>
<connect gate="1" pin="PB0(ICP1/CLKO/PCINT0)" pad="12"/>
<connect gate="1" pin="PB1(OC1A/PCINT1)" pad="13"/>
<connect gate="1" pin="PB2(SS/OC1B/PCINT2)" pad="14"/>
<connect gate="1" pin="PB3(MOSI/OC2A/PCINT3)" pad="15"/>
<connect gate="1" pin="PB4(MISO/PCINT4)" pad="16"/>
<connect gate="1" pin="PB5(SCK/PCINT5)" pad="17"/>
<connect gate="1" pin="PB6(XTAL1/TOSC1/PCINT6)" pad="7"/>
<connect gate="1" pin="PB7(XTAL2/TOSC2/PCINT7)" pad="8"/>
<connect gate="1" pin="PC0(ADC0/PCINT8)" pad="23"/>
<connect gate="1" pin="PC1(ADC1/PCINT9)" pad="24"/>
<connect gate="1" pin="PC2(ADC2/PCINT10)" pad="25"/>
<connect gate="1" pin="PC3(ADC3/PCINT11)" pad="26"/>
<connect gate="1" pin="PC4(ADC4/SDA/PCINT12)" pad="27"/>
<connect gate="1" pin="PC5(ADC5/SCL/PCINT13)" pad="28"/>
<connect gate="1" pin="PC6(/RESET/PCINT14)" pad="29"/>
<connect gate="1" pin="PD0(RXD/PCINT16)" pad="30"/>
<connect gate="1" pin="PD1(TXD/PCINT17)" pad="31"/>
<connect gate="1" pin="PD2(INT0/PCINT18)" pad="32"/>
<connect gate="1" pin="PD3(INT1/OC2B/PCINT19)" pad="1"/>
<connect gate="1" pin="PD4(T0/XCK/PCINT20)" pad="2"/>
<connect gate="1" pin="PD5(T1/OC0B/PCINT21)" pad="9"/>
<connect gate="1" pin="PD6(AIN0/OC0A/PCINT22)" pad="10"/>
<connect gate="1" pin="PD7(AIN1/PCINT23)" pad="11"/>
<connect gate="1" pin="VCC@1" pad="4"/>
<connect gate="1" pin="VCC@2" pad="6"/>
</connects>
<technologies>
<technology name="32"/>
</technologies>
</device>
<device name="P-MU" package="MLF32">
<connects>
<connect gate="1" pin="ADC6" pad="19"/>
<connect gate="1" pin="ADC7" pad="22"/>
<connect gate="1" pin="AGND" pad="21"/>
<connect gate="1" pin="AREF" pad="20"/>
<connect gate="1" pin="AVCC" pad="18"/>
<connect gate="1" pin="GND@1" pad="3"/>
<connect gate="1" pin="GND@2" pad="5"/>
<connect gate="1" pin="PB0(ICP1/CLKO/PCINT0)" pad="12"/>
<connect gate="1" pin="PB1(OC1A/PCINT1)" pad="13"/>
<connect gate="1" pin="PB2(SS/OC1B/PCINT2)" pad="14"/>
<connect gate="1" pin="PB3(MOSI/OC2A/PCINT3)" pad="15"/>
<connect gate="1" pin="PB4(MISO/PCINT4)" pad="16"/>
<connect gate="1" pin="PB5(SCK/PCINT5)" pad="17"/>
<connect gate="1" pin="PB6(XTAL1/TOSC1/PCINT6)" pad="7"/>
<connect gate="1" pin="PB7(XTAL2/TOSC2/PCINT7)" pad="8"/>
<connect gate="1" pin="PC0(ADC0/PCINT8)" pad="23"/>
<connect gate="1" pin="PC1(ADC1/PCINT9)" pad="24"/>
<connect gate="1" pin="PC2(ADC2/PCINT10)" pad="25"/>
<connect gate="1" pin="PC3(ADC3/PCINT11)" pad="26"/>
<connect gate="1" pin="PC4(ADC4/SDA/PCINT12)" pad="27"/>
<connect gate="1" pin="PC5(ADC5/SCL/PCINT13)" pad="28"/>
<connect gate="1" pin="PC6(/RESET/PCINT14)" pad="29"/>
<connect gate="1" pin="PD0(RXD/PCINT16)" pad="30"/>
<connect gate="1" pin="PD1(TXD/PCINT17)" pad="31"/>
<connect gate="1" pin="PD2(INT0/PCINT18)" pad="32"/>
<connect gate="1" pin="PD3(INT1/OC2B/PCINT19)" pad="1"/>
<connect gate="1" pin="PD4(T0/XCK/PCINT20)" pad="2"/>
<connect gate="1" pin="PD5(T1/OC0B/PCINT21)" pad="9"/>
<connect gate="1" pin="PD6(AIN0/OC0A/PCINT22)" pad="10"/>
<connect gate="1" pin="PD7(AIN1/PCINT23)" pad="11"/>
<connect gate="1" pin="VCC@1" pad="4"/>
<connect gate="1" pin="VCC@2" pad="6"/>
</connects>
<technologies>
<technology name="32"/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="silabs">
<description>Silicon Laboratories&lt;p&gt;
C8051Fxxx family of mixed-signal microcontrollers integrates world-class analog,&lt;br&gt;
a high-speed pipelined 8051 CPU, ISP Flash Memory,&lt;br&gt;
and on-chip JTAG based debug in each device.&lt;br&gt;
The combination of configurable high-performance analog,&lt;br&gt;
100 MIPS 8051 core and in-system field programmability provides the user with complete design flexibility,&lt;br&gt;
improved time-to-market, superior system performance and greater end product differentiation.&lt;p&gt;

Source: http://www.silabs.com&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="MSOP10">
<wire x1="-1.5" y1="1.5" x2="1.5" y2="1.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.5" x2="1.5" y2="-1.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="-1.5" x2="-1.5" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-1.5" x2="-1.5" y2="1.5" width="0.127" layer="21"/>
<smd name="6" x="1" y="2.3" dx="0.27" dy="1.5" layer="1"/>
<smd name="7" x="0.5" y="2.3" dx="0.27" dy="1.5" layer="1"/>
<smd name="8" x="0" y="2.3" dx="0.27" dy="1.5" layer="1"/>
<smd name="9" x="-0.5" y="2.3" dx="0.27" dy="1.5" layer="1"/>
<smd name="10" x="-1" y="2.3" dx="0.27" dy="1.5" layer="1"/>
<rectangle x1="0.365" y1="1.57" x2="0.635" y2="2.51" layer="51"/>
<rectangle x1="-0.135" y1="1.57" x2="0.135" y2="2.51" layer="51"/>
<rectangle x1="-0.635" y1="1.57" x2="-0.365" y2="2.51" layer="51"/>
<rectangle x1="-1.135" y1="1.57" x2="-0.865" y2="2.51" layer="51"/>
<rectangle x1="0.865" y1="1.57" x2="1.135" y2="2.51" layer="51"/>
<rectangle x1="0.865" y1="-2.5" x2="1.135" y2="-1.56" layer="51"/>
<smd name="5" x="1" y="-2.31" dx="0.27" dy="1.5" layer="1"/>
<smd name="4" x="0.5" y="-2.31" dx="0.27" dy="1.5" layer="1"/>
<smd name="3" x="0" y="-2.31" dx="0.27" dy="1.5" layer="1"/>
<smd name="2" x="-0.5" y="-2.31" dx="0.27" dy="1.5" layer="1"/>
<smd name="1" x="-1" y="-2.31" dx="0.27" dy="1.5" layer="1"/>
<rectangle x1="0.365" y1="-2.5" x2="0.635" y2="-1.56" layer="51"/>
<rectangle x1="-0.135" y1="-2.5" x2="0.135" y2="-1.56" layer="51"/>
<rectangle x1="-0.635" y1="-2.5" x2="-0.365" y2="-1.56" layer="51"/>
<rectangle x1="-1.135" y1="-2.5" x2="-0.865" y2="-1.56" layer="51"/>
<text x="-2.31" y="3.31" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.5" y="-5" size="1.27" layer="27">&gt;VALUE</text>
<circle x="-0.9" y="-0.9" radius="0.2" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="SI5351A">
<wire x1="-7.62" y1="7.62" x2="7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="7.62" x2="7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="-7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-7.62" x2="-7.62" y2="7.62" width="0.254" layer="94"/>
<pin name="VDD" x="-12.7" y="5.08" length="middle" direction="pwr"/>
<pin name="XA" x="-12.7" y="2.54" length="middle" direction="pas"/>
<pin name="XB" x="-12.7" y="0" length="middle" direction="pas"/>
<pin name="SCL" x="-12.7" y="-2.54" length="middle" direction="in"/>
<pin name="SDA" x="-12.7" y="-5.08" length="middle"/>
<pin name="CLK2" x="12.7" y="-5.08" length="middle" direction="out" rot="R180"/>
<pin name="VDDO" x="12.7" y="-2.54" length="middle" direction="pwr" rot="R180"/>
<pin name="GND" x="12.7" y="0" length="middle" direction="pwr" rot="R180"/>
<pin name="CLK1" x="12.7" y="2.54" length="middle" direction="out" rot="R180"/>
<pin name="CLK0" x="12.7" y="5.08" length="middle" direction="out" rot="R180"/>
<text x="-2.54" y="12.7" size="1.27" layer="95">&gt;NAME</text>
<text x="-2.54" y="10.16" size="1.27" layer="95">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="SI5351" prefix="IC">
<gates>
<gate name="G$1" symbol="SI5351A" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MSOP10">
<connects>
<connect gate="G$1" pin="CLK0" pad="10"/>
<connect gate="G$1" pin="CLK1" pad="9"/>
<connect gate="G$1" pin="CLK2" pad="6"/>
<connect gate="G$1" pin="GND" pad="8"/>
<connect gate="G$1" pin="SCL" pad="4"/>
<connect gate="G$1" pin="SDA" pad="5"/>
<connect gate="G$1" pin="VDD" pad="1"/>
<connect gate="G$1" pin="VDDO" pad="7"/>
<connect gate="G$1" pin="XA" pad="2"/>
<connect gate="G$1" pin="XB" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="IC_Robotech">
<description>&lt;Digital to Analog Converters - DAC Single 12-bit DAC w/SPI interface&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="SOP65P640X120-24N">
<description>&lt;b&gt;PW (R-PDSO-G24)&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-2.938" y="3.575" dx="1.475" dy="0.45" layer="1"/>
<smd name="2" x="-2.938" y="2.925" dx="1.475" dy="0.45" layer="1"/>
<smd name="3" x="-2.938" y="2.275" dx="1.475" dy="0.45" layer="1"/>
<smd name="4" x="-2.938" y="1.625" dx="1.475" dy="0.45" layer="1"/>
<smd name="5" x="-2.938" y="0.975" dx="1.475" dy="0.45" layer="1"/>
<smd name="6" x="-2.938" y="0.325" dx="1.475" dy="0.45" layer="1"/>
<smd name="7" x="-2.938" y="-0.325" dx="1.475" dy="0.45" layer="1"/>
<smd name="8" x="-2.938" y="-0.975" dx="1.475" dy="0.45" layer="1"/>
<smd name="9" x="-2.938" y="-1.625" dx="1.475" dy="0.45" layer="1"/>
<smd name="10" x="-2.938" y="-2.275" dx="1.475" dy="0.45" layer="1"/>
<smd name="11" x="-2.938" y="-2.925" dx="1.475" dy="0.45" layer="1"/>
<smd name="12" x="-2.938" y="-3.575" dx="1.475" dy="0.45" layer="1"/>
<smd name="13" x="2.938" y="-3.575" dx="1.475" dy="0.45" layer="1"/>
<smd name="14" x="2.938" y="-2.925" dx="1.475" dy="0.45" layer="1"/>
<smd name="15" x="2.938" y="-2.275" dx="1.475" dy="0.45" layer="1"/>
<smd name="16" x="2.938" y="-1.625" dx="1.475" dy="0.45" layer="1"/>
<smd name="17" x="2.938" y="-0.975" dx="1.475" dy="0.45" layer="1"/>
<smd name="18" x="2.938" y="-0.325" dx="1.475" dy="0.45" layer="1"/>
<smd name="19" x="2.938" y="0.325" dx="1.475" dy="0.45" layer="1"/>
<smd name="20" x="2.938" y="0.975" dx="1.475" dy="0.45" layer="1"/>
<smd name="21" x="2.938" y="1.625" dx="1.475" dy="0.45" layer="1"/>
<smd name="22" x="2.938" y="2.275" dx="1.475" dy="0.45" layer="1"/>
<smd name="23" x="2.938" y="2.925" dx="1.475" dy="0.45" layer="1"/>
<smd name="24" x="2.938" y="3.575" dx="1.475" dy="0.45" layer="1"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-3.925" y1="4.2" x2="3.925" y2="4.2" width="0.05" layer="51"/>
<wire x1="3.925" y1="4.2" x2="3.925" y2="-4.2" width="0.05" layer="51"/>
<wire x1="3.925" y1="-4.2" x2="-3.925" y2="-4.2" width="0.05" layer="51"/>
<wire x1="-3.925" y1="-4.2" x2="-3.925" y2="4.2" width="0.05" layer="51"/>
<wire x1="-2.2" y1="3.9" x2="2.2" y2="3.9" width="0.1" layer="51"/>
<wire x1="2.2" y1="3.9" x2="2.2" y2="-3.9" width="0.1" layer="51"/>
<wire x1="2.2" y1="-3.9" x2="-2.2" y2="-3.9" width="0.1" layer="51"/>
<wire x1="-2.2" y1="-3.9" x2="-2.2" y2="3.9" width="0.1" layer="51"/>
<wire x1="-2.2" y1="3.25" x2="-1.55" y2="3.9" width="0.1" layer="51"/>
<wire x1="-1.85" y1="3.9" x2="1.85" y2="3.9" width="0.2" layer="21"/>
<wire x1="1.85" y1="3.9" x2="1.85" y2="-3.9" width="0.2" layer="21"/>
<wire x1="1.85" y1="-3.9" x2="-1.85" y2="-3.9" width="0.2" layer="21"/>
<wire x1="-1.85" y1="-3.9" x2="-1.85" y2="3.9" width="0.2" layer="21"/>
<wire x1="-3.675" y1="4.15" x2="-2.2" y2="4.15" width="0.2" layer="21"/>
</package>
<package name="SOP65P640X120-14N">
<description>&lt;b&gt;PW (R-PDSO-G14)&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-2.938" y="1.95" dx="1.475" dy="0.45" layer="1"/>
<smd name="2" x="-2.938" y="1.3" dx="1.475" dy="0.45" layer="1"/>
<smd name="3" x="-2.938" y="0.65" dx="1.475" dy="0.45" layer="1"/>
<smd name="4" x="-2.938" y="0" dx="1.475" dy="0.45" layer="1"/>
<smd name="5" x="-2.938" y="-0.65" dx="1.475" dy="0.45" layer="1"/>
<smd name="6" x="-2.938" y="-1.3" dx="1.475" dy="0.45" layer="1"/>
<smd name="7" x="-2.938" y="-1.95" dx="1.475" dy="0.45" layer="1"/>
<smd name="8" x="2.938" y="-1.95" dx="1.475" dy="0.45" layer="1"/>
<smd name="9" x="2.938" y="-1.3" dx="1.475" dy="0.45" layer="1"/>
<smd name="10" x="2.938" y="-0.65" dx="1.475" dy="0.45" layer="1"/>
<smd name="11" x="2.938" y="0" dx="1.475" dy="0.45" layer="1"/>
<smd name="12" x="2.938" y="0.65" dx="1.475" dy="0.45" layer="1"/>
<smd name="13" x="2.938" y="1.3" dx="1.475" dy="0.45" layer="1"/>
<smd name="14" x="2.938" y="1.95" dx="1.475" dy="0.45" layer="1"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-3.925" y1="2.8" x2="3.925" y2="2.8" width="0.05" layer="51"/>
<wire x1="3.925" y1="2.8" x2="3.925" y2="-2.8" width="0.05" layer="51"/>
<wire x1="3.925" y1="-2.8" x2="-3.925" y2="-2.8" width="0.05" layer="51"/>
<wire x1="-3.925" y1="-2.8" x2="-3.925" y2="2.8" width="0.05" layer="51"/>
<wire x1="-2.2" y1="2.5" x2="2.2" y2="2.5" width="0.1" layer="51"/>
<wire x1="2.2" y1="2.5" x2="2.2" y2="-2.5" width="0.1" layer="51"/>
<wire x1="2.2" y1="-2.5" x2="-2.2" y2="-2.5" width="0.1" layer="51"/>
<wire x1="-2.2" y1="-2.5" x2="-2.2" y2="2.5" width="0.1" layer="51"/>
<wire x1="-2.2" y1="1.85" x2="-1.55" y2="2.5" width="0.1" layer="51"/>
<wire x1="-1.85" y1="2.5" x2="1.85" y2="2.5" width="0.2" layer="21"/>
<wire x1="1.85" y1="2.5" x2="1.85" y2="-2.5" width="0.2" layer="21"/>
<wire x1="1.85" y1="-2.5" x2="-1.85" y2="-2.5" width="0.2" layer="21"/>
<wire x1="-1.85" y1="-2.5" x2="-1.85" y2="2.5" width="0.2" layer="21"/>
<wire x1="-3.675" y1="2.525" x2="-2.2" y2="2.525" width="0.2" layer="21"/>
</package>
<package name="SOT89">
<description>&lt;b&gt;SMALL OUTLINE TRANSISTOR&lt;/b&gt;</description>
<text x="-2.54" y="3.175" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.4051" y="-4.3449" size="1.27" layer="27">&gt;VALUE</text>
<smd name="1" x="-1.499" y="-1.981" dx="0.8" dy="1.4" layer="1"/>
<smd name="3" x="1.499" y="-1.981" dx="0.8" dy="1.4" layer="1"/>
<smd name="2" x="0" y="-1.727" dx="0.8" dy="1.9" layer="1" stop="no" cream="no"/>
<rectangle x1="-1.7272" y1="-2.1082" x2="-1.27" y2="-1.27" layer="51"/>
<rectangle x1="1.27" y1="-2.1082" x2="1.7272" y2="-1.27" layer="51"/>
<rectangle x1="-0.2794" y1="-2.1082" x2="0.2794" y2="-1.27" layer="51"/>
<polygon width="0.1998" layer="51">
<vertex x="-0.7874" y="1.3208"/>
<vertex x="-0.7874" y="1.5748"/>
<vertex x="-0.3556" y="2.0066"/>
<vertex x="0.3048" y="2.0066"/>
<vertex x="0.3556" y="2.0066"/>
<vertex x="0.7874" y="1.5748"/>
<vertex x="0.7874" y="1.2954"/>
<vertex x="-0.7874" y="1.2954"/>
</polygon>
<smd name="2.1" x="0" y="0.889" dx="2" dy="3.5" layer="1" roundness="75" cream="no"/>
<rectangle x1="-0.508" y1="-2.794" x2="0.508" y2="-1.27" layer="29"/>
<rectangle x1="-0.381" y1="-2.667" x2="0.381" y2="-1.3335" layer="31"/>
<text x="-2.54" y="3.175" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.4051" y="-4.3449" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="2.235" y1="-1.245" x2="-2.235" y2="-1.245" width="0.1524" layer="21"/>
<rectangle x1="-1.7272" y1="-2.1082" x2="-1.27" y2="-1.27" layer="51"/>
<rectangle x1="1.27" y1="-2.1082" x2="1.7272" y2="-1.27" layer="51"/>
<rectangle x1="-0.2794" y1="-2.1082" x2="0.2794" y2="-1.27" layer="51"/>
<wire x1="2.235" y1="1.219" x2="2.235" y2="-1.245" width="0.1524" layer="21"/>
<wire x1="-2.235" y1="-1.245" x2="-2.235" y2="1.219" width="0.1524" layer="21"/>
<wire x1="-2.235" y1="1.219" x2="2.235" y2="1.219" width="0.1524" layer="21"/>
<polygon width="0.1998" layer="51">
<vertex x="-0.7874" y="1.3208"/>
<vertex x="-0.7874" y="1.5748"/>
<vertex x="-0.3556" y="2.0066"/>
<vertex x="0.3048" y="2.0066"/>
<vertex x="0.3556" y="2.0066"/>
<vertex x="0.7874" y="1.5748"/>
<vertex x="0.7874" y="1.2954"/>
<vertex x="-0.7874" y="1.2954"/>
</polygon>
<wire x1="-0.7874" y1="1.5748" x2="-0.3556" y2="2.0066" width="0.2032" layer="51"/>
<wire x1="-0.3556" y1="2.0066" x2="0.3556" y2="2.0066" width="0.2032" layer="51"/>
<wire x1="0.3556" y1="2.0066" x2="0.7874" y2="1.5748" width="0.2032" layer="51"/>
<wire x1="0.7874" y1="1.5748" x2="0.7874" y2="1.2954" width="0.2032" layer="51"/>
<wire x1="0.7874" y1="1.2954" x2="-0.7874" y2="1.2954" width="0.2032" layer="51"/>
<wire x1="-0.7874" y1="1.2954" x2="-0.7874" y2="1.5748" width="0.2032" layer="51"/>
<wire x1="-0.254" y1="-1.143" x2="-0.254" y2="-0.762" width="0.254" layer="43"/>
<wire x1="-0.254" y1="-0.762" x2="-0.381" y2="-0.762" width="0.254" layer="43"/>
<wire x1="-0.381" y1="-0.762" x2="-0.889" y2="-0.254" width="0.254" layer="43"/>
<wire x1="-0.889" y1="-0.254" x2="-0.889" y2="2.159" width="0.254" layer="43"/>
<wire x1="-0.889" y1="2.159" x2="-0.381" y2="2.667" width="0.254" layer="43"/>
<wire x1="-0.381" y1="2.667" x2="0.381" y2="2.667" width="0.254" layer="43"/>
<wire x1="0.381" y1="2.667" x2="0.889" y2="2.159" width="0.254" layer="43"/>
<wire x1="0.889" y1="2.159" x2="0.889" y2="-0.254" width="0.254" layer="43"/>
<wire x1="0.889" y1="-0.254" x2="0.381" y2="-0.762" width="0.254" layer="43"/>
<wire x1="0.381" y1="-0.762" x2="0.254" y2="-0.762" width="0.254" layer="43"/>
<wire x1="0.254" y1="-0.762" x2="0.254" y2="-1.143" width="0.254" layer="43"/>
<wire x1="0" y1="-1.143" x2="0" y2="-0.762" width="0.254" layer="43"/>
<wire x1="0" y1="-0.762" x2="0" y2="0" width="0.762" layer="43"/>
<wire x1="0" y1="0" x2="0" y2="1.778" width="1.6764" layer="43"/>
<rectangle x1="-0.508" y1="-2.794" x2="0.508" y2="-1.27" layer="29"/>
<rectangle x1="-0.381" y1="-2.667" x2="0.381" y2="-1.3335" layer="31"/>
<wire x1="0" y1="0.1905" x2="0" y2="1.651" width="1.8" layer="31"/>
</package>
<package name="SOT23-5L" urn="urn:adsk.eagle:footprint:26811/1">
<description>&lt;b&gt;Small Outline Transistor&lt;/b&gt; SOT23-5L&lt;p&gt;
Source: www.st.com LD3985.pdf</description>
<wire x1="1.422" y1="0.81" x2="1.422" y2="-0.81" width="0.1524" layer="21"/>
<wire x1="1.422" y1="-0.81" x2="-1.422" y2="-0.81" width="0.1524" layer="51"/>
<wire x1="-1.422" y1="-0.81" x2="-1.422" y2="0.81" width="0.1524" layer="21"/>
<wire x1="-1.422" y1="0.81" x2="1.422" y2="0.81" width="0.1524" layer="51"/>
<wire x1="-0.522" y1="0.81" x2="0.522" y2="0.81" width="0.1524" layer="21"/>
<wire x1="-0.428" y1="-0.81" x2="-0.522" y2="-0.81" width="0.1524" layer="21"/>
<wire x1="0.522" y1="-0.81" x2="0.428" y2="-0.81" width="0.1524" layer="21"/>
<smd name="1" x="-0.95" y="-1.15" dx="0.6" dy="1.2" layer="1" stop="no" cream="no"/>
<smd name="2" x="0" y="-1.15" dx="0.6" dy="1.2" layer="1" stop="no" cream="no"/>
<smd name="3" x="0.95" y="-1.15" dx="0.6" dy="1.2" layer="1" stop="no" cream="no"/>
<smd name="4" x="0.95" y="1.15" dx="0.6" dy="1.2" layer="1" stop="no" cream="no"/>
<smd name="5" x="-0.95" y="1.15" dx="0.6" dy="1.2" layer="1" stop="no" cream="no"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.429" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.2" y1="-1.4" x2="-0.7" y2="-0.85" layer="51"/>
<rectangle x1="-0.25" y1="-1.4" x2="0.25" y2="-0.85" layer="51"/>
<rectangle x1="0.7" y1="-1.4" x2="1.2" y2="-0.85" layer="51"/>
<rectangle x1="0.7" y1="0.85" x2="1.2" y2="1.4" layer="51"/>
<rectangle x1="-1.2" y1="0.85" x2="-0.7" y2="1.4" layer="51"/>
<rectangle x1="-1.2875" y1="-1.8125" x2="-0.6125" y2="-0.5" layer="29"/>
<rectangle x1="-1.2125" y1="-1.7125" x2="-0.6875" y2="-0.5875" layer="31"/>
<rectangle x1="-0.3375" y1="-1.8125" x2="0.3375" y2="-0.5" layer="29"/>
<rectangle x1="-0.2625" y1="-1.7125" x2="0.2625" y2="-0.5875" layer="31"/>
<rectangle x1="0.6125" y1="-1.8125" x2="1.2875" y2="-0.5" layer="29"/>
<rectangle x1="0.6875" y1="-1.7125" x2="1.2125" y2="-0.5875" layer="31"/>
<rectangle x1="0.6125" y1="0.5" x2="1.2875" y2="1.8125" layer="29" rot="R180"/>
<rectangle x1="0.6875" y1="0.5875" x2="1.2125" y2="1.7125" layer="31" rot="R180"/>
<rectangle x1="-1.2875" y1="0.5" x2="-0.6125" y2="1.8125" layer="29" rot="R180"/>
<rectangle x1="-1.2125" y1="0.5875" x2="-0.6875" y2="1.7125" layer="31" rot="R180"/>
</package>
</packages>
<symbols>
<symbol name="BQ24703PW">
<wire x1="5.08" y1="2.54" x2="30.48" y2="2.54" width="0.254" layer="94"/>
<wire x1="30.48" y1="-30.48" x2="30.48" y2="2.54" width="0.254" layer="94"/>
<wire x1="30.48" y1="-30.48" x2="5.08" y2="-30.48" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-30.48" width="0.254" layer="94"/>
<text x="31.75" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="31.75" y="5.08" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="ACDET" x="0" y="0" length="middle"/>
<pin name="ACPRES" x="0" y="-2.54" length="middle"/>
<pin name="ACSEL" x="0" y="-5.08" length="middle"/>
<pin name="BATDEP" x="0" y="-7.62" length="middle"/>
<pin name="SRSET" x="0" y="-10.16" length="middle"/>
<pin name="ACSET" x="0" y="-12.7" length="middle"/>
<pin name="VREF" x="0" y="-15.24" length="middle"/>
<pin name="ENABLE" x="0" y="-17.78" length="middle"/>
<pin name="BATSET" x="0" y="-20.32" length="middle"/>
<pin name="COMP" x="0" y="-22.86" length="middle"/>
<pin name="ACN" x="0" y="-25.4" length="middle"/>
<pin name="ACP" x="0" y="-27.94" length="middle"/>
<pin name="!ACDRV" x="35.56" y="0" length="middle" rot="R180"/>
<pin name="!BATDRV" x="35.56" y="-2.54" length="middle" rot="R180"/>
<pin name="VCC" x="35.56" y="-5.08" length="middle" rot="R180"/>
<pin name="!PWM" x="35.56" y="-7.62" length="middle" rot="R180"/>
<pin name="VHSP" x="35.56" y="-10.16" length="middle" rot="R180"/>
<pin name="ALARM" x="35.56" y="-12.7" length="middle" rot="R180"/>
<pin name="VS" x="35.56" y="-15.24" length="middle" rot="R180"/>
<pin name="GND" x="35.56" y="-17.78" length="middle" rot="R180"/>
<pin name="SRP" x="35.56" y="-20.32" length="middle" rot="R180"/>
<pin name="SRN" x="35.56" y="-22.86" length="middle" rot="R180"/>
<pin name="IBAT" x="35.56" y="-25.4" length="middle" rot="R180"/>
<pin name="BATP" x="35.56" y="-27.94" length="middle" rot="R180"/>
</symbol>
<symbol name="LM2700MTX-ADJ_NOPB">
<wire x1="5.08" y1="2.54" x2="27.94" y2="2.54" width="0.254" layer="94"/>
<wire x1="27.94" y1="-17.78" x2="27.94" y2="2.54" width="0.254" layer="94"/>
<wire x1="27.94" y1="-17.78" x2="5.08" y2="-17.78" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-17.78" width="0.254" layer="94"/>
<text x="29.21" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="29.21" y="5.08" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="VC" x="0" y="0" length="middle"/>
<pin name="FB" x="0" y="-2.54" length="middle"/>
<pin name="!SHDN" x="0" y="-5.08" length="middle"/>
<pin name="AGND" x="0" y="-7.62" length="middle"/>
<pin name="PGND_1" x="0" y="-10.16" length="middle"/>
<pin name="PGND_2" x="0" y="-12.7" length="middle"/>
<pin name="PGND_3" x="0" y="-15.24" length="middle"/>
<pin name="NC_2" x="33.02" y="0" length="middle" rot="R180"/>
<pin name="FSLCT" x="33.02" y="-2.54" length="middle" rot="R180"/>
<pin name="VIN" x="33.02" y="-5.08" length="middle" rot="R180"/>
<pin name="NC_1" x="33.02" y="-7.62" length="middle" rot="R180"/>
<pin name="SW_3" x="33.02" y="-10.16" length="middle" rot="R180"/>
<pin name="SW_2" x="33.02" y="-12.7" length="middle" rot="R180"/>
<pin name="SW_1" x="33.02" y="-15.24" length="middle" rot="R180"/>
</symbol>
<symbol name="LINEAR-REGULATOR">
<pin name="IN" x="-12.7" y="2.54" length="middle" direction="pwr"/>
<pin name="OUT" x="12.7" y="2.54" length="middle" direction="sup" rot="R180"/>
<pin name="GND" x="0" y="-7.62" length="middle" direction="pwr" rot="R90"/>
<wire x1="-7.62" y1="5.08" x2="-7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-2.54" x2="7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="-2.54" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
</symbol>
<symbol name="LT1761">
<pin name="VIN" x="-15.24" y="2.54" length="middle" direction="pwr"/>
<pin name="!SHDN" x="-15.24" y="0" length="middle" direction="in"/>
<pin name="GND" x="-15.24" y="-2.54" length="middle" direction="pwr"/>
<pin name="BYP" x="10.16" y="0" length="middle" direction="pas" rot="R180"/>
<pin name="VOUT" x="10.16" y="2.54" length="middle" direction="out" rot="R180"/>
<wire x1="-10.16" y1="-5.08" x2="-10.16" y2="5.08" width="0.254" layer="94"/>
<wire x1="-10.16" y1="5.08" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="-10.16" y2="-5.08" width="0.254" layer="94"/>
<text x="-10.16" y="7.62" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="5.08" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="BQ24703PW" prefix="IC">
<description>&lt;b&gt;BQ24703PW, Battery Charge Controller, Lithium-Ion, 9.4mA, 24-Pin TSSOP&lt;/b&gt;&lt;p&gt;
Source: &lt;a href=""&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="BQ24703PW" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOP65P640X120-24N">
<connects>
<connect gate="G$1" pin="!ACDRV" pad="24"/>
<connect gate="G$1" pin="!BATDRV" pad="23"/>
<connect gate="G$1" pin="!PWM" pad="21"/>
<connect gate="G$1" pin="ACDET" pad="1"/>
<connect gate="G$1" pin="ACN" pad="11"/>
<connect gate="G$1" pin="ACP" pad="12"/>
<connect gate="G$1" pin="ACPRES" pad="2"/>
<connect gate="G$1" pin="ACSEL" pad="3"/>
<connect gate="G$1" pin="ACSET" pad="6"/>
<connect gate="G$1" pin="ALARM" pad="19"/>
<connect gate="G$1" pin="BATDEP" pad="4"/>
<connect gate="G$1" pin="BATP" pad="13"/>
<connect gate="G$1" pin="BATSET" pad="9"/>
<connect gate="G$1" pin="COMP" pad="10"/>
<connect gate="G$1" pin="ENABLE" pad="8"/>
<connect gate="G$1" pin="GND" pad="17"/>
<connect gate="G$1" pin="IBAT" pad="14"/>
<connect gate="G$1" pin="SRN" pad="15"/>
<connect gate="G$1" pin="SRP" pad="16"/>
<connect gate="G$1" pin="SRSET" pad="5"/>
<connect gate="G$1" pin="VCC" pad="22"/>
<connect gate="G$1" pin="VHSP" pad="20"/>
<connect gate="G$1" pin="VREF" pad="7"/>
<connect gate="G$1" pin="VS" pad="18"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="BQ24703PW, Battery Charge Controller, Lithium-Ion, 9.4mA, 24-Pin TSSOP" constant="no"/>
<attribute name="HEIGHT" value="1.2mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Texas Instruments" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="BQ24703PW" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="595-BQ24703PW" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="https://www.mouser.com/Search/Refine.aspx?Keyword=595-BQ24703PW" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LM2700MTX-ADJ_NOPB" prefix="IC">
<description>&lt;b&gt;Switching Voltage Regulators 600kHz/1.25MHz, 2.5A, Step-up PWM DC/DC Converter 14-TSSOP -40 to 125&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://www.ti.com/lit/ds/symlink/lm2700.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="LM2700MTX-ADJ_NOPB" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOP65P640X120-14N">
<connects>
<connect gate="G$1" pin="!SHDN" pad="3"/>
<connect gate="G$1" pin="AGND" pad="4"/>
<connect gate="G$1" pin="FB" pad="2"/>
<connect gate="G$1" pin="FSLCT" pad="13"/>
<connect gate="G$1" pin="NC_1" pad="11"/>
<connect gate="G$1" pin="NC_2" pad="14"/>
<connect gate="G$1" pin="PGND_1" pad="5"/>
<connect gate="G$1" pin="PGND_2" pad="6"/>
<connect gate="G$1" pin="PGND_3" pad="7"/>
<connect gate="G$1" pin="SW_1" pad="8"/>
<connect gate="G$1" pin="SW_2" pad="9"/>
<connect gate="G$1" pin="SW_3" pad="10"/>
<connect gate="G$1" pin="VC" pad="1"/>
<connect gate="G$1" pin="VIN" pad="12"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="Switching Voltage Regulators 600kHz/1.25MHz, 2.5A, Step-up PWM DC/DC Converter 14-TSSOP -40 to 125" constant="no"/>
<attribute name="HEIGHT" value="1.2mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Texas Instruments" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="LM2700MTX-ADJ/NOPB" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="926-LM2700MTXADJNOPB" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="https://www.mouser.com/Search/Refine.aspx?Keyword=926-LM2700MTXADJNOPB" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="NJW4181" prefix="IC">
<gates>
<gate name="G$1" symbol="LINEAR-REGULATOR" x="0" y="0"/>
</gates>
<devices>
<device name="U3" package="SOT89">
<connects>
<connect gate="G$1" pin="GND" pad="2 2.1"/>
<connect gate="G$1" pin="IN" pad="3"/>
<connect gate="G$1" pin="OUT" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LT1761" prefix="IC">
<gates>
<gate name="G$1" symbol="LT1761" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23-5L">
<connects>
<connect gate="G$1" pin="!SHDN" pad="3"/>
<connect gate="G$1" pin="BYP" pad="4"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="VIN" pad="1"/>
<connect gate="G$1" pin="VOUT" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Connector_Robotech">
<description>&lt;h3&gt;RoboTech EAGLE Library&lt;/h3&gt;
Connector Library &lt;br&gt;
$Rev: 25542 $
&lt;p&gt;
Since 2007&lt;br&gt;
by RoboTech&lt;br&gt;
Jun'ichi Takisawa&lt;br&gt;
Hiroki Yabe&lt;br&gt;
Katsuhiko Nishimra&lt;br&gt;
Takuo Sawada&lt;br&gt;
Hideo Tanida&lt;br&gt;
Makoto Shimazu&lt;br&gt;
Mayu Kojima&lt;br&gt;
Takefumi Hiraki&lt;br&gt;
Soichiro Iwataki&lt;br&gt;
&lt;/p&gt;</description>
<packages>
<package name="AQM1248A-RN">
<pad name="9" x="0" y="0" drill="0.6" shape="long" rot="R90"/>
<pad name="10" x="-1.27" y="0" drill="0.6" shape="long" rot="R90"/>
<pad name="8" x="1.27" y="0" drill="0.6" shape="long" rot="R90"/>
<pad name="11" x="-2.54" y="0" drill="0.6" shape="long" rot="R90"/>
<pad name="7" x="2.54" y="0" drill="0.6" shape="long" rot="R90"/>
<pad name="12" x="-3.81" y="0" drill="0.6" shape="long" rot="R90"/>
<pad name="6" x="3.81" y="0" drill="0.6" shape="long" rot="R90"/>
<pad name="13" x="-5.08" y="0" drill="0.6" shape="long" rot="R90"/>
<pad name="5" x="5.08" y="0" drill="0.6" shape="long" rot="R90"/>
<pad name="14" x="-6.35" y="0" drill="0.6" shape="long" rot="R90"/>
<pad name="4" x="6.35" y="0" drill="0.6" shape="long" rot="R90"/>
<pad name="15" x="-7.62" y="0" drill="0.6" shape="long" rot="R90"/>
<pad name="3" x="7.62" y="0" drill="0.6" shape="long" rot="R90"/>
<pad name="16" x="-8.89" y="0" drill="0.6" shape="long" rot="R90"/>
<pad name="2" x="8.89" y="0" drill="0.6" shape="long" rot="R90"/>
<pad name="17" x="-10.16" y="0" drill="0.6" shape="long" rot="R90"/>
<pad name="1" x="10.16" y="0" drill="0.6" shape="long" rot="R90"/>
<wire x1="-17" y1="0" x2="-17" y2="6.5" width="0.127" layer="21"/>
<wire x1="-17" y1="6.5" x2="-17" y2="20" width="0.127" layer="21"/>
<wire x1="-17" y1="20" x2="17" y2="20" width="0.127" layer="21"/>
<wire x1="17" y1="20" x2="17" y2="6.5" width="0.127" layer="21"/>
<wire x1="17" y1="6.5" x2="17" y2="0" width="0.127" layer="21"/>
<wire x1="17" y1="0" x2="-17" y2="0" width="0.127" layer="21"/>
<wire x1="-17" y1="6.5" x2="17" y2="6.5" width="0.127" layer="21"/>
<wire x1="-16" y1="8" x2="-16" y2="19" width="0.127" layer="21"/>
<wire x1="-16" y1="19" x2="16" y2="19" width="0.127" layer="21"/>
<wire x1="16" y1="19" x2="16" y2="8" width="0.127" layer="21"/>
<wire x1="16" y1="8" x2="-16" y2="8" width="0.127" layer="21"/>
<text x="-16.51" y="20.32" size="1.27" layer="25">&gt;NAME</text>
<text x="6.35" y="20.32" size="1.27" layer="25">&gt;VALUE</text>
</package>
<package name="5251-02-A">
<description>&lt;b&gt;MOLEX 2.54mm KK RA CONNECTOR&lt;/b&gt;
&lt;br&gt;Fixed Orientation</description>
<text x="2.1321" y="-3.6561" size="1.016" layer="25" ratio="14" rot="R180">&gt;NAME</text>
<pad name="2" x="-1.27" y="0" drill="1" shape="long" rot="R270"/>
<pad name="1" x="1.27" y="0" drill="1" shape="long" rot="R270"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51" rot="R180"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51" rot="R180"/>
<text x="-2.8941" y="-2.8941" size="0.8128" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<wire x1="-1.27" y1="1.905" x2="1.27" y2="1.905" width="0.254" layer="21"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51" rot="R180"/>
<wire x1="1.27" y1="1.905" x2="2.54" y2="1.905" width="0.254" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-1.27" y2="9.906" width="0.254" layer="21"/>
<wire x1="-1.27" y1="9.906" x2="-1.016" y2="10.414" width="0.254" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.27" y2="9.906" width="0.254" layer="21"/>
<wire x1="1.27" y1="9.906" x2="1.016" y2="10.414" width="0.254" layer="21"/>
<wire x1="-1.016" y1="10.414" x2="1.016" y2="10.414" width="0.254" layer="21"/>
<wire x1="-2.54" y1="-3.175" x2="-2.54" y2="1.905" width="0.254" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-1.27" y2="1.905" width="0.254" layer="21"/>
<wire x1="2.54" y1="1.905" x2="2.54" y2="-3.175" width="0.254" layer="21"/>
<wire x1="2.54" y1="-3.175" x2="-2.54" y2="-3.175" width="0.254" layer="21"/>
<rectangle x1="-1.524" y1="0.254" x2="-1.016" y2="8.382" layer="51"/>
<rectangle x1="1.016" y1="0.254" x2="1.524" y2="8.382" layer="51"/>
<wire x1="-2.54" y1="-0.762" x2="2.54" y2="-0.762" width="0.254" layer="48"/>
<wire x1="-1.27" y1="2.54" x2="1.27" y2="2.54" width="0.3" layer="41"/>
<text x="2.286" y="-2.794" size="1.27" layer="51" rot="R90">1</text>
<text x="-1.016" y="-2.794" size="1.27" layer="51" rot="R90">2</text>
</package>
<package name="5251-02-S">
<description>&lt;b&gt;MOLEX 2.54mm KK  CONNECTOR&lt;/b&gt;</description>
<text x="-2.5131" y="3.2751" size="1.016" layer="25" ratio="14">&gt;NAME</text>
<text x="2.032" y="-2.032" size="1.27" layer="51" ratio="14" rot="R90">1</text>
<text x="-0.762" y="-2.032" size="1.27" layer="51" ratio="14" rot="R90">2</text>
<pad name="2" x="-1.27" y="0" drill="1" shape="octagon" rot="R90"/>
<pad name="1" x="1.27" y="0" drill="1" shape="octagon" rot="R90"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<text x="2.5131" y="-3.5291" size="0.8128" layer="27" ratio="10" rot="R180">&gt;VALUE</text>
<wire x1="-2.54" y1="2.921" x2="-1.27" y2="2.921" width="0.254" layer="21"/>
<wire x1="-1.27" y1="2.921" x2="-1.016" y2="2.921" width="0.254" layer="21"/>
<wire x1="-1.016" y1="2.921" x2="1.016" y2="2.921" width="0.254" layer="21"/>
<wire x1="1.016" y1="2.921" x2="1.27" y2="2.921" width="0.254" layer="21"/>
<wire x1="1.27" y1="2.921" x2="2.54" y2="2.921" width="0.254" layer="21"/>
<wire x1="-2.54" y1="2.921" x2="-2.54" y2="-2.921" width="0.254" layer="21"/>
<wire x1="2.54" y1="2.921" x2="2.54" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-2.54" y1="-2.921" x2="-1.905" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-1.905" y1="-2.921" x2="-0.635" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-1.905" y1="-2.921" x2="-1.905" y2="-2.286" width="0.254" layer="21"/>
<wire x1="-1.905" y1="-2.286" x2="-0.635" y2="-2.286" width="0.254" layer="21"/>
<wire x1="-0.635" y1="-2.286" x2="-0.635" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-0.635" y1="-2.921" x2="0.635" y2="-2.921" width="0.254" layer="21"/>
<wire x1="0.635" y1="-2.921" x2="1.905" y2="-2.921" width="0.254" layer="21"/>
<wire x1="0.635" y1="-2.921" x2="0.635" y2="-2.286" width="0.254" layer="21"/>
<wire x1="0.635" y1="-2.286" x2="1.905" y2="-2.286" width="0.254" layer="21"/>
<wire x1="1.905" y1="-2.286" x2="1.905" y2="-2.921" width="0.254" layer="21"/>
<wire x1="1.905" y1="-2.921" x2="2.54" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-1.27" y2="2.921" width="0.254" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-1.016" y2="1.905" width="0.254" layer="21"/>
<wire x1="-1.016" y1="1.905" x2="1.016" y2="1.905" width="0.254" layer="21"/>
<wire x1="1.016" y1="1.905" x2="1.27" y2="1.905" width="0.254" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.27" y2="2.921" width="0.254" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-1.016" y2="1.397" width="0.254" layer="21"/>
<wire x1="-1.016" y1="2.921" x2="-1.016" y2="1.905" width="0.254" layer="21"/>
<wire x1="-1.016" y1="1.397" x2="1.016" y2="1.397" width="0.254" layer="21"/>
<wire x1="1.016" y1="1.397" x2="1.27" y2="1.905" width="0.254" layer="21"/>
<wire x1="1.016" y1="2.921" x2="1.016" y2="1.905" width="0.254" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="1.27" y2="2.54" width="0.3" layer="41"/>
</package>
<package name="D3200S-2-TAB-HORIZONTAL">
<pad name="1" x="2.54" y="0" drill="1.1" shape="long" rot="R90"/>
<pad name="2" x="-2.54" y="0" drill="1.1" shape="long" rot="R270"/>
<text x="-5.715" y="14.605" size="3.81" layer="21" rot="SR0">AMP</text>
<text x="-2.8575" y="12.7" size="1.27" layer="21" rot="SR0">D-3200</text>
<text x="-8.89" y="24.13" size="1.27" layer="25">&gt;NAME</text>
<wire x1="-9.55" y1="-0.6" x2="9.55" y2="-0.6" width="0.127" layer="27"/>
<wire x1="9.55" y1="-0.6" x2="9.55" y2="0" width="0.127" layer="27"/>
<wire x1="9.55" y1="0" x2="9.55" y2="21.5" width="0.127" layer="27"/>
<wire x1="9.55" y1="21.5" x2="-9.55" y2="21.5" width="0.127" layer="27"/>
<wire x1="-9.55" y1="21.5" x2="-9.55" y2="0" width="0.127" layer="27"/>
<text x="-8.89" y="22.225" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-9.55" y1="0" x2="-9.55" y2="-0.6" width="0.127" layer="27"/>
<wire x1="-9.55" y1="0" x2="9.55" y2="0" width="0.127" layer="27"/>
<hole x="-6.35" y="7.62" drill="3.2"/>
<hole x="6.35" y="7.62" drill="3.2"/>
<wire x1="-8.255" y1="6.858" x2="-7.112" y2="5.715" width="0.508" layer="42"/>
<wire x1="-7.112" y1="5.715" x2="-5.588" y2="5.715" width="0.508" layer="42"/>
<wire x1="-5.588" y1="5.715" x2="-4.445" y2="6.858" width="0.508" layer="42"/>
<wire x1="-4.445" y1="6.858" x2="-4.445" y2="8.382" width="0.508" layer="42"/>
<wire x1="-4.445" y1="8.382" x2="-5.588" y2="9.525" width="0.508" layer="42"/>
<wire x1="-5.588" y1="9.525" x2="-7.112" y2="9.525" width="0.508" layer="42"/>
<wire x1="-7.112" y1="9.525" x2="-8.255" y2="8.382" width="0.508" layer="42"/>
<wire x1="-8.255" y1="8.382" x2="-8.255" y2="6.858" width="0.508" layer="42"/>
<wire x1="4.445" y1="6.858" x2="4.445" y2="8.382" width="0.508" layer="42"/>
<wire x1="4.445" y1="8.382" x2="5.588" y2="9.525" width="0.508" layer="42"/>
<wire x1="5.588" y1="9.525" x2="7.112" y2="9.525" width="0.508" layer="42"/>
<wire x1="7.112" y1="9.525" x2="8.255" y2="8.382" width="0.508" layer="42"/>
<wire x1="8.255" y1="8.382" x2="8.255" y2="6.858" width="0.508" layer="42"/>
<wire x1="8.255" y1="6.858" x2="7.112" y2="5.715" width="0.508" layer="42"/>
<wire x1="7.112" y1="5.715" x2="5.588" y2="5.715" width="0.508" layer="42"/>
<wire x1="5.588" y1="5.715" x2="4.445" y2="6.858" width="0.508" layer="42"/>
</package>
<package name="D3200S-2-TAB-VERTICAL">
<pad name="2" x="-2.54" y="0" drill="1" shape="long" rot="R270"/>
<pad name="1" x="2.54" y="0" drill="1" shape="long" rot="R270"/>
<text x="-9.525" y="6.985" size="1.27" layer="25">&gt;NAME</text>
<wire x1="-9.55" y1="4.7" x2="9.55" y2="4.7" width="0.127" layer="21"/>
<wire x1="9.55" y1="4.7" x2="9.55" y2="-4.7" width="0.127" layer="21"/>
<wire x1="9.55" y1="-4.7" x2="-9.55" y2="-4.7" width="0.127" layer="21"/>
<wire x1="-9.55" y1="-4.7" x2="-9.55" y2="4.7" width="0.127" layer="21"/>
<wire x1="-8.5" y1="3.68" x2="8.5" y2="3.68" width="0.127" layer="21"/>
<wire x1="8.5" y1="3.68" x2="8.5" y2="-3.68" width="0.127" layer="21"/>
<wire x1="8.5" y1="-3.68" x2="-8.5" y2="-3.68" width="0.127" layer="21"/>
<wire x1="-8.5" y1="-3.68" x2="-8.5" y2="3.68" width="0.127" layer="21"/>
<text x="-9.525" y="5.08" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-9.525" y1="0.555" x2="-8.525" y2="0.555" width="0.127" layer="21"/>
<wire x1="-8.525" y1="-1.445" x2="-9.525" y2="-1.445" width="0.127" layer="21"/>
<hole x="-5.715" y="-1.27" drill="2"/>
<wire x1="-6.985" y1="-1.778" x2="-6.985" y2="-0.762" width="0.508" layer="42"/>
<wire x1="-6.985" y1="-0.762" x2="-6.223" y2="0" width="0.508" layer="42"/>
<wire x1="-6.223" y1="0" x2="-5.207" y2="0" width="0.508" layer="42"/>
<wire x1="-5.207" y1="0" x2="-4.445" y2="-0.762" width="0.508" layer="42"/>
<wire x1="-4.445" y1="-0.762" x2="-4.445" y2="-1.778" width="0.508" layer="42"/>
<wire x1="-4.445" y1="-1.778" x2="-5.207" y2="-2.54" width="0.508" layer="42"/>
<wire x1="-5.207" y1="-2.54" x2="-6.223" y2="-2.54" width="0.508" layer="42"/>
<wire x1="-6.223" y1="-2.54" x2="-6.985" y2="-1.778" width="0.508" layer="42"/>
<hole x="5.715" y="-1.27" drill="2"/>
<wire x1="4.445" y1="-1.778" x2="4.445" y2="-0.762" width="0.508" layer="42"/>
<wire x1="4.445" y1="-0.762" x2="5.207" y2="0" width="0.508" layer="42"/>
<wire x1="5.207" y1="0" x2="6.223" y2="0" width="0.508" layer="42"/>
<wire x1="6.223" y1="0" x2="6.985" y2="-0.762" width="0.508" layer="42"/>
<wire x1="6.985" y1="-0.762" x2="6.985" y2="-1.778" width="0.508" layer="42"/>
<wire x1="6.985" y1="-1.778" x2="6.223" y2="-2.54" width="0.508" layer="42"/>
<wire x1="6.223" y1="-2.54" x2="5.207" y2="-2.54" width="0.508" layer="42"/>
<wire x1="5.207" y1="-2.54" x2="4.445" y2="-1.778" width="0.508" layer="42"/>
</package>
<package name="D3200S-2-TAB-VERTICAL-AND-HORIZONTAL">
<pad name="1" x="2.54" y="0" drill="1.1" shape="long" rot="R90"/>
<pad name="2" x="-2.54" y="0" drill="1.1" shape="long" rot="R270"/>
<text x="-5.715" y="14.605" size="3.81" layer="21" rot="SR0">AMP</text>
<text x="-2.8575" y="12.7" size="1.27" layer="21" rot="SR0">D-3200</text>
<text x="-8.89" y="24.13" size="1.27" layer="25">&gt;NAME</text>
<wire x1="-9.55" y1="-0.6" x2="9.55" y2="-0.6" width="0.127" layer="27"/>
<wire x1="9.55" y1="-0.6" x2="9.55" y2="0" width="0.127" layer="27"/>
<wire x1="9.55" y1="0" x2="9.55" y2="21.5" width="0.127" layer="27"/>
<wire x1="9.55" y1="21.5" x2="-9.55" y2="21.5" width="0.127" layer="27"/>
<wire x1="-9.55" y1="21.5" x2="-9.55" y2="0" width="0.127" layer="27"/>
<text x="-8.89" y="22.225" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-9.55" y1="0" x2="-9.55" y2="-0.6" width="0.127" layer="27"/>
<wire x1="-9.55" y1="0" x2="9.55" y2="0" width="0.127" layer="27"/>
<wire x1="-9.55" y1="-4.7" x2="9.55" y2="-4.7" width="0.127" layer="21"/>
<wire x1="9.55" y1="-4.7" x2="9.55" y2="4.7" width="0.127" layer="21"/>
<wire x1="9.55" y1="4.7" x2="-9.55" y2="4.7" width="0.127" layer="21"/>
<wire x1="-9.55" y1="4.7" x2="-9.55" y2="-4.7" width="0.127" layer="21"/>
<wire x1="8.5" y1="3.68" x2="8.5" y2="-3.68" width="0.127" layer="21"/>
<wire x1="8.5" y1="-3.68" x2="-8.5" y2="-3.68" width="0.127" layer="21"/>
<wire x1="-8.5" y1="-3.68" x2="-8.5" y2="3.68" width="0.127" layer="21"/>
<wire x1="-8.5" y1="3.68" x2="8.5" y2="3.68" width="0.127" layer="21"/>
<hole x="-6.35" y="7.62" drill="3.2"/>
<hole x="6.35" y="7.62" drill="3.2"/>
<hole x="-6.35" y="-1.27" drill="2"/>
<hole x="6.35" y="-1.27" drill="2"/>
<wire x1="-8.255" y1="6.858" x2="-8.255" y2="8.382" width="0.508" layer="42"/>
<wire x1="-8.255" y1="8.382" x2="-7.112" y2="9.525" width="0.508" layer="42"/>
<wire x1="-7.112" y1="9.525" x2="-5.588" y2="9.525" width="0.508" layer="42"/>
<wire x1="-5.588" y1="9.525" x2="-4.445" y2="8.382" width="0.508" layer="42"/>
<wire x1="-4.445" y1="8.382" x2="-4.445" y2="6.858" width="0.508" layer="42"/>
<wire x1="-4.445" y1="6.858" x2="-5.588" y2="5.715" width="0.508" layer="42"/>
<wire x1="-5.588" y1="5.715" x2="-7.112" y2="5.715" width="0.508" layer="42"/>
<wire x1="-7.112" y1="5.715" x2="-8.255" y2="6.858" width="0.508" layer="42"/>
<wire x1="4.445" y1="6.858" x2="4.445" y2="8.382" width="0.508" layer="42"/>
<wire x1="4.445" y1="8.382" x2="5.588" y2="9.525" width="0.508" layer="42"/>
<wire x1="5.588" y1="9.525" x2="7.112" y2="9.525" width="0.508" layer="42"/>
<wire x1="7.112" y1="9.525" x2="8.255" y2="8.382" width="0.508" layer="42"/>
<wire x1="8.255" y1="8.382" x2="8.255" y2="6.858" width="0.508" layer="42"/>
<wire x1="8.255" y1="6.858" x2="7.112" y2="5.715" width="0.508" layer="42"/>
<wire x1="7.112" y1="5.715" x2="5.588" y2="5.715" width="0.508" layer="42"/>
<wire x1="5.588" y1="5.715" x2="4.445" y2="6.858" width="0.508" layer="42"/>
<wire x1="-7.62" y1="-1.778" x2="-7.62" y2="-0.762" width="0.508" layer="42"/>
<wire x1="-7.62" y1="-0.762" x2="-6.858" y2="0" width="0.508" layer="42"/>
<wire x1="-6.858" y1="0" x2="-5.842" y2="0" width="0.508" layer="42"/>
<wire x1="-5.842" y1="0" x2="-5.08" y2="-0.762" width="0.508" layer="42"/>
<wire x1="-5.08" y1="-0.762" x2="-5.08" y2="-1.778" width="0.508" layer="42"/>
<wire x1="-5.08" y1="-1.778" x2="-5.842" y2="-2.54" width="0.508" layer="42"/>
<wire x1="-5.842" y1="-2.54" x2="-6.858" y2="-2.54" width="0.508" layer="42"/>
<wire x1="-6.858" y1="-2.54" x2="-7.62" y2="-1.778" width="0.508" layer="42"/>
<wire x1="5.08" y1="-1.778" x2="5.08" y2="-0.762" width="0.508" layer="42"/>
<wire x1="5.08" y1="-0.762" x2="5.842" y2="0" width="0.508" layer="42"/>
<wire x1="5.842" y1="0" x2="6.858" y2="0" width="0.508" layer="42"/>
<wire x1="6.858" y1="0" x2="7.62" y2="-0.762" width="0.508" layer="42"/>
<wire x1="7.62" y1="-0.762" x2="7.62" y2="-1.778" width="0.508" layer="42"/>
<wire x1="7.62" y1="-1.778" x2="6.858" y2="-2.54" width="0.508" layer="42"/>
<wire x1="6.858" y1="-2.54" x2="5.842" y2="-2.54" width="0.508" layer="42"/>
<wire x1="5.842" y1="-2.54" x2="5.08" y2="-1.778" width="0.508" layer="42"/>
</package>
<package name="2X03">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<pad name="1" x="-2.54" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="2" x="-2.54" y="1.27" drill="1.016" shape="octagon"/>
<pad name="3" x="0" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="4" x="0" y="1.27" drill="1.016" shape="octagon"/>
<wire x1="-3.81" y1="-1.905" x2="-3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-2.54" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.905" x2="-3.81" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="1.905" x2="-3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<rectangle x1="-2.794" y1="-1.524" x2="-2.286" y2="-1.016" layer="51"/>
<rectangle x1="-2.794" y1="1.016" x2="-2.286" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="1.016" x2="0.254" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="-1.524" x2="0.254" y2="-1.016" layer="51"/>
<text x="-3.81" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<wire x1="-0.635" y1="-2.54" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-2.54" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<pad name="5" x="2.54" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="6" x="2.54" y="1.27" drill="1.016" shape="octagon"/>
<wire x1="1.27" y1="-1.905" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-2.54" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="3.81" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.905" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<rectangle x1="2.286" y1="1.016" x2="2.794" y2="1.524" layer="51"/>
<rectangle x1="2.286" y1="-1.524" x2="2.794" y2="-1.016" layer="51"/>
<wire x1="1.905" y1="-2.54" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<text x="-3.81" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="ML6">
<pad name="1" x="-2.54" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="2" x="-2.54" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="3" x="0" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="4" x="0" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="5" x="2.54" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="6" x="2.54" y="1.27" drill="0.9144" shape="octagon"/>
<rectangle x1="-0.254" y1="1.016" x2="0.254" y2="1.524" layer="51"/>
<rectangle x1="-2.794" y1="1.016" x2="-2.286" y2="1.524" layer="51"/>
<rectangle x1="2.286" y1="1.016" x2="2.794" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="-1.524" x2="0.254" y2="-1.016" layer="51"/>
<rectangle x1="-2.794" y1="-1.524" x2="-2.286" y2="-1.016" layer="51"/>
<rectangle x1="2.286" y1="-1.524" x2="2.794" y2="-1.016" layer="51"/>
<wire x1="-6.35" y1="3.175" x2="6.35" y2="3.175" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-3.175" x2="6.35" y2="3.175" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="3.175" x2="-6.35" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="4.445" x2="-6.35" y2="4.445" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-4.445" x2="7.62" y2="4.445" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="4.445" x2="-7.62" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="2.032" y1="-2.413" x2="2.032" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="2.032" y1="-2.413" x2="-2.032" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="-2.032" y1="-3.175" x2="-2.032" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="-2.032" y1="-3.175" x2="-6.35" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="-2.032" y1="-3.175" x2="-2.032" y2="-3.429" width="0.1524" layer="21"/>
<wire x1="6.35" y1="4.445" x2="6.35" y2="4.699" width="0.1524" layer="21"/>
<wire x1="6.35" y1="4.699" x2="5.08" y2="4.699" width="0.1524" layer="21"/>
<wire x1="5.08" y1="4.445" x2="5.08" y2="4.699" width="0.1524" layer="21"/>
<wire x1="6.35" y1="4.445" x2="7.62" y2="4.445" width="0.1524" layer="21"/>
<wire x1="0.635" y1="4.699" x2="-0.635" y2="4.699" width="0.1524" layer="21"/>
<wire x1="0.635" y1="4.699" x2="0.635" y2="4.445" width="0.1524" layer="21"/>
<wire x1="0.635" y1="4.445" x2="5.08" y2="4.445" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="4.699" x2="-0.635" y2="4.445" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="4.699" x2="-6.35" y2="4.699" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="4.699" x2="-6.35" y2="4.445" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="4.699" x2="-5.08" y2="4.445" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="4.445" x2="-0.635" y2="4.445" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-4.445" x2="2.032" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="2.032" y1="-4.445" x2="-2.032" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-3.175" x2="2.032" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="2.032" y1="-3.175" x2="2.032" y2="-3.429" width="0.1524" layer="21"/>
<wire x1="2.032" y1="-3.429" x2="2.032" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="2.032" y1="-3.429" x2="6.604" y2="-3.429" width="0.0508" layer="21"/>
<wire x1="6.604" y1="-3.429" x2="6.604" y2="3.429" width="0.0508" layer="21"/>
<wire x1="6.604" y1="3.429" x2="-6.604" y2="3.429" width="0.0508" layer="21"/>
<wire x1="-6.604" y1="3.429" x2="-6.604" y2="-3.429" width="0.0508" layer="21"/>
<wire x1="-6.604" y1="-3.429" x2="-2.032" y2="-3.429" width="0.0508" layer="21"/>
<wire x1="-2.032" y1="-3.429" x2="-2.032" y2="-4.445" width="0.1524" layer="21"/>
<text x="-7.62" y="5.08" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="0.635" y="5.08" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<text x="-5.08" y="-1.905" size="1.27" layer="21" ratio="10">1</text>
<text x="-5.08" y="0.635" size="1.27" layer="21" ratio="10">2</text>
<wire x1="-2.032" y1="-4.445" x2="-2.54" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-4.318" x2="-2.54" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-4.318" x2="-3.81" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-4.445" x2="-3.81" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-4.445" x2="-7.62" y2="-4.445" width="0.1524" layer="21"/>
<text x="-0.381" y="-4.064" size="1.27" layer="21" ratio="10">6</text>
</package>
<package name="ML6L">
<pad name="1" x="-2.54" y="-6.35" drill="0.9144" shape="octagon"/>
<pad name="2" x="-2.54" y="-3.81" drill="0.9144" shape="octagon"/>
<pad name="3" x="0" y="-6.35" drill="0.9144" shape="octagon"/>
<pad name="4" x="0" y="-3.81" drill="0.9144" shape="octagon"/>
<pad name="5" x="2.54" y="-6.35" drill="0.9144" shape="octagon"/>
<pad name="6" x="2.54" y="-3.81" drill="0.9144" shape="octagon"/>
<text x="-4.6228" y="-6.7564" size="1.27" layer="21" ratio="10">1</text>
<text x="-4.6482" y="-4.7244" size="1.27" layer="21" ratio="10">2</text>
<text x="-7.6454" y="6.35" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<wire x1="-5.08" y1="5.207" x2="-2.54" y2="5.207" width="0.254" layer="21"/>
<wire x1="-2.54" y1="5.207" x2="-3.81" y2="2.667" width="0.254" layer="21"/>
<wire x1="-3.81" y1="2.667" x2="-5.08" y2="5.207" width="0.254" layer="21"/>
<text x="0.6096" y="6.35" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<wire x1="-2.159" y1="5.842" x2="-2.159" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="5.842" x2="2.159" y2="5.842" width="0.1524" layer="21"/>
<wire x1="2.159" y1="5.842" x2="2.159" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="2.159" y1="5.842" x2="7.62" y2="5.842" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="-2.159" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.651" x2="-2.159" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-3.048" x2="-1.905" y2="-3.048" width="0.1524" layer="51"/>
<wire x1="-1.905" y1="-3.048" x2="-0.635" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-3.048" x2="0.635" y2="-3.048" width="0.1524" layer="51"/>
<wire x1="0.635" y1="-3.048" x2="1.905" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-3.048" x2="3.175" y2="-3.048" width="0.1524" layer="51"/>
<rectangle x1="-0.254" y1="-0.635" x2="0.254" y2="5.207" layer="21"/>
<wire x1="0" y1="5.08" x2="0" y2="5.334" width="0.508" layer="21"/>
<wire x1="3.302" y1="5.207" x2="3.302" y2="2.413" width="0.1524" layer="21"/>
<wire x1="3.302" y1="2.413" x2="7.112" y2="2.413" width="0.1524" layer="21"/>
<wire x1="7.112" y1="5.207" x2="7.112" y2="2.413" width="0.1524" layer="21"/>
<wire x1="7.112" y1="5.207" x2="3.302" y2="5.207" width="0.1524" layer="21"/>
<rectangle x1="-4.953" y1="4.699" x2="-2.667" y2="5.207" layer="21"/>
<rectangle x1="-4.699" y1="4.191" x2="-2.921" y2="4.699" layer="21"/>
<rectangle x1="-4.445" y1="3.683" x2="-3.175" y2="4.191" layer="21"/>
<rectangle x1="-4.191" y1="3.175" x2="-3.429" y2="3.683" layer="21"/>
<rectangle x1="-3.937" y1="2.921" x2="-3.683" y2="3.175" layer="21"/>
<text x="4.572" y="3.048" size="1.524" layer="21" ratio="10">6</text>
<wire x1="7.62" y1="5.842" x2="7.62" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-3.048" x2="6.477" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="5.842" x2="-7.62" y2="5.842" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="5.842" x2="-7.62" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-3.048" x2="-6.477" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="5.461" y1="-3.048" x2="5.461" y2="-3.683" width="0.1524" layer="21"/>
<wire x1="5.461" y1="-3.048" x2="3.175" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="5.461" y1="-3.683" x2="6.477" y2="-3.683" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-3.048" x2="6.477" y2="-3.683" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-3.048" x2="5.461" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-5.461" y1="-3.048" x2="-5.461" y2="-3.683" width="0.1524" layer="21"/>
<wire x1="-5.461" y1="-3.048" x2="-3.175" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-5.461" y1="-3.683" x2="-6.477" y2="-3.683" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="-3.048" x2="-6.477" y2="-3.683" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="-3.048" x2="-5.461" y2="-3.048" width="0.1524" layer="21"/>
<rectangle x1="-2.794" y1="-4.699" x2="-2.286" y2="-3.048" layer="51"/>
<rectangle x1="-2.794" y1="-5.461" x2="-2.286" y2="-4.699" layer="21"/>
<rectangle x1="-2.794" y1="-6.604" x2="-2.286" y2="-5.461" layer="51"/>
<rectangle x1="-0.254" y1="-4.699" x2="0.254" y2="-3.048" layer="51"/>
<rectangle x1="-0.254" y1="-5.461" x2="0.254" y2="-4.699" layer="21"/>
<rectangle x1="-0.254" y1="-6.604" x2="0.254" y2="-5.461" layer="51"/>
<rectangle x1="2.286" y1="-4.699" x2="2.794" y2="-3.048" layer="51"/>
<rectangle x1="2.286" y1="-5.461" x2="2.794" y2="-4.699" layer="21"/>
<rectangle x1="2.286" y1="-6.604" x2="2.794" y2="-5.461" layer="51"/>
</package>
<package name="DCJACK">
<pad name="SIDE1" x="-2.54" y="0" drill="3.81" diameter="6.35"/>
<pad name="CENTER" x="5.08" y="0" drill="3.81" diameter="6.35"/>
<pad name="SIDE2" x="1.27" y="-5.08" drill="3.81"/>
<wire x1="5.08" y1="5.08" x2="5.08" y2="-5.08" width="0.127" layer="21"/>
<wire x1="5.08" y1="-5.08" x2="-7.62" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-7.62" y1="-5.08" x2="-7.62" y2="5.08" width="0.127" layer="21"/>
<wire x1="-7.62" y1="5.08" x2="5.08" y2="5.08" width="0.127" layer="21"/>
</package>
<package name="2DC-0415D200">
<smd name="2" x="3.25" y="6.5" dx="7" dy="5" layer="1"/>
<smd name="3" x="3.25" y="-6.5" dx="7" dy="5" layer="1"/>
<smd name="1" x="12.15" y="-0.63" dx="6.1" dy="3.8" layer="1"/>
<hole x="0" y="2.5" drill="2.2"/>
<hole x="0" y="-2.5" drill="2.2"/>
<wire x1="-3.5" y1="-4.5" x2="-3.5" y2="4.5" width="0.127" layer="21"/>
<wire x1="-3.5" y1="4.5" x2="11" y2="4.5" width="0.127" layer="21"/>
<wire x1="11" y1="4.5" x2="11" y2="-4.5" width="0.127" layer="21"/>
<wire x1="11" y1="-4.5" x2="-3.5" y2="-4.5" width="0.127" layer="21"/>
<text x="8.89" y="6.35" size="1.27" layer="25">&gt;NAME</text>
<text x="8.89" y="5.08" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="IL-G-7P-S3T2-SA">
<pad name="4" x="0" y="0" drill="1.1" shape="long" rot="R90"/>
<pad name="3" x="-2.54" y="0" drill="1.1" shape="long" rot="R90"/>
<pad name="5" x="2.54" y="0" drill="1.1" shape="long" rot="R90"/>
<pad name="2" x="-5.08" y="0" drill="1.1" shape="long" rot="R90"/>
<pad name="6" x="5.08" y="0" drill="1.1" shape="long" rot="R90"/>
<pad name="1" x="-7.62" y="0" drill="1.1" shape="long" rot="R90"/>
<pad name="7" x="7.62" y="0" drill="1.1" shape="long" rot="R90"/>
<wire x1="-9" y1="3.07" x2="9" y2="3.07" width="0.127" layer="21"/>
<wire x1="9" y1="3.07" x2="9" y2="2.54" width="0.127" layer="21"/>
<wire x1="9" y1="2.54" x2="9" y2="-2.93" width="0.127" layer="21"/>
<wire x1="9" y1="-2.93" x2="-9" y2="-2.93" width="0.127" layer="21"/>
<wire x1="-9" y1="-2.93" x2="-9" y2="2.54" width="0.127" layer="21"/>
<wire x1="-9" y1="2.54" x2="-9" y2="3.07" width="0.127" layer="21"/>
<wire x1="-9" y1="2.54" x2="9" y2="2.54" width="0.127" layer="21"/>
<text x="-8.89" y="5.08" size="1.27" layer="25">&gt;NAME</text>
<text x="-8.89" y="3.81" size="1.27" layer="27">&gt;VALUE</text>
<text x="-10.16" y="-2.54" size="1.27" layer="21">1</text>
</package>
</packages>
<symbols>
<symbol name="AQM1248A-RN">
<pin name="C1-" x="0" y="-5.08" length="middle" rot="R90"/>
<pin name="VOUT" x="-2.54" y="-5.08" length="middle" rot="R90"/>
<pin name="C2+" x="5.08" y="-5.08" length="middle" rot="R90"/>
<pin name="C1+" x="2.54" y="-5.08" length="middle" rot="R90"/>
<pin name="VSS" x="-5.08" y="-5.08" length="middle" rot="R90"/>
<pin name="VDD" x="-7.62" y="-5.08" length="middle" rot="R90"/>
<pin name="C2-" x="7.62" y="-5.08" length="middle" rot="R90"/>
<pin name="SDA" x="-10.16" y="-5.08" length="middle" rot="R90"/>
<pin name="V1" x="10.16" y="-5.08" length="middle" rot="R90"/>
<pin name="SCL" x="-12.7" y="-5.08" length="middle" rot="R90"/>
<pin name="V2" x="12.7" y="-5.08" length="middle" rot="R90"/>
<pin name="RS" x="-15.24" y="-5.08" length="middle" rot="R90"/>
<pin name="V3" x="15.24" y="-5.08" length="middle" rot="R90"/>
<pin name="!RST" x="-17.78" y="-5.08" length="middle" rot="R90"/>
<pin name="V4" x="17.78" y="-5.08" length="middle" rot="R90"/>
<pin name="!CS1B" x="-20.32" y="-5.08" length="middle" rot="R90"/>
<pin name="V5" x="20.32" y="-5.08" length="middle" rot="R90"/>
<wire x1="0" y1="0" x2="-22.86" y2="0" width="0.254" layer="94"/>
<wire x1="-22.86" y1="0" x2="-22.86" y2="10.16" width="0.254" layer="94"/>
<wire x1="-22.86" y1="10.16" x2="22.86" y2="10.16" width="0.254" layer="94"/>
<wire x1="22.86" y1="10.16" x2="22.86" y2="0" width="0.254" layer="94"/>
<wire x1="22.86" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<text x="-20.32" y="12.7" size="1.778" layer="95">&gt;NAME</text>
<text x="-20.32" y="10.16" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="M-1">
<pin name="S" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<text x="1.016" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<text x="1.016" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94"/>
</symbol>
<symbol name="M">
<pin name="S" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
</symbol>
<symbol name="PWRN">
<pin name="GND" x="0" y="-7.62" visible="pad" length="middle" direction="pwr" rot="R90"/>
<pin name="VCC" x="0" y="7.62" visible="pad" length="middle" direction="pwr" rot="R270"/>
<text x="-0.635" y="-0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="1.905" y="-5.842" size="1.27" layer="95" rot="R90">GND</text>
<text x="1.905" y="2.54" size="1.27" layer="95" rot="R90">VCC</text>
<text x="-0.635" y="-0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="1.905" y="-5.842" size="1.27" layer="95" rot="R90">GND</text>
<text x="1.905" y="2.54" size="1.27" layer="95" rot="R90">VCC</text>
</symbol>
<symbol name="DCJACK">
<pin name="1" x="5.08" y="2.54" length="middle" rot="R180"/>
<pin name="2" x="5.08" y="0" length="middle" rot="R180"/>
<pin name="3" x="5.08" y="-2.54" length="middle" rot="R180"/>
<text x="-2.54" y="5.08" size="1.27" layer="94">&gt;NAME</text>
<wire x1="-1.27" y1="3.81" x2="1.27" y2="3.81" width="0.254" layer="94"/>
<wire x1="1.27" y1="3.81" x2="1.27" y2="-3.81" width="0.254" layer="94"/>
<wire x1="1.27" y1="-3.81" x2="-1.27" y2="-3.81" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-3.81" x2="-1.27" y2="3.81" width="0.254" layer="94"/>
<text x="-2.54" y="5.08" size="1.27" layer="94">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="AQM1248" prefix="LCD">
<gates>
<gate name="G$1" symbol="AQM1248A-RN" x="0" y="0"/>
</gates>
<devices>
<device name="" package="AQM1248A-RN">
<connects>
<connect gate="G$1" pin="!CS1B" pad="1"/>
<connect gate="G$1" pin="!RST" pad="2"/>
<connect gate="G$1" pin="C1+" pad="10"/>
<connect gate="G$1" pin="C1-" pad="9"/>
<connect gate="G$1" pin="C2+" pad="11"/>
<connect gate="G$1" pin="C2-" pad="12"/>
<connect gate="G$1" pin="RS" pad="3"/>
<connect gate="G$1" pin="SCL" pad="4"/>
<connect gate="G$1" pin="SDA" pad="5"/>
<connect gate="G$1" pin="V1" pad="13"/>
<connect gate="G$1" pin="V2" pad="14"/>
<connect gate="G$1" pin="V3" pad="15"/>
<connect gate="G$1" pin="V4" pad="16"/>
<connect gate="G$1" pin="V5" pad="17"/>
<connect gate="G$1" pin="VDD" pad="6"/>
<connect gate="G$1" pin="VOUT" pad="8"/>
<connect gate="G$1" pin="VSS" pad="7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="POWER-SOURCE" prefix="CON">
<gates>
<gate name="_VCC" symbol="M-1" x="5.08" y="5.08"/>
<gate name="_GND" symbol="M-1" x="5.08" y="-5.08"/>
</gates>
<devices>
<device name="-M-A" package="5251-02-A">
<connects>
<connect gate="_GND" pin="S" pad="1"/>
<connect gate="_VCC" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-M-S" package="5251-02-S">
<connects>
<connect gate="_GND" pin="S" pad="1"/>
<connect gate="_VCC" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-D-H" package="D3200S-2-TAB-HORIZONTAL">
<connects>
<connect gate="_GND" pin="S" pad="1"/>
<connect gate="_VCC" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-D-V" package="D3200S-2-TAB-VERTICAL">
<connects>
<connect gate="_GND" pin="S" pad="1"/>
<connect gate="_VCC" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-D-V-H" package="D3200S-2-TAB-VERTICAL-AND-HORIZONTAL">
<connects>
<connect gate="_GND" pin="S" pad="1"/>
<connect gate="_VCC" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="AVRISP-NEWSYMBOL" prefix="CON">
<gates>
<gate name="SCK" symbol="M" x="2.54" y="2.54"/>
<gate name="MISO" symbol="M" x="2.54" y="0"/>
<gate name="MOSI" symbol="M" x="2.54" y="-2.54"/>
<gate name="RESET" symbol="M" x="2.54" y="-5.08"/>
<gate name="PWR" symbol="PWRN" x="17.78" y="-2.54"/>
</gates>
<devices>
<device name="-PH" package="2X03">
<connects>
<connect gate="MISO" pin="S" pad="1"/>
<connect gate="MOSI" pin="S" pad="4"/>
<connect gate="PWR" pin="GND" pad="6"/>
<connect gate="PWR" pin="VCC" pad="2"/>
<connect gate="RESET" pin="S" pad="5"/>
<connect gate="SCK" pin="S" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-ML6" package="ML6">
<connects>
<connect gate="MISO" pin="S" pad="1"/>
<connect gate="MOSI" pin="S" pad="4"/>
<connect gate="PWR" pin="GND" pad="6"/>
<connect gate="PWR" pin="VCC" pad="2"/>
<connect gate="RESET" pin="S" pad="5"/>
<connect gate="SCK" pin="S" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-ML6L" package="ML6L">
<connects>
<connect gate="MISO" pin="S" pad="1"/>
<connect gate="MOSI" pin="S" pad="4"/>
<connect gate="PWR" pin="GND" pad="6"/>
<connect gate="PWR" pin="VCC" pad="2"/>
<connect gate="RESET" pin="S" pad="5"/>
<connect gate="SCK" pin="S" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DCJACK" prefix="J">
<gates>
<gate name="G$1" symbol="DCJACK" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DCJACK">
<connects>
<connect gate="G$1" pin="1" pad="CENTER"/>
<connect gate="G$1" pin="2" pad="SIDE1"/>
<connect gate="G$1" pin="3" pad="SIDE2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD" package="2DC-0415D200">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RADIO-INTERCONNECT" prefix="CON">
<gates>
<gate name="PTT" symbol="M" x="2.54" y="7.62"/>
<gate name="VAGC" symbol="M" x="2.54" y="5.08"/>
<gate name="+12V" symbol="M" x="2.54" y="2.54"/>
<gate name="PWRSW" symbol="M" x="2.54" y="0"/>
<gate name="TXEN" symbol="M" x="2.54" y="-2.54"/>
<gate name="RXEN" symbol="M" x="2.54" y="-5.08"/>
<gate name="GND" symbol="M" x="2.54" y="-7.62"/>
</gates>
<devices>
<device name="" package="IL-G-7P-S3T2-SA">
<connects>
<connect gate="+12V" pin="S" pad="2"/>
<connect gate="GND" pin="S" pad="1"/>
<connect gate="PTT" pin="S" pad="3"/>
<connect gate="PWRSW" pin="S" pad="4"/>
<connect gate="RXEN" pin="S" pad="5"/>
<connect gate="TXEN" pin="S" pad="6"/>
<connect gate="VAGC" pin="S" pad="7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Transistor_Robotech">
<description>&lt;h3&gt;RoboTech EAGLE Library&lt;/h3&gt;
Transitor Library &lt;br&gt;
$Rev: 25542 $
&lt;p&gt;
Since 2007&lt;br&gt;
by RoboTech&lt;br&gt;
Jun'ichi Takisawa&lt;br&gt;
Hiroki Yabe&lt;br&gt;
Katsuhiko Nishimra&lt;br&gt;
&lt;/p&gt;</description>
<packages>
<package name="SOT23">
<description>&lt;b&gt;SOT-23&lt;/b&gt;</description>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<smd name="3" x="0" y="1.1" dx="1" dy="1.4" layer="1"/>
<smd name="2" x="0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<smd name="1" x="-0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.6604" width="0.1524" layer="21"/>
<wire x1="1.4224" y1="-0.6604" x2="-1.4224" y2="-0.6604" width="0.1524" layer="21"/>
<wire x1="-1.4224" y1="-0.6604" x2="-1.4224" y2="0.6604" width="0.1524" layer="21"/>
<wire x1="-1.4224" y1="0.6604" x2="1.4224" y2="0.6604" width="0.1524" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="PFET">
<pin name="D" x="5.08" y="2.54" visible="off" length="middle" direction="pas" rot="R180"/>
<pin name="S" x="5.08" y="-2.54" visible="off" length="middle" direction="pas" rot="R180"/>
<pin name="G" x="-5.08" y="-2.54" visible="off" length="short" direction="pas"/>
<text x="-2.54" y="0.635" size="1.778" layer="96" rot="R180">&gt;VALUE</text>
<text x="-2.54" y="3.175" size="1.778" layer="95" rot="R180">&gt;NAME</text>
<wire x1="-2.54" y1="-2.54" x2="-1.2192" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="0.762" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-0.762" width="0.254" layer="94"/>
<wire x1="0" y1="3.683" x2="0" y2="1.397" width="0.254" layer="94"/>
<wire x1="0.635" y1="0.635" x2="1.905" y2="0" width="0.254" layer="94"/>
<wire x1="0.635" y1="-0.635" x2="1.905" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="1.905" y2="0" width="0.1524" layer="94"/>
<wire x1="1.905" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.397" x2="0" y2="-3.683" width="0.254" layer="94"/>
<wire x1="-1.143" y1="2.54" x2="-1.143" y2="-2.54" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="A03401A">
<gates>
<gate name="G$1" symbol="PFET" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Supply_Robotech">
<description>&lt;h3&gt;RoboTech EAGLE Library&lt;/h3&gt;
Supply symbol library&lt;br&gt;
$Rev: 25542 $ 
&lt;p&gt;
since 2008&lt;br&gt;
by&lt;br&gt;
Takuo Sawada&lt;br&gt;
&lt;/p&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="+3V3">
<pin name="+3V3" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
<wire x1="1.27" y1="0.635" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="0.635" width="0.254" layer="94"/>
<text x="-2.54" y="0" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
</symbol>
<symbol name="+3V3INT">
<pin name="+5V" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
<wire x1="1.27" y1="0.635" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="0.635" width="0.254" layer="94"/>
<text x="-2.54" y="0" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
</symbol>
<symbol name="GND">
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-1.905" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="+3V3">
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+3V3INT">
<gates>
<gate name="G$1" symbol="+3V3INT" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Passive_Robotech">
<description>&lt;Encoders HORZ 24DET 24PULSE 15mm SHAFT SPST SW&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="1005">
<description>Metric Code Size 1005</description>
<smd name="1" x="-0.473" y="0" dx="0.8128" dy="0.4064" layer="1" rot="R90"/>
<smd name="2" x="0.473" y="0" dx="0.8128" dy="0.4064" layer="1" rot="R90"/>
<text x="-0.5" y="0.425" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-0.5" y="-1.45" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="0.508" y1="0.254" x2="0.508" y2="-0.254" width="0.127" layer="51"/>
<wire x1="0.508" y1="0.254" x2="-0.508" y2="0.254" width="0.127" layer="51"/>
<wire x1="-0.508" y1="0.254" x2="-0.508" y2="-0.254" width="0.127" layer="51"/>
<wire x1="-0.508" y1="-0.254" x2="0.508" y2="-0.254" width="0.127" layer="51"/>
</package>
<package name="1608">
<description>Metric Code Size 1608</description>
<smd name="1" x="-0.875" y="0" dx="1.016" dy="0.762" layer="1" rot="R90"/>
<smd name="2" x="0.875" y="0" dx="0.762" dy="1.016" layer="1"/>
<text x="-0.8" y="0.65" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-0.8" y="-1.65" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="0.762" y1="0.381" x2="0.762" y2="-0.381" width="0.127" layer="51"/>
<wire x1="0.762" y1="-0.381" x2="-0.762" y2="-0.381" width="0.127" layer="51"/>
<wire x1="-0.762" y1="-0.381" x2="-0.762" y2="0.381" width="0.127" layer="51"/>
<wire x1="-0.762" y1="0.381" x2="0.762" y2="0.381" width="0.127" layer="51"/>
</package>
<package name="3216">
<description>Metric Code Size 3216</description>
<smd name="1" x="-1.5" y="0" dx="1.5" dy="2" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.5" dy="2" layer="1"/>
<text x="-1.6" y="1.1" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-1.6" y="-2.1" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="-1.524" y1="0.762" x2="-1.524" y2="-0.762" width="0.127" layer="51"/>
<wire x1="-1.524" y1="-0.762" x2="1.524" y2="-0.762" width="0.127" layer="51"/>
<wire x1="1.524" y1="-0.762" x2="1.524" y2="0.762" width="0.127" layer="51"/>
<wire x1="1.524" y1="0.762" x2="-1.524" y2="0.762" width="0.127" layer="51"/>
</package>
<package name="3225">
<description>Metric Code Size 3225</description>
<smd name="1" x="-1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<text x="-1.6" y="1.55" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-1.6" y="-2.575" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="-1.524" y1="1.27" x2="1.524" y2="1.27" width="0.127" layer="51"/>
<wire x1="1.524" y1="1.27" x2="1.524" y2="-1.27" width="0.127" layer="51"/>
<wire x1="1.524" y1="-1.27" x2="-1.524" y2="-1.27" width="0.127" layer="51"/>
<wire x1="-1.524" y1="-1.27" x2="-1.524" y2="1.27" width="0.127" layer="51"/>
</package>
<package name="4532">
<description>Metric Code Size 4532</description>
<smd name="1" x="-2.05" y="0" dx="1.8" dy="3.7" layer="1"/>
<smd name="2" x="2.05" y="0" dx="1.8" dy="3.7" layer="1"/>
<text x="-2.25" y="1.95" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-2.25" y="-2.975" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="-2.159" y1="1.524" x2="-2.159" y2="-1.524" width="0.127" layer="51"/>
<wire x1="2.159" y1="1.524" x2="2.159" y2="-1.524" width="0.127" layer="51"/>
<wire x1="-2.159" y1="1.524" x2="2.159" y2="1.524" width="0.127" layer="51"/>
<wire x1="2.159" y1="-1.524" x2="-2.159" y2="-1.524" width="0.127" layer="51"/>
</package>
<package name="5650">
<description>Metric Code Size 5650</description>
<smd name="1" x="-2.55" y="0" dx="1.85" dy="5.5" layer="1"/>
<smd name="2" x="2.55" y="0" dx="1.85" dy="5.5" layer="1"/>
<text x="-2.8" y="2.95" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-2.8" y="-3.975" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="-2.794" y1="2.413" x2="2.794" y2="2.413" width="0.127" layer="51"/>
<wire x1="2.794" y1="2.413" x2="2.794" y2="-2.413" width="0.127" layer="51"/>
<wire x1="2.794" y1="-2.413" x2="-2.794" y2="-2.413" width="0.127" layer="51"/>
<wire x1="-2.794" y1="-2.413" x2="-2.794" y2="2.413" width="0.127" layer="51"/>
</package>
<package name="C025-024X044">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 2.4 x 4.4 mm</description>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.778" y="1.397" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-1.778" y="-2.667" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="51"/>
</package>
<package name="C050-024X044">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 2.4 x 4.4 mm</description>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.159" y="1.397" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.159" y="-2.667" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90" cap="flat"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90" cap="flat"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="21"/>
<rectangle x1="2.159" y1="-0.381" x2="2.54" y2="0.381" layer="51"/>
<rectangle x1="-2.54" y1="-0.381" x2="-2.159" y2="0.381" layer="51"/>
</package>
<package name="C075-032X103">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 3.2 x 10.3 mm</description>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="1.905" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-4.826" y="-3.048" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="4.826" y1="1.524" x2="-4.826" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-1.524" x2="4.826" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.826" y1="1.524" x2="5.08" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.826" y1="-1.524" x2="5.08" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="-1.27" x2="-4.826" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.27" x2="-4.826" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="0.508" y1="0" x2="2.54" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0" x2="-0.508" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="0.889" x2="-0.508" y2="0" width="0.4064" layer="21"/>
<wire x1="-0.508" y1="0" x2="-0.508" y2="-0.889" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0.889" x2="0.508" y2="0" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0" x2="0.508" y2="-0.889" width="0.4064" layer="21"/>
</package>
<package name="2012-C">
<smd name="1" x="-0.9207" y="0" dx="1.016" dy="1.524" layer="1"/>
<smd name="2" x="0.9206" y="0" dx="1.016" dy="1.524" layer="1"/>
<text x="-1.381" y="0.875" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-1.381" y="-1.9" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="0.889" y1="0.635" x2="-0.889" y2="0.635" width="0.127" layer="51"/>
<wire x1="-0.889" y1="0.635" x2="-0.889" y2="-0.635" width="0.127" layer="51"/>
<wire x1="-0.889" y1="-0.635" x2="0.889" y2="-0.635" width="0.127" layer="51"/>
<wire x1="0.889" y1="-0.635" x2="0.889" y2="0.635" width="0.127" layer="51"/>
</package>
<package name="0204/5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 5 mm</description>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.0066" y="1.1684" size="0.9906" layer="25" font="vector" ratio="12">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="0.9906" layer="27" font="vector" ratio="12">&gt;VALUE</text>
<wire x1="2.54" y1="0" x2="2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0" x2="-2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-1.778" y1="0.635" x2="-1.524" y2="0.889" width="0.1524" layer="21" curve="-90" cap="flat"/>
<wire x1="-1.778" y1="-0.635" x2="-1.524" y2="-0.889" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="1.524" y1="-0.889" x2="1.778" y2="-0.635" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="1.524" y1="0.889" x2="1.778" y2="0.635" width="0.1524" layer="21" curve="-90" cap="flat"/>
<wire x1="-1.778" y1="-0.635" x2="-1.778" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-1.524" y1="0.889" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0.762" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-0.889" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-0.762" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="-1.143" y2="0.762" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="-1.143" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.524" y1="0.889" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.524" y1="-0.889" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.635" x2="1.778" y2="0.635" width="0.1524" layer="51"/>
<rectangle x1="-2.032" y1="-0.254" x2="-1.778" y2="0.254" layer="51"/>
<rectangle x1="1.778" y1="-0.254" x2="2.032" y2="0.254" layer="51"/>
</package>
<package name="0204/7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 7.5 mm</description>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.2954" size="0.9906" layer="25" font="vector" ratio="12">&gt;NAME</text>
<text x="-1.6256" y="-0.4826" size="0.9906" layer="27" font="vector" ratio="12">&gt;VALUE</text>
<wire x1="3.81" y1="0" x2="2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-3.81" y1="0" x2="-2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0.762" x2="-2.286" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.286" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="-0.762" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.016" x2="2.54" y2="0.762" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="1.016" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="0.889" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.016" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="-0.889" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="-1.778" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="-1.778" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="2.286" y1="1.016" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.286" y1="-1.016" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="2.54" y2="0.762" width="0.1524" layer="21"/>
<rectangle x1="2.54" y1="-0.254" x2="2.921" y2="0.254" layer="21"/>
<rectangle x1="-2.921" y1="-0.254" x2="-2.54" y2="0.254" layer="21"/>
</package>
<package name="0204V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 2.5 mm</description>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.1336" y="1.1684" size="1.27" layer="25" font="vector" ratio="12">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="1.27" layer="27" font="vector" ratio="12">&gt;VALUE</text>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.508" layer="51"/>
<circle x="-1.27" y="0" radius="0.889" width="0.1524" layer="51"/>
<circle x="-1.27" y="0" radius="0.635" width="0.0508" layer="51"/>
<wire x1="-0.127" y1="0" x2="0.127" y2="0" width="0.508" layer="21"/>
</package>
<package name="0207/10">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 10 mm</description>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.048" y="1.524" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.2606" y="-0.635" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="5.08" y1="0" x2="4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-5.08" y1="0" x2="-4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
</package>
<package name="0207/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 12 mm</description>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="6.35" y1="0" x2="5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="4.445" y1="-0.3048" x2="5.3086" y2="0.3048" layer="21"/>
<rectangle x1="-5.3086" y1="-0.3048" x2="-4.445" y2="0.3048" layer="21"/>
<wire x1="4.445" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-4.445" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
</package>
<package name="0207/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 15mm</description>
<pad name="1" x="-7.62" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="5.715" y1="-0.3048" x2="6.5786" y2="0.3048" layer="21"/>
<rectangle x1="-6.5786" y1="-0.3048" x2="-5.715" y2="0.3048" layer="21"/>
<wire x1="5.715" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-5.715" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
</package>
<package name="2012-R">
<description>Metric Code Size 2012</description>
<smd name="1" x="-1.0477" y="0" dx="1.016" dy="1.524" layer="1"/>
<smd name="2" x="1.0476" y="0" dx="1.016" dy="1.524" layer="1"/>
<text x="-1" y="0.875" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-1" y="-1.9" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="1.016" y1="0.635" x2="-1.016" y2="0.635" width="0.127" layer="51"/>
<wire x1="-1.016" y1="0.635" x2="-1.016" y2="-0.635" width="0.127" layer="51"/>
<wire x1="-1.016" y1="-0.635" x2="1.016" y2="-0.635" width="0.127" layer="51"/>
<wire x1="1.016" y1="-0.635" x2="1.016" y2="0.635" width="0.127" layer="51"/>
</package>
<package name="250-80">
<wire x1="12.5" y1="4" x2="-12.5" y2="4" width="0.127" layer="21"/>
<wire x1="-12.5" y1="4" x2="-12.5" y2="-4" width="0.127" layer="21"/>
<wire x1="-12.5" y1="-4" x2="12.5" y2="-4" width="0.127" layer="21"/>
<wire x1="12.5" y1="-4" x2="12.5" y2="4" width="0.127" layer="21"/>
<pad name="P$1" x="-15.24" y="0" drill="1.2" shape="octagon"/>
<pad name="P$2" x="15.24" y="0" drill="1.2" shape="octagon"/>
<rectangle x1="-15.24" y1="-0.508" x2="-12.573" y2="0.508" layer="51"/>
<rectangle x1="12.573" y1="-0.508" x2="15.24" y2="0.508" layer="51"/>
</package>
<package name="6032">
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.7" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.7" layer="1"/>
<text x="-2.25" y="1.95" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-2.25" y="-2.975" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="-2.909" y1="1.524" x2="-2.909" y2="-1.524" width="0.127" layer="51"/>
<wire x1="2.909" y1="1.524" x2="2.909" y2="-1.524" width="0.127" layer="51"/>
<wire x1="-2.909" y1="1.524" x2="2.909" y2="1.524" width="0.127" layer="51"/>
<wire x1="2.909" y1="-1.524" x2="-2.909" y2="-1.524" width="0.127" layer="51"/>
</package>
<package name="CEMENTR/48">
<description>Cement Resistor
Width=48mm, Depth=10mm, Height=10mm</description>
<pad name="1" x="-29.21" y="0" drill="1.5" shape="octagon"/>
<pad name="2" x="29.21" y="0" drill="1.5" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="29.21" y1="0" x2="28.194" y2="0" width="0.6096" layer="51"/>
<wire x1="-29.21" y1="0" x2="-28.194" y2="0" width="0.6096" layer="51"/>
<rectangle x1="27.305" y1="-0.3048" x2="28.1686" y2="0.3048" layer="21"/>
<rectangle x1="-28.1686" y1="-0.3048" x2="-27.305" y2="0.3048" layer="21"/>
<wire x1="27.305" y1="0" x2="24.384" y2="0" width="0.6096" layer="21"/>
<wire x1="-27.305" y1="0" x2="-24.384" y2="0" width="0.6096" layer="21"/>
<wire x1="-24" y1="5" x2="24" y2="5" width="0.127" layer="21"/>
<wire x1="24" y1="5" x2="24" y2="-5" width="0.127" layer="21"/>
<wire x1="24" y1="-5" x2="-24" y2="-5" width="0.127" layer="21"/>
<wire x1="-24" y1="-5" x2="-24" y2="5" width="0.127" layer="21"/>
</package>
<package name="L5-7,5">
<pad name="P$1" x="2.5" y="0" drill="0.8"/>
<pad name="P$2" x="-2.5" y="0" drill="0.8"/>
<circle x="0" y="0" radius="4" width="0.127" layer="21"/>
<wire x1="-1.5" y1="0" x2="-0.5" y2="0" width="0.0634" layer="21" curve="-180"/>
<wire x1="-0.5" y1="0" x2="0.5" y2="0" width="0.0634" layer="21" curve="-180"/>
<wire x1="0.5" y1="0" x2="1.5" y2="0" width="0.0634" layer="21" curve="-180"/>
<text x="-3" y="-2.5" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-3.5" y="-4" size="1.27" layer="27" font="vector">&gt;VALUE</text>
</package>
<package name="L22">
<pad name="P$1" x="11.43" y="0" drill="2.8" diameter="6.4516"/>
<pad name="P$2" x="-11.43" y="0" drill="2.8" diameter="6.4516"/>
<circle x="0" y="0" radius="11.359225" width="0.127" layer="21"/>
<text x="-3.81" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.81" y="-1.27" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="T50H">
<circle x="0" y="0" radius="2.83980625" width="0.127" layer="21"/>
<circle x="0" y="0" radius="6.35" width="0.127" layer="21"/>
<pad name="P$1" x="-2.54" y="-6.35" drill="1"/>
<pad name="P$2" x="2.54" y="-6.35" drill="1"/>
<text x="-2.54" y="8.89" size="1.27" layer="21">&gt;NAME</text>
<text x="-2.54" y="7.62" size="1.27" layer="21">&gt;VALUE</text>
</package>
<package name="T50V">
<wire x1="-2.54" y1="6.35" x2="2.54" y2="6.35" width="0.127" layer="21"/>
<wire x1="2.54" y1="6.35" x2="2.54" y2="5.08" width="0.127" layer="21"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="3.81" width="0.127" layer="21"/>
<wire x1="2.54" y1="3.81" x2="2.54" y2="2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="0" width="0.127" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="-3.81" width="0.127" layer="21"/>
<wire x1="2.54" y1="-3.81" x2="2.54" y2="-5.08" width="0.127" layer="21"/>
<wire x1="2.54" y1="-5.08" x2="2.54" y2="-6.35" width="0.127" layer="21"/>
<wire x1="2.54" y1="-6.35" x2="-2.54" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-6.35" x2="-2.54" y2="-5.08" width="0.127" layer="21"/>
<pad name="P$1" x="-2.54" y="0" drill="1"/>
<pad name="P$2" x="2.54" y="0" drill="1"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-3.81" x2="-2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="0" width="0.127" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="2.54" width="0.127" layer="21"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="3.81" width="0.127" layer="21"/>
<wire x1="-2.54" y1="3.81" x2="-2.54" y2="5.08" width="0.127" layer="21"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="6.35" width="0.127" layer="21"/>
<wire x1="-2.54" y1="5.08" x2="2.54" y2="3.81" width="0.127" layer="21"/>
<wire x1="-2.54" y1="3.81" x2="2.54" y2="2.54" width="0.127" layer="21"/>
<wire x1="-2.54" y1="2.54" x2="2.54" y2="1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="2.54" y2="0" width="0.127" layer="21"/>
<wire x1="-2.54" y1="0" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-2.54" x2="2.54" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-3.81" x2="2.54" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-5.08" x2="2.54" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-2.54" y1="6.35" x2="2.54" y2="5.08" width="0.127" layer="21"/>
<text x="-2.54" y="8.89" size="1.27" layer="21">&gt;NAME</text>
<text x="-2.54" y="7.62" size="1.27" layer="21">&gt;VALUE</text>
</package>
<package name="T20V">
<pad name="P$1" x="-1.27" y="0" drill="0.8"/>
<pad name="P$2" x="1.27" y="0" drill="0.8"/>
<wire x1="-0.635" y1="2.54" x2="0.635" y2="2.54" width="0.127" layer="21"/>
<wire x1="0.635" y1="2.54" x2="0.635" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="-0.635" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="-0.635" y2="2.54" width="0.127" layer="21"/>
<text x="-1.905" y="4.445" size="1.27" layer="21">&gt;NAME</text>
<text x="-1.905" y="3.175" size="1.27" layer="21">&gt;VALUE</text>
</package>
<package name="2125">
<description>Metric Code Size 2012</description>
<smd name="1" x="-1.0477" y="0" dx="1.016" dy="1.8" layer="1"/>
<smd name="2" x="1.0476" y="0" dx="1.016" dy="1.8" layer="1"/>
<text x="-1" y="0.875" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-1" y="-1.9" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="1.016" y1="0.635" x2="-1.016" y2="0.635" width="0.127" layer="51"/>
<wire x1="-1.016" y1="0.635" x2="-1.016" y2="-0.635" width="0.127" layer="51"/>
<wire x1="-1.016" y1="-0.635" x2="1.016" y2="-0.635" width="0.127" layer="51"/>
<wire x1="1.016" y1="-0.635" x2="1.016" y2="0.635" width="0.127" layer="51"/>
</package>
<package name="SOLENOID-D5-L2.5">
<pad name="P$1" x="-1.27" y="0" drill="0.8" shape="octagon"/>
<pad name="P$2" x="1.27" y="0" drill="0.8" shape="octagon"/>
<text x="-2.54" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="2.54" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-1.27" y1="0" x2="-0.635" y2="0" width="0.127" layer="27"/>
<wire x1="-0.635" y1="0" x2="0" y2="2.54" width="0.127" layer="27"/>
<wire x1="0" y1="2.54" x2="0" y2="-2.54" width="0.127" layer="27"/>
<wire x1="0" y1="-2.54" x2="0.635" y2="2.54" width="0.127" layer="27"/>
<wire x1="0.635" y1="2.54" x2="0.635" y2="-2.54" width="0.127" layer="27"/>
<wire x1="0.635" y1="-2.54" x2="1.27" y2="0" width="0.127" layer="27"/>
</package>
<package name="SOLENOID-D5-L5">
<pad name="P$1" x="-2.54" y="0" drill="0.8" shape="octagon"/>
<pad name="P$2" x="2.54" y="0" drill="0.8" shape="octagon"/>
<text x="-2.54" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="2.54" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-2.54" y1="0" x2="-1.905" y2="0" width="0.127" layer="21"/>
<wire x1="-1.905" y1="0" x2="-1.27" y2="2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="-0.635" y2="2.54" width="0.127" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="-0.635" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="0" y2="2.54" width="0.127" layer="21"/>
<wire x1="0" y1="2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0.635" y2="2.54" width="0.127" layer="21"/>
<wire x1="0.635" y1="2.54" x2="0.635" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="1.27" y2="2.54" width="0.127" layer="21"/>
<wire x1="1.27" y1="2.54" x2="1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="1.27" y1="-2.54" x2="1.905" y2="2.54" width="0.127" layer="21"/>
<wire x1="1.905" y1="2.54" x2="1.905" y2="0" width="0.127" layer="21"/>
<wire x1="1.905" y1="0" x2="2.54" y2="0" width="0.127" layer="21"/>
</package>
<package name="NR6028T">
<description>&lt;b&gt;NR6028&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-2.35" y="0" dx="5.7" dy="1.6" layer="1" rot="R90"/>
<smd name="2" x="2.35" y="0" dx="5.7" dy="1.6" layer="1" rot="R90"/>
<text x="-1.27" y="6.35" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="-1.27" y="5.08" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-3" y1="3" x2="3" y2="3" width="0.2" layer="51"/>
<wire x1="3" y1="3" x2="3" y2="-3" width="0.2" layer="51"/>
<wire x1="3" y1="-3" x2="-3" y2="-3" width="0.2" layer="51"/>
<wire x1="-3" y1="-3" x2="-3" y2="3" width="0.2" layer="51"/>
<wire x1="-4.15" y1="4" x2="4.15" y2="4" width="0.1" layer="51"/>
<wire x1="4.15" y1="4" x2="4.15" y2="-4" width="0.1" layer="51"/>
<wire x1="4.15" y1="-4" x2="-4.15" y2="-4" width="0.1" layer="51"/>
<wire x1="-4.15" y1="-4" x2="-4.15" y2="4" width="0.1" layer="51"/>
<wire x1="-3" y1="-3" x2="3" y2="-3" width="0.1" layer="21"/>
<wire x1="-3" y1="3" x2="3" y2="3" width="0.1" layer="21"/>
</package>
<package name="NR8040T220M">
<description>&lt;b&gt;NR8040&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-2.8" y="0" dx="7.5" dy="1.8" layer="1" rot="R90"/>
<smd name="2" x="2.8" y="0" dx="7.5" dy="1.8" layer="1" rot="R90"/>
<text x="-1.019" y="0.137" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="-1.019" y="0.137" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-4" y1="4" x2="4" y2="4" width="0.2" layer="51"/>
<wire x1="4" y1="4" x2="4" y2="-4" width="0.2" layer="51"/>
<wire x1="4" y1="-4" x2="-4" y2="-4" width="0.2" layer="51"/>
<wire x1="-4" y1="-4" x2="-4" y2="4" width="0.2" layer="51"/>
<wire x1="-4" y1="4" x2="-4" y2="-4" width="0.2" layer="21"/>
<wire x1="-4" y1="-4" x2="4" y2="-4" width="0.2" layer="21"/>
<wire x1="4" y1="-4" x2="4" y2="4" width="0.2" layer="21"/>
<wire x1="4" y1="4" x2="-4" y2="4" width="0.2" layer="21"/>
</package>
<package name="PUSHSW">
<pad name="1" x="-2.54" y="3.81" drill="1"/>
<pad name="2" x="-2.54" y="-3.81" drill="1"/>
<pad name="3" x="2.54" y="3.81" drill="1"/>
<pad name="4" x="2.54" y="-3.81" drill="1"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-2.54" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="2.54" x2="-2.54" y2="2.54" width="0.127" layer="21"/>
<circle x="0" y="0" radius="1.796" width="0.127" layer="21"/>
</package>
<package name="PUSHSW_SMD">
<wire x1="-2.65" y1="-2.65" x2="2.65" y2="-2.65" width="0.127" layer="21"/>
<wire x1="2.65" y1="-2.65" x2="2.65" y2="2.65" width="0.127" layer="21"/>
<wire x1="2.65" y1="2.65" x2="-2.65" y2="2.65" width="0.127" layer="21"/>
<wire x1="-2.65" y1="2.65" x2="-2.65" y2="-2.65" width="0.127" layer="21"/>
<smd name="2" x="3.1" y="1.85" dx="3.6" dy="2" layer="1"/>
<smd name="1" x="-3.1" y="1.85" dx="3.6" dy="2" layer="1"/>
<smd name="3" x="-3.1" y="-1.85" dx="3.6" dy="2" layer="1"/>
<smd name="4" x="3.1" y="-1.85" dx="3.6" dy="2" layer="1"/>
<wire x1="-1.85" y1="-1.85" x2="1.85" y2="-1.85" width="0.127" layer="21"/>
<wire x1="1.85" y1="-1.85" x2="1.85" y2="1.85" width="0.127" layer="21"/>
<wire x1="1.85" y1="1.85" x2="-1.85" y2="1.85" width="0.127" layer="21"/>
<wire x1="-1.85" y1="1.85" x2="-1.85" y2="-1.85" width="0.127" layer="21"/>
<text x="-3.81" y="5.08" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-3.81" y="3.81" size="1.27" layer="27" font="vector">&gt;VALUE</text>
</package>
<package name="PUSHSW_THIN">
<pad name="1" x="-2.54" y="5.08" drill="1.2" shape="octagon"/>
<pad name="2" x="2.54" y="5.08" drill="1.2" shape="octagon"/>
<pad name="3" x="-2.54" y="-5.08" drill="1.2" shape="octagon"/>
<pad name="4" x="2.54" y="-5.08" drill="1.2" shape="octagon"/>
<circle x="0" y="0" radius="6.254" width="0.127" layer="21"/>
<circle x="0" y="0" radius="5.08" width="0.127" layer="21"/>
<circle x="0" y="3.81" radius="0.635" width="0.127" layer="21"/>
<wire x1="-3.81" y1="4.953" x2="-3.81" y2="6.35" width="0.127" layer="21"/>
<wire x1="-3.81" y1="6.35" x2="3.81" y2="6.35" width="0.127" layer="21"/>
<wire x1="3.81" y1="6.35" x2="3.81" y2="4.953" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-4.953" x2="-3.81" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-5.715" y1="2.54" x2="-6.35" y2="2.54" width="0.127" layer="21"/>
<wire x1="-6.35" y1="2.54" x2="-6.35" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-2.54" x2="-5.715" y2="-2.54" width="0.127" layer="21"/>
<wire x1="5.715" y1="2.54" x2="6.35" y2="2.54" width="0.127" layer="21"/>
<wire x1="6.35" y1="2.54" x2="6.35" y2="-2.54" width="0.127" layer="21"/>
<wire x1="6.35" y1="-2.54" x2="5.715" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-6.35" x2="3.81" y2="-6.35" width="0.127" layer="21"/>
<wire x1="3.81" y1="-6.35" x2="3.81" y2="-4.953" width="0.127" layer="21"/>
</package>
<package name="PEC09">
<pad name="B" x="-5" y="2.5" drill="1" shape="long" rot="R180"/>
<pad name="C" x="-5" y="0" drill="1" shape="long" rot="R180"/>
<pad name="A" x="-5" y="-2.5" drill="1" shape="long" rot="R180"/>
<wire x1="-7.05" y1="4.85" x2="0" y2="4.85" width="0.127" layer="21"/>
<wire x1="0" y1="4.85" x2="0" y2="3.81" width="0.127" layer="21"/>
<wire x1="0" y1="3.81" x2="0" y2="-3.81" width="0.127" layer="21"/>
<wire x1="0" y1="-3.81" x2="0" y2="-4.85" width="0.127" layer="21"/>
<wire x1="0" y1="-4.85" x2="-7.05" y2="-4.85" width="0.127" layer="21"/>
<wire x1="-7.05" y1="-4.85" x2="-7.05" y2="4.85" width="0.127" layer="21"/>
<wire x1="0" y1="3.81" x2="6.35" y2="3.81" width="0.127" layer="21"/>
<wire x1="6.35" y1="3.81" x2="7.62" y2="2.54" width="0.127" layer="21"/>
<wire x1="7.62" y1="2.54" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<wire x1="7.62" y1="-2.54" x2="6.35" y2="-3.81" width="0.127" layer="21"/>
<wire x1="6.35" y1="-3.81" x2="0" y2="-3.81" width="0.127" layer="21"/>
<wire x1="7.62" y1="2.54" x2="13.97" y2="2.54" width="0.127" layer="21"/>
<wire x1="13.97" y1="2.54" x2="13.97" y2="-1.27" width="0.127" layer="21"/>
<wire x1="13.97" y1="-1.27" x2="13.97" y2="-2.54" width="0.127" layer="21"/>
<wire x1="13.97" y1="-2.54" x2="8.89" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="-2.54" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="-2.54" x2="8.89" y2="-1.27" width="0.127" layer="21"/>
<wire x1="8.89" y1="-1.27" x2="13.97" y2="-1.27" width="0.127" layer="21"/>
<text x="-7.62" y="7.62" size="1.27" layer="25">&gt;NAME</text>
<text x="-7.62" y="6.35" size="1.27" layer="25">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="C">
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="-1.016" x2="0" y2="-1.0161" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.0161" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1" x2="2.4892" y2="-1.8542" width="0.254" layer="94" curve="-37.8782" cap="flat"/>
<wire x1="-2.4668" y1="-1.8504" x2="0" y2="-1.0161" width="0.254" layer="94" curve="-37.3729" cap="flat"/>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<text x="1.016" y="0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="1.016" y="-4.191" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="R">
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
</symbol>
<symbol name="L">
<text x="-1.27" y="-5.08" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="3.81" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<wire x1="0" y1="-2.54" x2="0.635" y2="-1.905" width="0.254" layer="94" curve="90"/>
<wire x1="0.635" y1="-1.905" x2="0" y2="-1.27" width="0.254" layer="94" curve="90"/>
<wire x1="0" y1="-1.27" x2="0.635" y2="-0.635" width="0.254" layer="94" curve="90"/>
<wire x1="0.635" y1="-0.635" x2="0" y2="0" width="0.254" layer="94" curve="90"/>
<wire x1="0" y1="0" x2="0.635" y2="0.635" width="0.254" layer="94" curve="90"/>
<wire x1="0.635" y1="0.635" x2="0" y2="1.27" width="0.254" layer="94" curve="90"/>
<wire x1="0" y1="1.27" x2="0.635" y2="1.905" width="0.254" layer="94" curve="90"/>
<wire x1="0.635" y1="1.905" x2="0" y2="2.54" width="0.254" layer="94" curve="90"/>
</symbol>
<symbol name="PUSHSW2">
<pin name="1" x="-5.08" y="5.08" length="short" swaplevel="1" rot="R270"/>
<pin name="2" x="-5.08" y="-5.08" length="short" swaplevel="1" rot="R90"/>
<pin name="3" x="5.08" y="5.08" length="short" swaplevel="2" rot="R270"/>
<pin name="4" x="5.08" y="-5.08" length="short" swaplevel="2" rot="R90"/>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="0" width="0.254" layer="94"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="0" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="0" x2="-2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="5.08" y2="0" width="0.254" layer="94"/>
<circle x="-1.778" y="0" radius="0.7184" width="0.254" layer="94"/>
<circle x="1.778" y="0" radius="0.7184" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="3.81" x2="0" y2="2.54" width="0.254" layer="94"/>
</symbol>
<symbol name="ENCODER">
<pin name="A" x="-7.62" y="2.54" length="middle"/>
<pin name="B" x="-7.62" y="0" length="middle"/>
<pin name="COM" x="-7.62" y="-2.54" length="middle"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
<text x="-2.54" y="7.62" size="1.27" layer="95">&gt;NAME</text>
<text x="-2.54" y="5.08" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="C" prefix="C" uservalue="yes">
<gates>
<gate name="G$1" symbol="C" x="0" y="0"/>
</gates>
<devices>
<device name="-1005" package="1005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1608" package="1608">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3216" package="3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3225" package="3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-4532" package="4532">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5650" package="5650">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/2.5" package="C025-024X044">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/5" package="C050-024X044">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/7.5" package="C075-032X103">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2012" package="2012-C">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="R" prefix="R" uservalue="yes">
<gates>
<gate name="G$1" symbol="R" x="0" y="0"/>
</gates>
<devices>
<device name="/5" package="0204/5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/7" package="0204/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/2.5" package="0204V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/10" package="0207/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/12" package="0207/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/15" package="0207/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1005" package="1005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1608" package="1608">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2012" package="2012-R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3216" package="3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3225" package="3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-4532" package="4532">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5650" package="5650">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="250-80">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-6032" package="6032">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/48" package="CEMENTR/48">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="L" prefix="L" uservalue="yes">
<gates>
<gate name="G$1" symbol="L" x="0" y="0"/>
</gates>
<devices>
<device name="/5" package="0204/5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/7" package="0204/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/2.5" package="0204V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/10" package="0207/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/12" package="0207/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/15" package="0207/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1005" package="1005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1608" package="1608">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2012" package="2012-R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3216" package="3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3225" package="3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-4532" package="4532">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5650" package="5650">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="250-80">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/5(VERTICAL)" package="L5-7,5">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="22(VERTICAL)" package="L22">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="T50H" package="T50H">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="T50V" package="T50V">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="T20V" package="T20V">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2125" package="2125">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-D5-L2.5" package="SOLENOID-D5-L2.5">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-D5-L5" package="SOLENOID-D5-L5">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="NR6028" package="NR6028T">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="NR8040" package="NR8040T220M">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PUSHSW" prefix="SW">
<gates>
<gate name="G$1" symbol="PUSHSW2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PUSHSW">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD" package="PUSHSW_SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_THIN" package="PUSHSW_THIN">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
<connect gate="G$1" pin="3" pad="2"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PEC09" prefix="ENC">
<gates>
<gate name="G$1" symbol="ENCODER" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PEC09">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="B" pad="B"/>
<connect gate="G$1" pin="COM" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Diode_Robotech">
<description>&lt;h3&gt;RoboTech EAGLE Library&lt;/h3&gt;
Diode Library &lt;br&gt;
$Rev: 25542 $

&lt;p&gt;
Since 2007&lt;br&gt;
by RoboTech&lt;br&gt;
Jun'ichi Takisawa&lt;br&gt;
Hiroki Yabe&lt;br&gt;
Katsuhiko Nishimra&lt;br&gt;
Takuo Sawada&lt;br&gt;
&lt;/p&gt;</description>
<packages>
<package name="SOD-123FL">
<description>SOD-123FL for manual soldering</description>
<smd name="K" x="-1.85" y="0" dx="1.8" dy="1.2" layer="1"/>
<smd name="A" x="1.85" y="0" dx="1.8" dy="1.2" layer="1"/>
<text x="-2.25" y="1.95" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.25" y="-2.975" size="1.016" layer="27">&gt;VALUE</text>
<wire x1="-1.5" y1="1" x2="-1.5" y2="-1" width="0.127" layer="51"/>
<wire x1="1.5" y1="1" x2="1.5" y2="-1" width="0.127" layer="51"/>
<wire x1="-1.5" y1="1" x2="1.5" y2="1" width="0.127" layer="51"/>
<wire x1="1.5" y1="-1" x2="-1.5" y2="-1" width="0.127" layer="51"/>
<wire x1="-1.27" y1="0" x2="-0.635" y2="0" width="0.127" layer="51"/>
<wire x1="-0.635" y1="0" x2="1.27" y2="0" width="0.127" layer="51"/>
<wire x1="-0.635" y1="0.635" x2="-0.635" y2="0" width="0.127" layer="51"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-0.635" width="0.127" layer="51"/>
<wire x1="-0.635" y1="0" x2="0.635" y2="0.635" width="0.127" layer="51"/>
<wire x1="0.635" y1="0.635" x2="0.635" y2="-0.635" width="0.127" layer="51"/>
<wire x1="0.635" y1="-0.635" x2="-0.635" y2="0" width="0.127" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="D">
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<text x="2.54" y="0.4826" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.3114" size="1.778" layer="96">&gt;VALUE</text>
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SS2040FL" prefix="D">
<gates>
<gate name="G$1" symbol="D" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOD-123FL">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="IQD-Frequency-Products">
<description>&lt;b&gt;Crystals and Oscillators&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by IQD Frequency Products&lt;/author&gt;</description>
<packages>
<package name="7.5X5-4-PAD">
<description>&lt;b&gt;IQD Frequency Products SMD Package&lt;/b&gt;</description>
<smd name="1" x="-3.15" y="-1.25" dx="2.2" dy="1.4" layer="1"/>
<smd name="2" x="3.15" y="-1.25" dx="2.2" dy="1.4" layer="1"/>
<smd name="3" x="3.15" y="1.25" dx="2.2" dy="1.4" layer="1" rot="R180"/>
<smd name="4" x="-3.15" y="1.25" dx="2.2" dy="1.4" layer="1" rot="R180"/>
<text x="-4.0192" y="2.815" size="1.27" layer="25">&gt;NAME</text>
<text x="-4.0192" y="-4.593" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-5.5" y1="2.5" x2="4.73" y2="2.5" width="0.127" layer="21"/>
<wire x1="4.73" y1="2.5" x2="4.73" y2="-2.5" width="0.127" layer="21"/>
<wire x1="4.73" y1="-2.5" x2="-4.74" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-4.74" y1="-2.5" x2="-4.74" y2="2.46" width="0.127" layer="21"/>
<wire x1="-5.5" y1="2.5" x2="-5.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-5.5" y1="-2.5" x2="-4.74" y2="-2.5" width="0.127" layer="21"/>
<circle x="-5.05" y="-2.87" radius="0.2" width="0" layer="21"/>
</package>
<package name="5.2X3.4-4-PAD">
<description>&lt;b&gt;IQD Frequency Products SMD Package&lt;/b&gt;</description>
<wire x1="-4.2" y1="-2.3" x2="3.368" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-3.3" y1="2.276" x2="-3.3" y2="-2.176" width="0.2032" layer="21"/>
<wire x1="3.368" y1="2.3" x2="-4.2" y2="2.3" width="0.2032" layer="21"/>
<wire x1="3.4" y1="-2.276" x2="3.4" y2="2.276" width="0.2032" layer="21"/>
<smd name="1" x="-2.1" y="-1.1" dx="1.4" dy="1.6" layer="1" rot="R90"/>
<smd name="2" x="2.1" y="-1.1" dx="1.4" dy="1.6" layer="1" rot="R90"/>
<smd name="3" x="2.1" y="1.1" dx="1.4" dy="1.6" layer="1" rot="R270"/>
<smd name="4" x="-2.1" y="1.1" dx="1.4" dy="1.6" layer="1" rot="R270"/>
<text x="3.04" y="4.39" size="1.27" layer="25" rot="R180">&gt;NAME</text>
<text x="-3.686" y="-4.608" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-4.2" y1="2.3" x2="-4.2" y2="-2.3" width="0.2032" layer="21"/>
<circle x="-3.7" y="-2.8" radius="0.2" width="0" layer="21"/>
</package>
<package name="3.4X2.7-4-PAD">
<description>&lt;b&gt;IQD Frequency Products SMD Package&lt;/b&gt;</description>
<wire x1="-3.1" y1="1.9" x2="2.4" y2="1.9" width="0.2032" layer="21"/>
<wire x1="2.4" y1="-1.9" x2="-3.1" y2="-1.9" width="0.2032" layer="21"/>
<smd name="1" x="-1.1" y="-0.8" dx="1.4" dy="1.15" layer="1"/>
<smd name="2" x="1.1" y="-0.8" dx="1.4" dy="1.15" layer="1"/>
<smd name="3" x="1.1" y="0.8" dx="1.4" dy="1.15" layer="1" rot="R180"/>
<smd name="4" x="-1.1" y="0.8" dx="1.4" dy="1.15" layer="1" rot="R180"/>
<text x="-2.54" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="2.4" y1="1.9" x2="2.4" y2="-1.91" width="0.2" layer="21"/>
<wire x1="-2.42" y1="1.9" x2="-2.42" y2="-1.91" width="0.2" layer="21"/>
<wire x1="-3.1" y1="1.9" x2="-3.1" y2="-1.9" width="0.2032" layer="21"/>
<circle x="-2.7" y="-2.3" radius="0.2" width="0" layer="21"/>
</package>
<package name="2.5X2-4-PAD">
<description>&lt;b&gt;IQD Frequency Products SMD Package&lt;/b&gt;</description>
<smd name="1" x="-0.85" y="-0.65" dx="1.2" dy="1" layer="1"/>
<smd name="2" x="0.85" y="-0.65" dx="1.2" dy="1" layer="1"/>
<smd name="3" x="0.85" y="0.65" dx="1.2" dy="1" layer="1" rot="R180"/>
<smd name="4" x="-0.85" y="0.65" dx="1.2" dy="1" layer="1" rot="R180"/>
<text x="-3.27" y="2.07" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.47" y="-3.54" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-2.1" y1="-1.5" x2="2" y2="-1.5" width="0.127" layer="21"/>
<wire x1="2" y1="-1.5" x2="2" y2="1.5" width="0.127" layer="21"/>
<wire x1="2" y1="1.5" x2="-2.1" y2="1.5" width="0.127" layer="21"/>
<wire x1="-2.1" y1="1.5" x2="-2.1" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-2.1" y1="1.5" x2="-2.6" y2="1.5" width="0.127" layer="21"/>
<wire x1="-2.6" y1="1.5" x2="-2.6" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-2.6" y1="-1.5" x2="-2.1" y2="-1.5" width="0.127" layer="21"/>
<circle x="-2.3" y="-1.8" radius="0.2" width="0" layer="21"/>
</package>
<package name="2.6X2.1-4-PAD">
<description>&lt;b&gt;IQD Frequency Products SMD Package&lt;/b&gt;</description>
<smd name="1" x="-0.8" y="-0.6" dx="0.9" dy="0.8" layer="1"/>
<smd name="2" x="0.8" y="-0.6" dx="0.9" dy="0.8" layer="1"/>
<smd name="3" x="0.8" y="0.6" dx="0.9" dy="0.8" layer="1" rot="R180"/>
<smd name="4" x="-0.8" y="0.6" dx="0.9" dy="0.8" layer="1" rot="R180"/>
<text x="-3.24" y="2.04" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.54" y="-3.41" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-1.6" y1="1.4" x2="1.7" y2="1.4" width="0.127" layer="21"/>
<wire x1="1.7" y1="1.4" x2="1.7" y2="-1.4" width="0.127" layer="21"/>
<wire x1="1.7" y1="-1.4" x2="-1.6" y2="-1.4" width="0.127" layer="21"/>
<wire x1="-1.6" y1="-1.4" x2="-1.6" y2="1.4" width="0.127" layer="21"/>
<wire x1="-1.6" y1="1.4" x2="-2" y2="1.4" width="0.127" layer="21"/>
<wire x1="-2" y1="1.4" x2="-2" y2="-1.4" width="0.127" layer="21"/>
<wire x1="-2" y1="-1.4" x2="-1.6" y2="-1.4" width="0.127" layer="21"/>
<circle x="-1.8" y="-1.7" radius="0.2" width="0" layer="21"/>
</package>
<package name="2.0X1.6-4-PAD">
<description>&lt;b&gt;IQD Frequency Products SMD Package&lt;/b&gt;</description>
<smd name="1" x="-0.637" y="-0.487" dx="0.875" dy="0.775" layer="1"/>
<smd name="2" x="0.637" y="-0.487" dx="0.875" dy="0.775" layer="1"/>
<smd name="3" x="0.637" y="0.487" dx="0.875" dy="0.775" layer="1" rot="R180"/>
<smd name="4" x="-0.637" y="0.487" dx="0.875" dy="0.775" layer="1" rot="R180"/>
<text x="-2.27" y="1.77" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.67" y="-3.34" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-1.4" y1="1.3" x2="1.5" y2="1.3" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.3" x2="1.5" y2="-1.3" width="0.127" layer="21"/>
<wire x1="1.5" y1="-1.3" x2="-1.4" y2="-1.3" width="0.127" layer="21"/>
<wire x1="-1.4" y1="-1.3" x2="-1.4" y2="1.3" width="0.127" layer="21"/>
<wire x1="-1.4" y1="1.3" x2="-1.9" y2="1.3" width="0.127" layer="21"/>
<wire x1="-1.9" y1="1.3" x2="-1.9" y2="-1.3" width="0.127" layer="21"/>
<wire x1="-1.9" y1="-1.3" x2="-1.4" y2="-1.3" width="0.127" layer="21"/>
<circle x="-1.6" y="-1.6" radius="0.2" width="0" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="Q-SHIELD2">
<wire x1="2.286" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0.254" y2="0" width="0.1524" layer="94"/>
<wire x1="0.889" y1="1.524" x2="0.889" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.889" y1="-1.524" x2="1.651" y2="-1.524" width="0.254" layer="94"/>
<wire x1="1.651" y1="-1.524" x2="1.651" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.651" y1="1.524" x2="0.889" y2="1.524" width="0.254" layer="94"/>
<wire x1="2.286" y1="1.778" x2="2.286" y2="0" width="0.254" layer="94"/>
<wire x1="2.286" y1="0" x2="2.286" y2="-1.778" width="0.254" layer="94"/>
<wire x1="0.254" y1="1.778" x2="0.254" y2="0" width="0.254" layer="94"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.778" y1="1.905" x2="-1.778" y2="2.54" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-1.778" y1="2.54" x2="4.318" y2="2.54" width="0.1524" layer="94" style="shortdash"/>
<wire x1="4.318" y1="2.54" x2="4.318" y2="1.905" width="0.1524" layer="94" style="shortdash"/>
<wire x1="4.318" y1="-1.905" x2="4.318" y2="-2.54" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-1.778" y1="-2.54" x2="4.318" y2="-2.54" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-1.778" y1="-2.54" x2="-1.778" y2="-1.905" width="0.1524" layer="94" style="shortdash"/>
<text x="-2.54" y="6.096" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="3.81" size="1.778" layer="96">&gt;VALUE</text>
<pin name="3" x="5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-2.54" y="0" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="4" x="2.54" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="2" x="0" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CRYSTALS-GND-LID" prefix="X">
<description>&lt;b&gt;CRYSTALS WITH CAN GROUNDED&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="Q-SHIELD2" x="0" y="0"/>
</gates>
<devices>
<device name="-4-PAD" package="7.5X5-4-PAD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="12SMX-B"/>
</technologies>
</device>
<device name="-CFPX-104" package="5.2X3.4-4-PAD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-CFPX-180" package="3.4X2.7-4-PAD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-CFPX-181" package="2.5X2-4-PAD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-CFPX-218" package="2.6X2.1-4-PAD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-IQXC-42" package="2.0X1.6-4-PAD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="IC1" library="MicroControler_Robotech" deviceset="ATMEGA*8" device="P-AU" technology="32"/>
<part name="IC2" library="silabs" deviceset="SI5351" device=""/>
<part name="IC3" library="IC_Robotech" deviceset="BQ24703PW" device=""/>
<part name="IC4" library="IC_Robotech" deviceset="LM2700MTX-ADJ_NOPB" device=""/>
<part name="LCD1" library="Connector_Robotech" deviceset="AQM1248" device=""/>
<part name="IC5" library="IC_Robotech" deviceset="NJW4181" device="U3"/>
<part name="IC6" library="IC_Robotech" deviceset="LT1761" device=""/>
<part name="U$1" library="Transistor_Robotech" deviceset="A03401A" device=""/>
<part name="U$2" library="Transistor_Robotech" deviceset="A03401A" device=""/>
<part name="U$3" library="Transistor_Robotech" deviceset="A03401A" device=""/>
<part name="U$5" library="Supply_Robotech" deviceset="+3V3" device=""/>
<part name="U$6" library="Supply_Robotech" deviceset="+3V3INT" device=""/>
<part name="C1" library="Passive_Robotech" deviceset="C" device="-3225"/>
<part name="C2" library="Passive_Robotech" deviceset="C" device="-3225"/>
<part name="C3" library="Passive_Robotech" deviceset="C" device="-3225"/>
<part name="C4" library="Passive_Robotech" deviceset="C" device="-3225"/>
<part name="C5" library="Passive_Robotech" deviceset="C" device="-3225"/>
<part name="C6" library="Passive_Robotech" deviceset="C" device="-3225"/>
<part name="C7" library="Passive_Robotech" deviceset="C" device="-3225"/>
<part name="C8" library="Passive_Robotech" deviceset="C" device="-3225"/>
<part name="R1" library="Passive_Robotech" deviceset="R" device="-3225"/>
<part name="R2" library="Passive_Robotech" deviceset="R" device="-3225"/>
<part name="L1" library="Passive_Robotech" deviceset="L" device="NR8040"/>
<part name="L2" library="Passive_Robotech" deviceset="L" device="NR6028"/>
<part name="D1" library="Diode_Robotech" deviceset="SS2040FL" device=""/>
<part name="D2" library="Diode_Robotech" deviceset="SS2040FL" device=""/>
<part name="D3" library="Diode_Robotech" deviceset="SS2040FL" device=""/>
<part name="R3" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R4" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R5" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R6" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R7" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R8" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R9" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R10" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R11" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R12" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R13" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R14" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R15" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="U$7" library="Supply_Robotech" deviceset="+3V3INT" device=""/>
<part name="U$8" library="Supply_Robotech" deviceset="+3V3" device=""/>
<part name="R19" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R20" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R21" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R22" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="SUPPLY1" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY2" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY3" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY4" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY5" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY6" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY7" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY8" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY9" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY10" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY11" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY12" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY13" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY14" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY15" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY16" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY17" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY18" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="C9" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C10" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C11" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C12" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="R23" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R24" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R25" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="SUPPLY19" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY20" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY21" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY22" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="R26" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="C13" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C14" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="SUPPLY23" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY24" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="C15" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="SUPPLY25" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="C16" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C17" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C18" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C19" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C20" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C21" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C22" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C23" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C24" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C25" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C26" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C27" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C28" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="SUPPLY26" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY27" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY28" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY29" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY30" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY31" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY32" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="U$4" library="Supply_Robotech" deviceset="+3V3INT" device=""/>
<part name="R16" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R17" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="U$9" library="Supply_Robotech" deviceset="+3V3" device=""/>
<part name="C29" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="SUPPLY33" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="C30" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="SUPPLY34" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="L3" library="Passive_Robotech" deviceset="L" device="-2125"/>
<part name="U$10" library="Supply_Robotech" deviceset="+3V3" device=""/>
<part name="X1" library="IQD-Frequency-Products" deviceset="CRYSTALS-GND-LID" device="-CFPX-104"/>
<part name="SUPPLY35" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="CON1" library="Connector_Robotech" deviceset="POWER-SOURCE" device="-M-A"/>
<part name="SUPPLY36" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="CON2" library="Connector_Robotech" deviceset="AVRISP-NEWSYMBOL" device="-PH"/>
<part name="U$11" library="Supply_Robotech" deviceset="+3V3INT" device=""/>
<part name="U$12" library="Supply_Robotech" deviceset="+3V3INT" device=""/>
<part name="U$13" library="Supply_Robotech" deviceset="+3V3INT" device=""/>
<part name="SUPPLY38" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY39" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY40" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY41" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="U$14" library="Supply_Robotech" deviceset="+3V3INT" device=""/>
<part name="SUPPLY42" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SW1" library="Passive_Robotech" deviceset="PUSHSW" device=""/>
<part name="SW2" library="Passive_Robotech" deviceset="PUSHSW" device=""/>
<part name="SW3" library="Passive_Robotech" deviceset="PUSHSW" device=""/>
<part name="SUPPLY43" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="J1" library="Connector_Robotech" deviceset="DCJACK" device="SMD"/>
<part name="SUPPLY44" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="ENC1" library="Passive_Robotech" deviceset="PEC09" device=""/>
<part name="R18" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R27" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R28" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R29" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="SUPPLY45" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="C31" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C32" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="SUPPLY46" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY47" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="U$15" library="Supply_Robotech" deviceset="+3V3INT" device=""/>
<part name="U$16" library="Supply_Robotech" deviceset="+3V3INT" device=""/>
<part name="CON3" library="Connector_Robotech" deviceset="RADIO-INTERCONNECT" device=""/>
<part name="SUPPLY37" library="Supply_Robotech" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="IC1" gate="1" x="111.76" y="-5.08"/>
<instance part="IC2" gate="G$1" x="226.06" y="-30.48"/>
<instance part="IC3" gate="G$1" x="-22.86" y="73.66"/>
<instance part="IC4" gate="G$1" x="121.92" y="71.12"/>
<instance part="LCD1" gate="G$1" x="208.28" y="20.32"/>
<instance part="IC5" gate="G$1" x="233.68" y="50.8"/>
<instance part="IC6" gate="G$1" x="236.22" y="71.12"/>
<instance part="U$1" gate="G$1" x="45.72" y="73.66" rot="MR90"/>
<instance part="U$2" gate="G$1" x="22.86" y="99.06" rot="MR90"/>
<instance part="U$3" gate="G$1" x="101.6" y="73.66" rot="MR90"/>
<instance part="U$5" gate="G$1" x="248.92" y="76.2"/>
<instance part="U$6" gate="G$1" x="248.92" y="55.88"/>
<instance part="C1" gate="G$1" x="213.36" y="48.26"/>
<instance part="C2" gate="G$1" x="248.92" y="48.26"/>
<instance part="C3" gate="G$1" x="254" y="71.12"/>
<instance part="C4" gate="G$1" x="213.36" y="71.12"/>
<instance part="C5" gate="G$1" x="38.1" y="76.2"/>
<instance part="C6" gate="G$1" x="88.9" y="71.12"/>
<instance part="C7" gate="G$1" x="109.22" y="76.2"/>
<instance part="C8" gate="G$1" x="175.26" y="55.88"/>
<instance part="R1" gate="G$1" x="-33.02" y="104.14"/>
<instance part="R2" gate="G$1" x="68.58" y="78.74"/>
<instance part="L1" gate="G$1" x="55.88" y="78.74" rot="R90"/>
<instance part="L2" gate="G$1" x="165.1" y="73.66"/>
<instance part="D1" gate="G$1" x="-45.72" y="104.14"/>
<instance part="D2" gate="G$1" x="50.8" y="63.5" rot="R90"/>
<instance part="D3" gate="G$1" x="170.18" y="60.96"/>
<instance part="R3" gate="G$1" x="-50.8" y="96.52" rot="R90"/>
<instance part="R4" gate="G$1" x="-50.8" y="83.82" rot="R90"/>
<instance part="R5" gate="G$1" x="-40.64" y="96.52" rot="R90"/>
<instance part="R6" gate="G$1" x="-27.94" y="96.52" rot="R90"/>
<instance part="R7" gate="G$1" x="20.32" y="88.9" rot="R90"/>
<instance part="R8" gate="G$1" x="20.32" y="66.04"/>
<instance part="R9" gate="G$1" x="63.5" y="71.12" rot="R90"/>
<instance part="R10" gate="G$1" x="76.2" y="71.12" rot="R90"/>
<instance part="R11" gate="G$1" x="99.06" y="60.96" rot="R90"/>
<instance part="R12" gate="G$1" x="-53.34" y="53.34" rot="R90"/>
<instance part="R13" gate="G$1" x="114.3" y="66.04" rot="R90"/>
<instance part="R14" gate="G$1" x="182.88" y="55.88" rot="R90"/>
<instance part="R15" gate="G$1" x="182.88" y="43.18" rot="R90"/>
<instance part="U$7" gate="G$1" x="76.2" y="-5.08"/>
<instance part="U$8" gate="G$1" x="210.82" y="-22.86"/>
<instance part="R19" gate="G$1" x="38.1" y="45.72" rot="R180"/>
<instance part="R20" gate="G$1" x="30.48" y="40.64" rot="R270"/>
<instance part="R21" gate="G$1" x="83.82" y="71.12" rot="R270"/>
<instance part="R22" gate="G$1" x="83.82" y="60.96" rot="R270"/>
<instance part="SUPPLY1" gate="GND" x="30.48" y="33.02"/>
<instance part="SUPPLY2" gate="GND" x="50.8" y="55.88"/>
<instance part="SUPPLY3" gate="GND" x="38.1" y="68.58"/>
<instance part="SUPPLY4" gate="GND" x="-50.8" y="76.2"/>
<instance part="SUPPLY5" gate="GND" x="83.82" y="53.34"/>
<instance part="SUPPLY6" gate="GND" x="88.9" y="63.5"/>
<instance part="SUPPLY7" gate="GND" x="121.92" y="50.8"/>
<instance part="SUPPLY8" gate="GND" x="182.88" y="35.56"/>
<instance part="SUPPLY9" gate="GND" x="175.26" y="48.26"/>
<instance part="SUPPLY10" gate="GND" x="213.36" y="63.5"/>
<instance part="SUPPLY11" gate="GND" x="213.36" y="40.64"/>
<instance part="SUPPLY12" gate="GND" x="233.68" y="40.64"/>
<instance part="SUPPLY13" gate="GND" x="248.92" y="40.64"/>
<instance part="SUPPLY14" gate="GND" x="254" y="63.5"/>
<instance part="SUPPLY15" gate="GND" x="220.98" y="63.5"/>
<instance part="SUPPLY16" gate="GND" x="203.2" y="5.08"/>
<instance part="SUPPLY17" gate="GND" x="243.84" y="-45.72"/>
<instance part="SUPPLY18" gate="GND" x="81.28" y="-45.72"/>
<instance part="C9" gate="G$1" x="-35.56" y="91.44" rot="R90"/>
<instance part="C10" gate="G$1" x="33.02" y="76.2"/>
<instance part="C11" gate="G$1" x="71.12" y="66.04" rot="R270"/>
<instance part="C12" gate="G$1" x="-58.42" y="55.88"/>
<instance part="R23" gate="G$1" x="-53.34" y="35.56" rot="R90"/>
<instance part="R24" gate="G$1" x="-48.26" y="53.34" rot="R90"/>
<instance part="R25" gate="G$1" x="-48.26" y="35.56" rot="R90"/>
<instance part="SUPPLY19" gate="GND" x="-53.34" y="27.94"/>
<instance part="SUPPLY20" gate="GND" x="-48.26" y="27.94"/>
<instance part="SUPPLY21" gate="GND" x="-58.42" y="48.26"/>
<instance part="SUPPLY22" gate="GND" x="-30.48" y="40.64"/>
<instance part="R26" gate="G$1" x="-35.56" y="38.1" rot="R90"/>
<instance part="C13" gate="G$1" x="-35.56" y="30.48"/>
<instance part="C14" gate="G$1" x="-40.64" y="30.48"/>
<instance part="SUPPLY23" gate="GND" x="-35.56" y="22.86"/>
<instance part="SUPPLY24" gate="GND" x="-40.64" y="22.86"/>
<instance part="C15" gate="G$1" x="114.3" y="58.42"/>
<instance part="SUPPLY25" gate="GND" x="109.22" y="68.58"/>
<instance part="C16" gate="G$1" x="76.2" y="-20.32"/>
<instance part="C17" gate="G$1" x="66.04" y="-12.7"/>
<instance part="C18" gate="G$1" x="58.42" y="-12.7"/>
<instance part="C19" gate="G$1" x="50.8" y="-12.7"/>
<instance part="C20" gate="G$1" x="193.04" y="-7.62"/>
<instance part="C21" gate="G$1" x="200.66" y="-7.62"/>
<instance part="C22" gate="G$1" x="213.36" y="-7.62" rot="R90"/>
<instance part="C23" gate="G$1" x="223.52" y="-7.62" rot="R90"/>
<instance part="C24" gate="G$1" x="231.14" y="-5.08"/>
<instance part="C25" gate="G$1" x="236.22" y="-5.08"/>
<instance part="C26" gate="G$1" x="241.3" y="-5.08"/>
<instance part="C27" gate="G$1" x="246.38" y="-5.08"/>
<instance part="C28" gate="G$1" x="251.46" y="-5.08"/>
<instance part="SUPPLY26" gate="GND" x="200.66" y="-15.24"/>
<instance part="SUPPLY27" gate="GND" x="193.04" y="-15.24"/>
<instance part="SUPPLY28" gate="GND" x="231.14" y="-12.7"/>
<instance part="SUPPLY29" gate="GND" x="236.22" y="-12.7"/>
<instance part="SUPPLY30" gate="GND" x="241.3" y="-12.7"/>
<instance part="SUPPLY31" gate="GND" x="246.38" y="-12.7"/>
<instance part="SUPPLY32" gate="GND" x="251.46" y="-12.7"/>
<instance part="U$4" gate="G$1" x="190.5" y="-5.08"/>
<instance part="R16" gate="G$1" x="190.5" y="-25.4" rot="R270"/>
<instance part="R17" gate="G$1" x="185.42" y="-25.4" rot="R270"/>
<instance part="U$9" gate="G$1" x="185.42" y="-17.78"/>
<instance part="C29" gate="G$1" x="205.74" y="-25.4" rot="R270"/>
<instance part="SUPPLY33" gate="GND" x="198.12" y="-25.4" rot="R270"/>
<instance part="C30" gate="G$1" x="251.46" y="-35.56"/>
<instance part="SUPPLY34" gate="GND" x="251.46" y="-43.18"/>
<instance part="L3" gate="G$1" x="259.08" y="-33.02" rot="R90"/>
<instance part="U$10" gate="G$1" x="264.16" y="-30.48"/>
<instance part="X1" gate="G$1" x="205.74" y="-43.18"/>
<instance part="SUPPLY35" gate="GND" x="205.74" y="-50.8"/>
<instance part="CON1" gate="_VCC" x="99.06" y="88.9"/>
<instance part="CON1" gate="_GND" x="99.06" y="86.36"/>
<instance part="SUPPLY36" gate="GND" x="91.44" y="83.82"/>
<instance part="CON2" gate="SCK" x="165.1" y="-38.1"/>
<instance part="CON2" gate="MISO" x="165.1" y="-33.02"/>
<instance part="CON2" gate="MOSI" x="165.1" y="-40.64"/>
<instance part="CON2" gate="RESET" x="78.74" y="22.86" rot="R180"/>
<instance part="CON2" gate="PWR" x="134.62" y="-53.34"/>
<instance part="U$11" gate="G$1" x="66.04" y="-10.16"/>
<instance part="U$12" gate="G$1" x="58.42" y="-10.16"/>
<instance part="U$13" gate="G$1" x="50.8" y="-10.16"/>
<instance part="SUPPLY38" gate="GND" x="66.04" y="-20.32"/>
<instance part="SUPPLY39" gate="GND" x="76.2" y="-27.94"/>
<instance part="SUPPLY40" gate="GND" x="58.42" y="-20.32"/>
<instance part="SUPPLY41" gate="GND" x="50.8" y="-20.32"/>
<instance part="U$14" gate="G$1" x="134.62" y="-45.72"/>
<instance part="SUPPLY42" gate="GND" x="134.62" y="-63.5"/>
<instance part="SW1" gate="G$1" x="157.48" y="-76.2"/>
<instance part="SW2" gate="G$1" x="157.48" y="-88.9"/>
<instance part="SW3" gate="G$1" x="157.48" y="-101.6"/>
<instance part="SUPPLY43" gate="GND" x="167.64" y="-111.76"/>
<instance part="J1" gate="G$1" x="-63.5" y="101.6"/>
<instance part="SUPPLY44" gate="GND" x="-55.88" y="91.44"/>
<instance part="ENC1" gate="G$1" x="200.66" y="-63.5"/>
<instance part="R18" gate="G$1" x="190.5" y="-53.34" rot="R270"/>
<instance part="R27" gate="G$1" x="187.96" y="-53.34" rot="R270"/>
<instance part="R28" gate="G$1" x="180.34" y="-60.96"/>
<instance part="R29" gate="G$1" x="180.34" y="-63.5"/>
<instance part="SUPPLY45" gate="GND" x="172.72" y="-76.2"/>
<instance part="C31" gate="G$1" x="167.64" y="-68.58"/>
<instance part="C32" gate="G$1" x="172.72" y="-68.58"/>
<instance part="SUPPLY46" gate="GND" x="167.64" y="-76.2"/>
<instance part="SUPPLY47" gate="GND" x="193.04" y="-68.58"/>
<instance part="U$15" gate="G$1" x="190.5" y="-48.26"/>
<instance part="U$16" gate="G$1" x="187.96" y="-48.26"/>
<instance part="CON3" gate="PTT" x="144.78" y="-2.54"/>
<instance part="CON3" gate="VAGC" x="144.78" y="22.86"/>
<instance part="CON3" gate="+12V" x="190.5" y="104.14"/>
<instance part="CON3" gate="PWRSW" x="144.78" y="0"/>
<instance part="CON3" gate="TXEN" x="144.78" y="17.78"/>
<instance part="CON3" gate="RXEN" x="144.78" y="20.32"/>
<instance part="CON3" gate="GND" x="22.86" y="-12.7"/>
<instance part="SUPPLY37" gate="GND" x="20.32" y="-15.24"/>
</instances>
<busses>
</busses>
<nets>
<net name="N$1" class="0">
<segment>
<pinref part="C5" gate="G$1" pin="1"/>
<pinref part="U$1" gate="G$1" pin="S"/>
<wire x1="38.1" y1="78.74" x2="43.18" y2="78.74" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="S"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="-27.94" y1="104.14" x2="-7.62" y2="104.14" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="104.14" x2="20.32" y2="104.14" width="0.1524" layer="91"/>
<wire x1="38.1" y1="78.74" x2="33.02" y2="78.74" width="0.1524" layer="91"/>
<wire x1="33.02" y1="78.74" x2="27.94" y2="78.74" width="0.1524" layer="91"/>
<wire x1="27.94" y1="78.74" x2="-7.62" y2="78.74" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="78.74" x2="-7.62" y2="104.14" width="0.1524" layer="91"/>
<junction x="38.1" y="78.74"/>
<junction x="-7.62" y="104.14"/>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="-27.94" y1="101.6" x2="-27.94" y2="104.14" width="0.1524" layer="91"/>
<junction x="-27.94" y="104.14"/>
<pinref part="C10" gate="G$1" pin="1"/>
<junction x="33.02" y="78.74"/>
<pinref part="IC3" gate="G$1" pin="VCC"/>
<wire x1="12.7" y1="68.58" x2="27.94" y2="68.58" width="0.1524" layer="91"/>
<wire x1="27.94" y1="68.58" x2="27.94" y2="78.74" width="0.1524" layer="91"/>
<junction x="27.94" y="78.74"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="D"/>
<pinref part="L1" gate="G$1" pin="1"/>
<wire x1="50.8" y1="78.74" x2="48.26" y2="78.74" width="0.1524" layer="91"/>
<pinref part="D2" gate="G$1" pin="C"/>
<wire x1="50.8" y1="66.04" x2="50.8" y2="78.74" width="0.1524" layer="91"/>
<junction x="50.8" y="78.74"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="L1" gate="G$1" pin="2"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="63.5" y1="78.74" x2="60.96" y2="78.74" width="0.1524" layer="91"/>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="63.5" y1="76.2" x2="63.5" y2="78.74" width="0.1524" layer="91"/>
<junction x="63.5" y="78.74"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="IC6" gate="G$1" pin="VOUT"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="246.38" y1="73.66" x2="248.92" y2="73.66" width="0.1524" layer="91"/>
<pinref part="U$5" gate="G$1" pin="+3V3"/>
<wire x1="248.92" y1="73.66" x2="254" y2="73.66" width="0.1524" layer="91"/>
<wire x1="248.92" y1="76.2" x2="248.92" y2="73.66" width="0.1524" layer="91"/>
<junction x="248.92" y="73.66"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="VDD"/>
<pinref part="U$8" gate="G$1" pin="+3V3"/>
<wire x1="213.36" y1="-25.4" x2="210.82" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="210.82" y1="-25.4" x2="210.82" y2="-22.86" width="0.1524" layer="91"/>
<pinref part="C29" gate="G$1" pin="1"/>
<wire x1="210.82" y1="-25.4" x2="208.28" y2="-25.4" width="0.1524" layer="91"/>
<junction x="210.82" y="-25.4"/>
</segment>
<segment>
<pinref part="U$9" gate="G$1" pin="+3V3"/>
<pinref part="R17" gate="G$1" pin="1"/>
<wire x1="185.42" y1="-17.78" x2="185.42" y2="-20.32" width="0.1524" layer="91"/>
<pinref part="R16" gate="G$1" pin="1"/>
<wire x1="190.5" y1="-20.32" x2="185.42" y2="-20.32" width="0.1524" layer="91"/>
<junction x="185.42" y="-20.32"/>
</segment>
<segment>
<pinref part="U$10" gate="G$1" pin="+3V3"/>
<pinref part="L3" gate="G$1" pin="2"/>
<wire x1="264.16" y1="-30.48" x2="264.16" y2="-33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="IC5" gate="G$1" pin="OUT"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="246.38" y1="53.34" x2="248.92" y2="53.34" width="0.1524" layer="91"/>
<wire x1="248.92" y1="53.34" x2="248.92" y2="50.8" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="+5V"/>
<wire x1="248.92" y1="55.88" x2="248.92" y2="53.34" width="0.1524" layer="91"/>
<junction x="248.92" y="53.34"/>
</segment>
<segment>
<pinref part="IC1" gate="1" pin="VCC@1"/>
<pinref part="U$7" gate="G$1" pin="+5V"/>
<wire x1="81.28" y1="-5.08" x2="76.2" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="IC1" gate="1" pin="VCC@2"/>
<wire x1="81.28" y1="-7.62" x2="76.2" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="76.2" y1="-7.62" x2="76.2" y2="-5.08" width="0.1524" layer="91"/>
<junction x="76.2" y="-5.08"/>
<pinref part="IC1" gate="1" pin="AVCC"/>
<wire x1="81.28" y1="-12.7" x2="76.2" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="76.2" y1="-12.7" x2="76.2" y2="-7.62" width="0.1524" layer="91"/>
<junction x="76.2" y="-7.62"/>
</segment>
<segment>
<pinref part="LCD1" gate="G$1" pin="VDD"/>
<wire x1="200.66" y1="15.24" x2="200.66" y2="12.7" width="0.1524" layer="91"/>
<wire x1="200.66" y1="12.7" x2="200.66" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="200.66" y1="-2.54" x2="193.04" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="C20" gate="G$1" pin="1"/>
<wire x1="193.04" y1="-2.54" x2="193.04" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="U$4" gate="G$1" pin="+5V"/>
<wire x1="190.5" y1="-5.08" x2="193.04" y2="-5.08" width="0.1524" layer="91"/>
<junction x="193.04" y="-5.08"/>
<pinref part="LCD1" gate="G$1" pin="!RST"/>
<wire x1="190.5" y1="15.24" x2="190.5" y2="12.7" width="0.1524" layer="91"/>
<wire x1="190.5" y1="12.7" x2="200.66" y2="12.7" width="0.1524" layer="91"/>
<junction x="200.66" y="12.7"/>
</segment>
<segment>
<pinref part="C17" gate="G$1" pin="1"/>
<pinref part="U$11" gate="G$1" pin="+5V"/>
</segment>
<segment>
<pinref part="C18" gate="G$1" pin="1"/>
<pinref part="U$12" gate="G$1" pin="+5V"/>
</segment>
<segment>
<pinref part="C19" gate="G$1" pin="1"/>
<pinref part="U$13" gate="G$1" pin="+5V"/>
</segment>
<segment>
<pinref part="CON2" gate="PWR" pin="VCC"/>
<pinref part="U$14" gate="G$1" pin="+5V"/>
</segment>
<segment>
<pinref part="R18" gate="G$1" pin="1"/>
<pinref part="U$15" gate="G$1" pin="+5V"/>
</segment>
<segment>
<pinref part="R27" gate="G$1" pin="1"/>
<pinref part="U$16" gate="G$1" pin="+5V"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="IC5" gate="G$1" pin="IN"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="220.98" y1="53.34" x2="213.36" y2="53.34" width="0.1524" layer="91"/>
<wire x1="213.36" y1="53.34" x2="213.36" y2="50.8" width="0.1524" layer="91"/>
<pinref part="IC6" gate="G$1" pin="VIN"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="220.98" y1="73.66" x2="213.36" y2="73.66" width="0.1524" layer="91"/>
<pinref part="D3" gate="G$1" pin="C"/>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="172.72" y1="60.96" x2="175.26" y2="60.96" width="0.1524" layer="91"/>
<wire x1="175.26" y1="60.96" x2="175.26" y2="58.42" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="D"/>
<pinref part="R14" gate="G$1" pin="2"/>
<wire x1="25.4" y1="104.14" x2="182.88" y2="104.14" width="0.1524" layer="91"/>
<wire x1="182.88" y1="104.14" x2="182.88" y2="73.66" width="0.1524" layer="91"/>
<wire x1="182.88" y1="73.66" x2="182.88" y2="60.96" width="0.1524" layer="91"/>
<wire x1="175.26" y1="60.96" x2="182.88" y2="60.96" width="0.1524" layer="91"/>
<junction x="175.26" y="60.96"/>
<junction x="182.88" y="60.96"/>
<wire x1="213.36" y1="73.66" x2="208.28" y2="73.66" width="0.1524" layer="91"/>
<junction x="213.36" y="73.66"/>
<junction x="182.88" y="73.66"/>
<wire x1="208.28" y1="73.66" x2="182.88" y2="73.66" width="0.1524" layer="91"/>
<wire x1="213.36" y1="53.34" x2="208.28" y2="53.34" width="0.1524" layer="91"/>
<wire x1="208.28" y1="53.34" x2="208.28" y2="73.66" width="0.1524" layer="91"/>
<junction x="213.36" y="53.34"/>
<junction x="208.28" y="73.66"/>
<pinref part="CON3" gate="+12V" pin="S"/>
<wire x1="187.96" y1="104.14" x2="185.42" y2="104.14" width="0.1524" layer="91"/>
<wire x1="185.42" y1="104.14" x2="182.88" y2="104.14" width="0.1524" layer="91"/>
<junction x="182.88" y="104.14"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<pinref part="U$3" gate="G$1" pin="S"/>
<wire x1="73.66" y1="78.74" x2="76.2" y2="78.74" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="76.2" y1="78.74" x2="78.74" y2="78.74" width="0.1524" layer="91"/>
<wire x1="78.74" y1="78.74" x2="83.82" y2="78.74" width="0.1524" layer="91"/>
<wire x1="83.82" y1="78.74" x2="88.9" y2="78.74" width="0.1524" layer="91"/>
<wire x1="88.9" y1="78.74" x2="99.06" y2="78.74" width="0.1524" layer="91"/>
<wire x1="88.9" y1="73.66" x2="88.9" y2="78.74" width="0.1524" layer="91"/>
<junction x="88.9" y="78.74"/>
<pinref part="R10" gate="G$1" pin="2"/>
<wire x1="76.2" y1="76.2" x2="76.2" y2="78.74" width="0.1524" layer="91"/>
<junction x="76.2" y="78.74"/>
<pinref part="R19" gate="G$1" pin="1"/>
<wire x1="43.18" y1="45.72" x2="78.74" y2="45.72" width="0.1524" layer="91"/>
<wire x1="78.74" y1="45.72" x2="78.74" y2="78.74" width="0.1524" layer="91"/>
<junction x="78.74" y="78.74"/>
<pinref part="R21" gate="G$1" pin="1"/>
<wire x1="83.82" y1="76.2" x2="83.82" y2="78.74" width="0.1524" layer="91"/>
<junction x="83.82" y="78.74"/>
<pinref part="CON1" gate="_VCC" pin="S"/>
<wire x1="93.98" y1="88.9" x2="88.9" y2="88.9" width="0.1524" layer="91"/>
<wire x1="88.9" y1="88.9" x2="88.9" y2="78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="U$3" gate="G$1" pin="D"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="104.14" y1="78.74" x2="109.22" y2="78.74" width="0.1524" layer="91"/>
<wire x1="109.22" y1="78.74" x2="160.02" y2="78.74" width="0.1524" layer="91"/>
<wire x1="160.02" y1="78.74" x2="160.02" y2="66.04" width="0.1524" layer="91"/>
<junction x="109.22" y="78.74"/>
<pinref part="IC4" gate="G$1" pin="VIN"/>
<wire x1="160.02" y1="66.04" x2="154.94" y2="66.04" width="0.1524" layer="91"/>
<pinref part="L2" gate="G$1" pin="1"/>
<wire x1="165.1" y1="78.74" x2="160.02" y2="78.74" width="0.1524" layer="91"/>
<junction x="160.02" y="78.74"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="IC4" gate="G$1" pin="SW_3"/>
<pinref part="L2" gate="G$1" pin="2"/>
<wire x1="154.94" y1="60.96" x2="165.1" y2="60.96" width="0.1524" layer="91"/>
<wire x1="165.1" y1="60.96" x2="165.1" y2="68.58" width="0.1524" layer="91"/>
<pinref part="IC4" gate="G$1" pin="SW_2"/>
<wire x1="154.94" y1="58.42" x2="165.1" y2="58.42" width="0.1524" layer="91"/>
<wire x1="165.1" y1="58.42" x2="165.1" y2="60.96" width="0.1524" layer="91"/>
<junction x="165.1" y="60.96"/>
<pinref part="IC4" gate="G$1" pin="SW_1"/>
<wire x1="154.94" y1="55.88" x2="165.1" y2="55.88" width="0.1524" layer="91"/>
<wire x1="165.1" y1="55.88" x2="165.1" y2="58.42" width="0.1524" layer="91"/>
<junction x="165.1" y="58.42"/>
<pinref part="D3" gate="G$1" pin="A"/>
<wire x1="167.64" y1="60.96" x2="165.1" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="D1" gate="G$1" pin="C"/>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="-43.18" y1="104.14" x2="-40.64" y2="104.14" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="-40.64" y1="104.14" x2="-38.1" y2="104.14" width="0.1524" layer="91"/>
<wire x1="-40.64" y1="101.6" x2="-40.64" y2="104.14" width="0.1524" layer="91"/>
<junction x="-40.64" y="104.14"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="D1" gate="G$1" pin="A"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="-48.26" y1="104.14" x2="-50.8" y2="104.14" width="0.1524" layer="91"/>
<wire x1="-50.8" y1="104.14" x2="-50.8" y2="101.6" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="1"/>
<wire x1="-58.42" y1="104.14" x2="-50.8" y2="104.14" width="0.1524" layer="91"/>
<junction x="-50.8" y="104.14"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="2"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="-50.8" y1="88.9" x2="-50.8" y2="91.44" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="ACDET"/>
<wire x1="-22.86" y1="73.66" x2="-22.86" y2="88.9" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="88.9" x2="-50.8" y2="88.9" width="0.1524" layer="91"/>
<junction x="-50.8" y="88.9"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="R7" gate="G$1" pin="2"/>
<pinref part="U$2" gate="G$1" pin="G"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="R15" gate="G$1" pin="2"/>
<pinref part="R14" gate="G$1" pin="1"/>
<wire x1="182.88" y1="48.26" x2="182.88" y2="50.8" width="0.1524" layer="91"/>
<wire x1="182.88" y1="48.26" x2="119.38" y2="48.26" width="0.1524" layer="91"/>
<wire x1="119.38" y1="48.26" x2="119.38" y2="68.58" width="0.1524" layer="91"/>
<junction x="182.88" y="48.26"/>
<pinref part="IC4" gate="G$1" pin="FB"/>
<wire x1="121.92" y1="68.58" x2="119.38" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="R7" gate="G$1" pin="1"/>
<pinref part="IC3" gate="G$1" pin="!ACDRV"/>
<wire x1="20.32" y1="83.82" x2="20.32" y2="73.66" width="0.1524" layer="91"/>
<wire x1="20.32" y1="73.66" x2="12.7" y2="73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="!BATDRV"/>
<wire x1="12.7" y1="71.12" x2="30.48" y2="71.12" width="0.1524" layer="91"/>
<wire x1="30.48" y1="71.12" x2="30.48" y2="48.26" width="0.1524" layer="91"/>
<pinref part="R11" gate="G$1" pin="1"/>
<wire x1="30.48" y1="48.26" x2="99.06" y2="48.26" width="0.1524" layer="91"/>
<wire x1="99.06" y1="48.26" x2="99.06" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="R11" gate="G$1" pin="2"/>
<pinref part="U$3" gate="G$1" pin="G"/>
<wire x1="99.06" y1="66.04" x2="99.06" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="SRP"/>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="12.7" y1="53.34" x2="63.5" y2="53.34" width="0.1524" layer="91"/>
<wire x1="63.5" y1="53.34" x2="63.5" y2="66.04" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="2"/>
<wire x1="66.04" y1="66.04" x2="63.5" y2="66.04" width="0.1524" layer="91"/>
<junction x="63.5" y="66.04"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="SRN"/>
<pinref part="R10" gate="G$1" pin="1"/>
<wire x1="12.7" y1="50.8" x2="76.2" y2="50.8" width="0.1524" layer="91"/>
<wire x1="76.2" y1="50.8" x2="76.2" y2="66.04" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="73.66" y1="66.04" x2="76.2" y2="66.04" width="0.1524" layer="91"/>
<junction x="76.2" y="66.04"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="!PWM"/>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="12.7" y1="66.04" x2="15.24" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="R19" gate="G$1" pin="2"/>
<pinref part="R20" gate="G$1" pin="1"/>
<wire x1="33.02" y1="45.72" x2="30.48" y2="45.72" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="BATP"/>
<wire x1="12.7" y1="45.72" x2="30.48" y2="45.72" width="0.1524" layer="91"/>
<junction x="30.48" y="45.72"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="R21" gate="G$1" pin="2"/>
<pinref part="R22" gate="G$1" pin="1"/>
<pinref part="IC3" gate="G$1" pin="BATDEP"/>
<wire x1="-22.86" y1="66.04" x2="-25.4" y2="66.04" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="66.04" x2="-25.4" y2="27.94" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="27.94" x2="81.28" y2="27.94" width="0.1524" layer="91"/>
<wire x1="81.28" y1="27.94" x2="81.28" y2="66.04" width="0.1524" layer="91"/>
<wire x1="81.28" y1="66.04" x2="83.82" y2="66.04" width="0.1524" layer="91"/>
<junction x="83.82" y="66.04"/>
<wire x1="81.28" y1="27.94" x2="142.24" y2="27.94" width="0.1524" layer="91"/>
<junction x="81.28" y="27.94"/>
<wire x1="142.24" y1="27.94" x2="157.48" y2="27.94" width="0.1524" layer="91"/>
<wire x1="157.48" y1="27.94" x2="157.48" y2="7.62" width="0.1524" layer="91"/>
<pinref part="IC1" gate="1" pin="ADC6"/>
<wire x1="157.48" y1="7.62" x2="142.24" y2="7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="R20" gate="G$1" pin="2"/>
<pinref part="SUPPLY1" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="IC3" gate="G$1" pin="GND"/>
<wire x1="12.7" y1="55.88" x2="15.24" y2="55.88" width="0.1524" layer="91"/>
<wire x1="15.24" y1="55.88" x2="48.26" y2="55.88" width="0.1524" layer="91"/>
<wire x1="48.26" y1="55.88" x2="48.26" y2="58.42" width="0.1524" layer="91"/>
<pinref part="SUPPLY2" gate="GND" pin="GND"/>
<wire x1="48.26" y1="58.42" x2="50.8" y2="58.42" width="0.1524" layer="91"/>
<pinref part="D2" gate="G$1" pin="A"/>
<wire x1="50.8" y1="60.96" x2="50.8" y2="58.42" width="0.1524" layer="91"/>
<junction x="50.8" y="58.42"/>
<pinref part="IC3" gate="G$1" pin="VS"/>
<wire x1="12.7" y1="58.42" x2="15.24" y2="58.42" width="0.1524" layer="91"/>
<wire x1="15.24" y1="58.42" x2="15.24" y2="55.88" width="0.1524" layer="91"/>
<junction x="15.24" y="55.88"/>
</segment>
<segment>
<pinref part="C5" gate="G$1" pin="2"/>
<pinref part="SUPPLY3" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="R4" gate="G$1" pin="1"/>
<pinref part="SUPPLY4" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="R22" gate="G$1" pin="2"/>
<pinref part="SUPPLY5" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C6" gate="G$1" pin="2"/>
<pinref part="SUPPLY6" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="R15" gate="G$1" pin="1"/>
<pinref part="SUPPLY8" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C8" gate="G$1" pin="2"/>
<pinref part="SUPPLY9" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C4" gate="G$1" pin="2"/>
<pinref part="SUPPLY10" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="2"/>
<pinref part="SUPPLY11" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="IC5" gate="G$1" pin="GND"/>
<pinref part="SUPPLY12" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C2" gate="G$1" pin="2"/>
<pinref part="SUPPLY13" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C3" gate="G$1" pin="2"/>
<pinref part="SUPPLY14" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="LCD1" gate="G$1" pin="VSS"/>
<pinref part="SUPPLY16" gate="GND" pin="GND"/>
<wire x1="203.2" y1="15.24" x2="203.2" y2="7.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="GND"/>
<pinref part="SUPPLY17" gate="GND" pin="GND"/>
<wire x1="238.76" y1="-30.48" x2="243.84" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="243.84" y1="-30.48" x2="243.84" y2="-43.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC1" gate="1" pin="AGND"/>
<pinref part="IC1" gate="1" pin="GND@1"/>
<wire x1="81.28" y1="-27.94" x2="81.28" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="IC1" gate="1" pin="GND@2"/>
<wire x1="81.28" y1="-35.56" x2="81.28" y2="-33.02" width="0.1524" layer="91"/>
<junction x="81.28" y="-33.02"/>
<pinref part="SUPPLY18" gate="GND" pin="GND"/>
<wire x1="81.28" y1="-43.18" x2="81.28" y2="-35.56" width="0.1524" layer="91"/>
<junction x="81.28" y="-35.56"/>
</segment>
<segment>
<pinref part="R23" gate="G$1" pin="1"/>
<pinref part="SUPPLY19" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="R25" gate="G$1" pin="1"/>
<pinref part="SUPPLY20" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C12" gate="G$1" pin="2"/>
<pinref part="SUPPLY21" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="IC3" gate="G$1" pin="BATSET"/>
<pinref part="SUPPLY22" gate="GND" pin="GND"/>
<wire x1="-22.86" y1="53.34" x2="-30.48" y2="53.34" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="53.34" x2="-30.48" y2="43.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C13" gate="G$1" pin="2"/>
<pinref part="SUPPLY23" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C14" gate="G$1" pin="2"/>
<pinref part="SUPPLY24" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C15" gate="G$1" pin="2"/>
<pinref part="SUPPLY7" gate="GND" pin="GND"/>
<wire x1="114.3" y1="53.34" x2="121.92" y2="53.34" width="0.1524" layer="91"/>
<pinref part="IC4" gate="G$1" pin="AGND"/>
<pinref part="IC4" gate="G$1" pin="PGND_1"/>
<wire x1="121.92" y1="63.5" x2="121.92" y2="60.96" width="0.1524" layer="91"/>
<pinref part="IC4" gate="G$1" pin="PGND_2"/>
<wire x1="121.92" y1="60.96" x2="121.92" y2="58.42" width="0.1524" layer="91"/>
<junction x="121.92" y="60.96"/>
<pinref part="IC4" gate="G$1" pin="PGND_3"/>
<wire x1="121.92" y1="58.42" x2="121.92" y2="55.88" width="0.1524" layer="91"/>
<junction x="121.92" y="58.42"/>
<wire x1="121.92" y1="55.88" x2="121.92" y2="53.34" width="0.1524" layer="91"/>
<junction x="121.92" y="55.88"/>
<junction x="121.92" y="53.34"/>
</segment>
<segment>
<pinref part="C7" gate="G$1" pin="2"/>
<pinref part="SUPPLY25" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C21" gate="G$1" pin="2"/>
<pinref part="SUPPLY26" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C20" gate="G$1" pin="2"/>
<pinref part="SUPPLY27" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C24" gate="G$1" pin="2"/>
<pinref part="SUPPLY28" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C25" gate="G$1" pin="2"/>
<pinref part="SUPPLY29" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C26" gate="G$1" pin="2"/>
<pinref part="SUPPLY30" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C27" gate="G$1" pin="2"/>
<pinref part="SUPPLY31" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C28" gate="G$1" pin="2"/>
<pinref part="SUPPLY32" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C29" gate="G$1" pin="2"/>
<pinref part="SUPPLY33" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C30" gate="G$1" pin="2"/>
<pinref part="SUPPLY34" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="X1" gate="G$1" pin="2"/>
<pinref part="SUPPLY35" gate="GND" pin="GND"/>
<pinref part="X1" gate="G$1" pin="4"/>
<wire x1="208.28" y1="-48.26" x2="205.74" y2="-48.26" width="0.1524" layer="91"/>
<junction x="205.74" y="-48.26"/>
</segment>
<segment>
<pinref part="CON1" gate="_GND" pin="S"/>
<pinref part="SUPPLY36" gate="GND" pin="GND"/>
<wire x1="93.98" y1="86.36" x2="91.44" y2="86.36" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY15" gate="GND" pin="GND"/>
<pinref part="IC6" gate="G$1" pin="GND"/>
<wire x1="220.98" y1="66.04" x2="220.98" y2="68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C17" gate="G$1" pin="2"/>
<pinref part="SUPPLY38" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C16" gate="G$1" pin="2"/>
<pinref part="SUPPLY39" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C18" gate="G$1" pin="2"/>
<pinref part="SUPPLY40" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C19" gate="G$1" pin="2"/>
<pinref part="SUPPLY41" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="CON2" gate="PWR" pin="GND"/>
<pinref part="SUPPLY42" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="SW1" gate="G$1" pin="4"/>
<pinref part="SUPPLY43" gate="GND" pin="GND"/>
<wire x1="162.56" y1="-81.28" x2="167.64" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="167.64" y1="-81.28" x2="167.64" y2="-93.98" width="0.1524" layer="91"/>
<pinref part="SW2" gate="G$1" pin="4"/>
<wire x1="167.64" y1="-93.98" x2="167.64" y2="-106.68" width="0.1524" layer="91"/>
<wire x1="167.64" y1="-106.68" x2="167.64" y2="-109.22" width="0.1524" layer="91"/>
<wire x1="162.56" y1="-93.98" x2="167.64" y2="-93.98" width="0.1524" layer="91"/>
<junction x="167.64" y="-93.98"/>
<pinref part="SW3" gate="G$1" pin="4"/>
<wire x1="162.56" y1="-106.68" x2="167.64" y2="-106.68" width="0.1524" layer="91"/>
<junction x="167.64" y="-106.68"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="2"/>
<pinref part="SUPPLY44" gate="GND" pin="GND"/>
<wire x1="-58.42" y1="101.6" x2="-55.88" y2="101.6" width="0.1524" layer="91"/>
<wire x1="-55.88" y1="101.6" x2="-55.88" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C32" gate="G$1" pin="2"/>
<pinref part="SUPPLY45" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C31" gate="G$1" pin="2"/>
<pinref part="SUPPLY46" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="ENC1" gate="G$1" pin="COM"/>
<pinref part="SUPPLY47" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="CON3" gate="GND" pin="S"/>
<pinref part="SUPPLY37" gate="GND" pin="GND"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="ACP"/>
<wire x1="-22.86" y1="45.72" x2="-40.64" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-40.64" y1="45.72" x2="-40.64" y2="91.44" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="1"/>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="-38.1" y1="91.44" x2="-40.64" y2="91.44" width="0.1524" layer="91"/>
<junction x="-40.64" y="91.44"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="ACN"/>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="-22.86" y1="48.26" x2="-27.94" y2="48.26" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="48.26" x2="-27.94" y2="91.44" width="0.1524" layer="91"/>
<pinref part="C9" gate="G$1" pin="2"/>
<wire x1="-30.48" y1="91.44" x2="-27.94" y2="91.44" width="0.1524" layer="91"/>
<junction x="-27.94" y="91.44"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="VHSP"/>
<wire x1="12.7" y1="63.5" x2="33.02" y2="63.5" width="0.1524" layer="91"/>
<wire x1="33.02" y1="63.5" x2="33.02" y2="68.58" width="0.1524" layer="91"/>
<pinref part="C10" gate="G$1" pin="2"/>
<wire x1="33.02" y1="68.58" x2="33.02" y2="71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="R8" gate="G$1" pin="2"/>
<pinref part="U$1" gate="G$1" pin="G"/>
<wire x1="25.4" y1="66.04" x2="43.18" y2="66.04" width="0.1524" layer="91"/>
<wire x1="43.18" y1="66.04" x2="43.18" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="VREF"/>
<wire x1="-22.86" y1="58.42" x2="-48.26" y2="58.42" width="0.1524" layer="91"/>
<pinref part="C12" gate="G$1" pin="1"/>
<wire x1="-48.26" y1="58.42" x2="-53.34" y2="58.42" width="0.1524" layer="91"/>
<wire x1="-53.34" y1="58.42" x2="-55.88" y2="58.42" width="0.1524" layer="91"/>
<wire x1="-55.88" y1="58.42" x2="-58.42" y2="58.42" width="0.1524" layer="91"/>
<pinref part="R12" gate="G$1" pin="2"/>
<junction x="-53.34" y="58.42"/>
<pinref part="R24" gate="G$1" pin="2"/>
<junction x="-48.26" y="58.42"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="R12" gate="G$1" pin="1"/>
<pinref part="R23" gate="G$1" pin="2"/>
<wire x1="-53.34" y1="40.64" x2="-53.34" y2="45.72" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="SRSET"/>
<wire x1="-53.34" y1="45.72" x2="-53.34" y2="48.26" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="63.5" x2="-45.72" y2="63.5" width="0.1524" layer="91"/>
<wire x1="-45.72" y1="63.5" x2="-45.72" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-45.72" y1="45.72" x2="-53.34" y2="45.72" width="0.1524" layer="91"/>
<junction x="-53.34" y="45.72"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="R24" gate="G$1" pin="1"/>
<pinref part="R25" gate="G$1" pin="2"/>
<wire x1="-48.26" y1="40.64" x2="-48.26" y2="43.18" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="ACSET"/>
<wire x1="-48.26" y1="43.18" x2="-48.26" y2="48.26" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="60.96" x2="-43.18" y2="60.96" width="0.1524" layer="91"/>
<wire x1="-43.18" y1="60.96" x2="-43.18" y2="43.18" width="0.1524" layer="91"/>
<wire x1="-43.18" y1="43.18" x2="-48.26" y2="43.18" width="0.1524" layer="91"/>
<junction x="-48.26" y="43.18"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="R26" gate="G$1" pin="1"/>
<pinref part="C13" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="C14" gate="G$1" pin="1"/>
<pinref part="R26" gate="G$1" pin="2"/>
<wire x1="-40.64" y1="33.02" x2="-40.64" y2="43.18" width="0.1524" layer="91"/>
<wire x1="-40.64" y1="43.18" x2="-35.56" y2="43.18" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="COMP"/>
<wire x1="-22.86" y1="50.8" x2="-35.56" y2="50.8" width="0.1524" layer="91"/>
<wire x1="-35.56" y1="50.8" x2="-35.56" y2="43.18" width="0.1524" layer="91"/>
<junction x="-35.56" y="43.18"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="R13" gate="G$1" pin="1"/>
<pinref part="C15" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="IC4" gate="G$1" pin="VC"/>
<pinref part="R13" gate="G$1" pin="2"/>
<wire x1="121.92" y1="71.12" x2="114.3" y2="71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="IC1" gate="1" pin="AREF"/>
<wire x1="81.28" y1="-15.24" x2="76.2" y2="-15.24" width="0.1524" layer="91"/>
<pinref part="C16" gate="G$1" pin="1"/>
<wire x1="76.2" y1="-15.24" x2="76.2" y2="-17.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<pinref part="LCD1" gate="G$1" pin="C1-"/>
<wire x1="208.28" y1="15.24" x2="208.28" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="208.28" y1="-7.62" x2="210.82" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="C22" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="LCD1" gate="G$1" pin="C1+"/>
<wire x1="210.82" y1="15.24" x2="210.82" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="210.82" y1="-2.54" x2="215.9" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="C22" gate="G$1" pin="2"/>
<wire x1="215.9" y1="-2.54" x2="218.44" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="218.44" y1="-2.54" x2="218.44" y2="-7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="LCD1" gate="G$1" pin="C2+"/>
<wire x1="213.36" y1="15.24" x2="213.36" y2="0" width="0.1524" layer="91"/>
<wire x1="213.36" y1="0" x2="220.98" y2="0" width="0.1524" layer="91"/>
<pinref part="C23" gate="G$1" pin="1"/>
<wire x1="220.98" y1="0" x2="220.98" y2="-7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<pinref part="LCD1" gate="G$1" pin="C2-"/>
<wire x1="215.9" y1="15.24" x2="215.9" y2="2.54" width="0.1524" layer="91"/>
<wire x1="215.9" y1="2.54" x2="223.52" y2="2.54" width="0.1524" layer="91"/>
<pinref part="C23" gate="G$1" pin="2"/>
<wire x1="223.52" y1="2.54" x2="228.6" y2="2.54" width="0.1524" layer="91"/>
<wire x1="228.6" y1="2.54" x2="228.6" y2="-7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="C21" gate="G$1" pin="1"/>
<wire x1="200.66" y1="-5.08" x2="205.74" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="LCD1" gate="G$1" pin="VOUT"/>
<wire x1="205.74" y1="-5.08" x2="205.74" y2="15.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<pinref part="LCD1" gate="G$1" pin="V1"/>
<wire x1="218.44" y1="15.24" x2="218.44" y2="5.08" width="0.1524" layer="91"/>
<wire x1="218.44" y1="5.08" x2="231.14" y2="5.08" width="0.1524" layer="91"/>
<pinref part="C24" gate="G$1" pin="1"/>
<wire x1="231.14" y1="5.08" x2="231.14" y2="-2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="LCD1" gate="G$1" pin="V2"/>
<wire x1="220.98" y1="15.24" x2="220.98" y2="7.62" width="0.1524" layer="91"/>
<pinref part="C25" gate="G$1" pin="1"/>
<wire x1="220.98" y1="7.62" x2="236.22" y2="7.62" width="0.1524" layer="91"/>
<wire x1="236.22" y1="7.62" x2="236.22" y2="-2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<pinref part="LCD1" gate="G$1" pin="V3"/>
<wire x1="223.52" y1="15.24" x2="223.52" y2="10.16" width="0.1524" layer="91"/>
<wire x1="223.52" y1="10.16" x2="241.3" y2="10.16" width="0.1524" layer="91"/>
<pinref part="C26" gate="G$1" pin="1"/>
<wire x1="241.3" y1="10.16" x2="241.3" y2="-2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<pinref part="LCD1" gate="G$1" pin="V4"/>
<wire x1="226.06" y1="15.24" x2="226.06" y2="12.7" width="0.1524" layer="91"/>
<wire x1="226.06" y1="12.7" x2="246.38" y2="12.7" width="0.1524" layer="91"/>
<pinref part="C27" gate="G$1" pin="1"/>
<wire x1="246.38" y1="12.7" x2="246.38" y2="-2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<pinref part="LCD1" gate="G$1" pin="V5"/>
<pinref part="C28" gate="G$1" pin="1"/>
<wire x1="228.6" y1="15.24" x2="251.46" y2="15.24" width="0.1524" layer="91"/>
<wire x1="251.46" y1="15.24" x2="251.46" y2="-2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<pinref part="LCD1" gate="G$1" pin="!CS1B"/>
<wire x1="187.96" y1="15.24" x2="187.96" y2="5.08" width="0.1524" layer="91"/>
<wire x1="187.96" y1="5.08" x2="180.34" y2="5.08" width="0.1524" layer="91"/>
<pinref part="IC1" gate="1" pin="PB2(SS/OC1B/PCINT2)"/>
<wire x1="142.24" y1="-27.94" x2="167.64" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="167.64" y1="-27.94" x2="167.64" y2="5.08" width="0.1524" layer="91"/>
<wire x1="167.64" y1="5.08" x2="180.34" y2="5.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<pinref part="LCD1" gate="G$1" pin="SCL"/>
<wire x1="195.58" y1="15.24" x2="195.58" y2="2.54" width="0.1524" layer="91"/>
<wire x1="195.58" y1="2.54" x2="177.8" y2="2.54" width="0.1524" layer="91"/>
<pinref part="IC1" gate="1" pin="PB5(SCK/PCINT5)"/>
<wire x1="142.24" y1="-35.56" x2="162.56" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="162.56" y1="-35.56" x2="170.18" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="170.18" y1="-35.56" x2="170.18" y2="2.54" width="0.1524" layer="91"/>
<wire x1="170.18" y1="2.54" x2="177.8" y2="2.54" width="0.1524" layer="91"/>
<pinref part="CON2" gate="SCK" pin="S"/>
<wire x1="162.56" y1="-38.1" x2="162.56" y2="-35.56" width="0.1524" layer="91"/>
<junction x="162.56" y="-35.56"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<pinref part="LCD1" gate="G$1" pin="SDA"/>
<wire x1="198.12" y1="15.24" x2="198.12" y2="0" width="0.1524" layer="91"/>
<wire x1="198.12" y1="0" x2="175.26" y2="0" width="0.1524" layer="91"/>
<wire x1="175.26" y1="0" x2="172.72" y2="0" width="0.1524" layer="91"/>
<wire x1="172.72" y1="0" x2="172.72" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="IC1" gate="1" pin="PB3(MOSI/OC2A/PCINT3)"/>
<wire x1="172.72" y1="-30.48" x2="160.02" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="CON2" gate="MOSI" pin="S"/>
<wire x1="160.02" y1="-30.48" x2="142.24" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="162.56" y1="-40.64" x2="160.02" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="160.02" y1="-40.64" x2="160.02" y2="-30.48" width="0.1524" layer="91"/>
<junction x="160.02" y="-30.48"/>
</segment>
</net>
<net name="N$48" class="0">
<segment>
<pinref part="LCD1" gate="G$1" pin="RS"/>
<wire x1="193.04" y1="15.24" x2="193.04" y2="7.62" width="0.1524" layer="91"/>
<wire x1="193.04" y1="7.62" x2="165.1" y2="7.62" width="0.1524" layer="91"/>
<wire x1="165.1" y1="7.62" x2="165.1" y2="-25.4" width="0.1524" layer="91"/>
<pinref part="IC1" gate="1" pin="PB1(OC1A/PCINT1)"/>
<wire x1="165.1" y1="-25.4" x2="142.24" y2="-25.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$49" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="SDA"/>
<wire x1="213.36" y1="-35.56" x2="185.42" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="185.42" y1="-35.56" x2="177.8" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="177.8" y1="-35.56" x2="177.8" y2="12.7" width="0.1524" layer="91"/>
<pinref part="IC1" gate="1" pin="PC4(ADC4/SDA/PCINT12)"/>
<wire x1="177.8" y1="12.7" x2="142.24" y2="12.7" width="0.1524" layer="91"/>
<pinref part="R17" gate="G$1" pin="2"/>
<wire x1="185.42" y1="-30.48" x2="185.42" y2="-35.56" width="0.1524" layer="91"/>
<junction x="185.42" y="-35.56"/>
</segment>
</net>
<net name="N$50" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="SCL"/>
<wire x1="213.36" y1="-33.02" x2="190.5" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="190.5" y1="-33.02" x2="175.26" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="175.26" y1="-33.02" x2="175.26" y2="10.16" width="0.1524" layer="91"/>
<wire x1="175.26" y1="10.16" x2="160.02" y2="10.16" width="0.1524" layer="91"/>
<pinref part="IC1" gate="1" pin="PC5(ADC5/SCL/PCINT13)"/>
<wire x1="160.02" y1="10.16" x2="142.24" y2="10.16" width="0.1524" layer="91"/>
<pinref part="R16" gate="G$1" pin="2"/>
<wire x1="190.5" y1="-30.48" x2="190.5" y2="-33.02" width="0.1524" layer="91"/>
<junction x="190.5" y="-33.02"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="VDDO"/>
<pinref part="C30" gate="G$1" pin="1"/>
<wire x1="238.76" y1="-33.02" x2="251.46" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="L3" gate="G$1" pin="1"/>
<wire x1="254" y1="-33.02" x2="251.46" y2="-33.02" width="0.1524" layer="91"/>
<junction x="251.46" y="-33.02"/>
</segment>
</net>
<net name="N$47" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="XB"/>
<pinref part="X1" gate="G$1" pin="3"/>
<wire x1="213.36" y1="-30.48" x2="210.82" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="210.82" y1="-30.48" x2="210.82" y2="-43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$51" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="XA"/>
<pinref part="X1" gate="G$1" pin="1"/>
<wire x1="213.36" y1="-27.94" x2="203.2" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="203.2" y1="-27.94" x2="203.2" y2="-43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$52" class="0">
<segment>
<pinref part="IC1" gate="1" pin="PB4(MISO/PCINT4)"/>
<pinref part="CON2" gate="MISO" pin="S"/>
<wire x1="162.56" y1="-33.02" x2="142.24" y2="-33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$53" class="0">
<segment>
<pinref part="IC1" gate="1" pin="PC6(/RESET/PCINT14)"/>
<pinref part="CON2" gate="RESET" pin="S"/>
</segment>
</net>
<net name="N$55" class="0">
<segment>
<wire x1="160.02" y1="17.78" x2="160.02" y2="43.18" width="0.1524" layer="91"/>
<wire x1="160.02" y1="43.18" x2="116.84" y2="43.18" width="0.1524" layer="91"/>
<wire x1="116.84" y1="43.18" x2="116.84" y2="66.04" width="0.1524" layer="91"/>
<pinref part="IC4" gate="G$1" pin="!SHDN"/>
<wire x1="116.84" y1="66.04" x2="121.92" y2="66.04" width="0.1524" layer="91"/>
<wire x1="160.02" y1="17.78" x2="182.88" y2="17.78" width="0.1524" layer="91"/>
<wire x1="182.88" y1="17.78" x2="182.88" y2="33.02" width="0.1524" layer="91"/>
<wire x1="182.88" y1="33.02" x2="205.74" y2="33.02" width="0.1524" layer="91"/>
<wire x1="205.74" y1="33.02" x2="205.74" y2="68.58" width="0.1524" layer="91"/>
<wire x1="205.74" y1="68.58" x2="218.44" y2="68.58" width="0.1524" layer="91"/>
<wire x1="218.44" y1="68.58" x2="218.44" y2="71.12" width="0.1524" layer="91"/>
<pinref part="IC6" gate="G$1" pin="!SHDN"/>
<wire x1="218.44" y1="71.12" x2="220.98" y2="71.12" width="0.1524" layer="91"/>
<pinref part="IC1" gate="1" pin="ADC7"/>
<wire x1="142.24" y1="5.08" x2="160.02" y2="5.08" width="0.1524" layer="91"/>
<wire x1="160.02" y1="5.08" x2="160.02" y2="17.78" width="0.1524" layer="91"/>
<junction x="160.02" y="17.78"/>
</segment>
</net>
<net name="N$56" class="0">
<segment>
<pinref part="SW1" gate="G$1" pin="1"/>
<pinref part="IC1" gate="1" pin="PD5(T1/OC0B/PCINT21)"/>
<wire x1="152.4" y1="-71.12" x2="152.4" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="152.4" y1="-12.7" x2="142.24" y2="-12.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$57" class="0">
<segment>
<pinref part="SW2" gate="G$1" pin="1"/>
<wire x1="152.4" y1="-83.82" x2="149.86" y2="-83.82" width="0.1524" layer="91"/>
<wire x1="149.86" y1="-83.82" x2="149.86" y2="-15.24" width="0.1524" layer="91"/>
<pinref part="IC1" gate="1" pin="PD6(AIN0/OC0A/PCINT22)"/>
<wire x1="149.86" y1="-15.24" x2="142.24" y2="-15.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$58" class="0">
<segment>
<pinref part="SW3" gate="G$1" pin="1"/>
<wire x1="152.4" y1="-96.52" x2="147.32" y2="-96.52" width="0.1524" layer="91"/>
<wire x1="147.32" y1="-96.52" x2="147.32" y2="-17.78" width="0.1524" layer="91"/>
<pinref part="IC1" gate="1" pin="PD7(AIN1/PCINT23)"/>
<wire x1="147.32" y1="-17.78" x2="142.24" y2="-17.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$59" class="0">
<segment>
<pinref part="ENC1" gate="G$1" pin="A"/>
<pinref part="R28" gate="G$1" pin="2"/>
<wire x1="193.04" y1="-60.96" x2="190.5" y2="-60.96" width="0.1524" layer="91"/>
<pinref part="R18" gate="G$1" pin="2"/>
<wire x1="190.5" y1="-60.96" x2="185.42" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="190.5" y1="-58.42" x2="190.5" y2="-60.96" width="0.1524" layer="91"/>
<junction x="190.5" y="-60.96"/>
</segment>
</net>
<net name="N$60" class="0">
<segment>
<pinref part="ENC1" gate="G$1" pin="B"/>
<pinref part="R29" gate="G$1" pin="2"/>
<wire x1="193.04" y1="-63.5" x2="187.96" y2="-63.5" width="0.1524" layer="91"/>
<pinref part="R27" gate="G$1" pin="2"/>
<wire x1="187.96" y1="-63.5" x2="185.42" y2="-63.5" width="0.1524" layer="91"/>
<wire x1="187.96" y1="-58.42" x2="187.96" y2="-63.5" width="0.1524" layer="91"/>
<junction x="187.96" y="-63.5"/>
</segment>
</net>
<net name="N$61" class="0">
<segment>
<pinref part="R29" gate="G$1" pin="1"/>
<wire x1="175.26" y1="-63.5" x2="167.64" y2="-63.5" width="0.1524" layer="91"/>
<pinref part="C31" gate="G$1" pin="1"/>
<wire x1="167.64" y1="-63.5" x2="167.64" y2="-66.04" width="0.1524" layer="91"/>
<wire x1="167.64" y1="-63.5" x2="154.94" y2="-63.5" width="0.1524" layer="91"/>
<wire x1="154.94" y1="-63.5" x2="154.94" y2="-10.16" width="0.1524" layer="91"/>
<junction x="167.64" y="-63.5"/>
<pinref part="IC1" gate="1" pin="PD4(T0/XCK/PCINT20)"/>
<wire x1="154.94" y1="-10.16" x2="142.24" y2="-10.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$62" class="0">
<segment>
<pinref part="R28" gate="G$1" pin="1"/>
<pinref part="C32" gate="G$1" pin="1"/>
<wire x1="175.26" y1="-60.96" x2="172.72" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="172.72" y1="-60.96" x2="172.72" y2="-66.04" width="0.1524" layer="91"/>
<wire x1="172.72" y1="-60.96" x2="157.48" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="157.48" y1="-60.96" x2="157.48" y2="-7.62" width="0.1524" layer="91"/>
<junction x="172.72" y="-60.96"/>
<pinref part="IC1" gate="1" pin="PD3(INT1/OC2B/PCINT19)"/>
<wire x1="157.48" y1="-7.62" x2="142.24" y2="-7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$54" class="0">
<segment>
<pinref part="IC1" gate="1" pin="PC0(ADC0/PCINT8)"/>
<pinref part="CON3" gate="VAGC" pin="S"/>
</segment>
</net>
<net name="N$63" class="0">
<segment>
<pinref part="IC1" gate="1" pin="PC1(ADC1/PCINT9)"/>
<pinref part="CON3" gate="RXEN" pin="S"/>
</segment>
</net>
<net name="N$65" class="0">
<segment>
<pinref part="IC1" gate="1" pin="PD0(RXD/PCINT16)"/>
<pinref part="CON3" gate="PWRSW" pin="S"/>
</segment>
</net>
<net name="N$66" class="0">
<segment>
<pinref part="IC1" gate="1" pin="PD1(TXD/PCINT17)"/>
<pinref part="CON3" gate="PTT" pin="S"/>
</segment>
</net>
<net name="N$64" class="0">
<segment>
<pinref part="IC1" gate="1" pin="PC2(ADC2/PCINT10)"/>
<pinref part="CON3" gate="TXEN" pin="S"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
